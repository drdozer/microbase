#!/bin/bash

# This script obtains EC2 user-data text and processes it.
# Keith Flanagan <k.s.flanagan@gmail.com>

if [ -z $1 ] ; then
  echo "Usage: parse_userdata.sh /path/to/userdata/file"
  exit 1
fi
USER_DATA=$1
MB_FS_JAR="/home/ubuntu/mb-filesystem-exe.jar"

if [ -z $MICROBASE_HOME ] ; then
    echo "Error: you must set MICROBASE_HOME to point at a directory containing your Microbase installation"
    exit 1
fi

if [ -z $MICROBASE_LOG ] ; then
    echo "Error: you must set MICROBASE_LOG to point at a directory for containing Microbase/responder log files."
    exit 1
fi
if [ -z $MICROBASE_CONFIG ] ; then
    echo "Error: you must set MICROBASE_CONFIG to point at a directory containing Microbase/responder configuration files."
    exit 1
fi
if [ -z $MICROBASE_SCRATCH ] ; then
    echo "Error: you must set MICROBASE_SCRATCH to point at a directory to be used as temporary space for job processing."
    exit 1
fi
if [ -z $RESPONDER_DATA ] ; then
    echo "Error: you must set RESPONDER_DATA to point at a directory to be used for holding large responder resources such as executables and file-based databases."
    exit 1
fi
if [ -z $RESPONDER_JARS ] ; then
    echo "Error: you must set RESPONDER_JARS to point at a directory containing compiled responder jar files."
    exit 1
fi


######## HTTP/FTP functions

# Downloads a file to a requested location
# Takes two paramters: 1) a URL; 2) a directory location
# If the directory does not currently exist, an attempt is made to create it.
function download {
  if [ ! -d $2 ] ; then
    echo "Creating destination directory $2"
    mkdir -p $2
    if [ ! -d $2 ] ; then
      echo "ERROR: Failed to create target directory: $2"
      exit 1
    fi
  fi
  echo "Downloading $1 ---> $2"
  filename=`awk -F '/' '{print $NF}' <<< $1`
  wget $1 -O $2/$filename
}


# Takes a 'value' from a key/value pair
# Parses out two parameters: a HTTP/FTP URL, and a destination directory
function parseAndDoDownload {
  url=`awk '{print $1}' <<< $1`
  dest=`awk '{print $2}' <<< $1`
  download $url $dest
}


# Takes a single parameter (a HTTP/FTP URL) of a config file to download and parse
function parseAndProcessInclude {
  echo "Downloading and including configuration file $1"
  # Extract filename from the URL (last field when delimited by '/')
  filename=`awk -F '/' '{print $NF}' <<< $1`
  download $1 $MICROBASE_CONFIG
  parse_userdata.sh $MICROBASE_CONFIG/$filename
}

# Downloads a file and unzips its content
# Takes 1 parameter - a URL whose content is to be downloaded
function downloadAndUnzip {
  download $1 $RESPONDER_DATA
  filename=`awk -F '/' '{print $NF}' <<< $1`
  ( cd $RESPONDER_DATA ; unzip $filename )
}


# Downloads a file and gunzips its content
# Takes 1 parameter - a URL whose content is to be downloaded
function downloadAndGunzip {
  download $1 $RESPONDER_DATA
  filename=`awk -F '/' '{print $NF}' <<< $1`
  ( cd $RESPONDER_DATA ; gunzip $filename )
}

# Downloads a file and gunzips/untars its content
# Takes 1 parameter - a URL whose content is to be downloaded
function downloadAndTarGunzip {
  download $1 $RESPONDER_DATA
  filename=`awk -F '/' '{print $NF}' <<< $1`
  ( cd $RESPONDER_DATA ; tar xzf $filename )
}


######## S3 functions


# Downloads a file from S3 to a local directory
# Takes six parameters:
# 1) Amazon access key
# 2) Amazon secret key
# 3) remote bucket name
# 4) remote path name (use '/' if downloading from the bucket root)
# 5) remote filename
# 6) local directory to download the file to
function download_s3 {
  aws_access_key=$1
  aws_secret_key=$2
  remote_bucket=$3
  remote_path=$4
  remote_file=$5
  local_dir=$6
  echo "Downloading $remote_bucket;$remote_path;$remote_file ---> $local_dir"
  java -jar $MB_FS_JAR \
    --aws-access-key $aws_access_key \
    --aws-secret-key $aws_secret_key \
    --download-to-file \
    -b $remote_bucket -p $remote_path -n $remote_file -f $local_dir/$remote_file
}


# Takes a 'value' from a key/value pair
# Parses  six parameters:
# 1) Amazon access key
# 2) Amazon secret key
# 3) remote bucket name
# 4) remote path name (use '/' if downloading from the bucket root)
# 5) remote filename
# 6) local directory to download the file to
function parseAndDoDownload_s3 {
  echo "Downloading a file to a specific directory location $3 $4 $5"
  aws_access_key=$1
  aws_secret_key=$2
  remote_bucket=$3
  remote_path=$4
  remote_name=$5
  local_dir=$6

  download_s3 $aws_access_key $aws_secret_key $remote_bucket $remote_path $remote_name $local_dir
}


# Downloads and parses a configuration file from S3
# The 'value' part of the key/value pair should contain a set of space-separated tokens
function parseAndProcessInclude_s3 {
  echo "Downloading and including configuration file $3 $4 $5"
  aws_access_key=$1
  aws_secret_key=$2
  remote_bucket=$3
  remote_path=$4
  remote_name=$5
  local_dir=$MICROBASE_CONFIG

  #echo "aws_access_key = $aws_access_key"
  #echo "aws_secret_key = $aws_secret_key"
  #echo "remote_bucket = $remote_bucket"
  #echo "remote_path = $remote_path"
  #echo "remote_name = $remote_name"
  #echo "local_dir = $local_dir"
  
  download_s3 $aws_access_key $aws_secret_key $remote_bucket $remote_path $remote_name $local_dir
 
  parse_userdata.sh $local_dir/$remote_name
}

# Downloads a file and unzips its content
# Takes 5 parameters - Amazon access keys, plus the bucket, path and remote name of the file to download
function downloadAndUnzip_s3 {
  echo "Downloading and unzipping configuration file $3 $4 $5"
  download_s3 $1 $2 $3 $4 $5 $RESPONDER_DATA
  filename=$5
  ( cd $RESPONDER_DATA ; unzip $filename )
}


# Downloads a file and gunzips its content
# Takes 5 parameters - Amazon access keys, plus the bucket, path and remote name of the file to download
function downloadAndGunzip_s3 {
  echo "Downloading and gunzipping configuration file $3 $4 $5"
  download_s3 $1 $2 $3 $4 $5 $RESPONDER_DATA
  filename=$5
  ( cd $RESPONDER_DATA ; gunzip $filename )
}

# Downloads a file and gunzips/untars its content
# Takes 5 parameters - Amazon access keys, plus the bucket, path and remote name of the file to download
function downloadAndTarGunzip_s3 {
  echo "Downloading and tar/gunzipping configuration file $3 $4 $5"
  download_s3 $1 $2 $3 $4 $5 $RESPONDER_DATA
  filename=$5
  echo "Filename: $filename ==========================================="
  ( cd $RESPONDER_DATA ; tar xzf $filename )
}


###############################################################################



while read line ; do
  command=`echo $line | awk -F '=' '{print $1}'`
  # The part of the line after the first occurrence of '='
  value=`echo $line | cut -d '=' -f2-`
  #echo "Command: $command"
  #echo "Value: $value"
  case "$command" in 
    "!download" ) parseAndDoDownload $value 
    ;;
    "!install_config" ) download $value $MICROBASE_CONFIG
    ;;
    "!install_jar" ) download $value $RESPONDER_JARS
    ;;
    "!include" ) parseAndProcessInclude $value
    ;;
    "!download_and_unzip" ) downloadAndUnzip $value
    ;;
    "!download_and_gunzip" ) downloadAndGunzip $value
    ;;
    "!download_and_tar_gunzip" ) downloadAndTarGunzip $value
    ;;

    
    "!download_s3" ) parseAndDoDownload_s3 $value 
    ;;
    "!install_config_s3" ) download_s3 $value $MICROBASE_CONFIG
    ;;
    "!install_jar_s3" ) download_s3 $value $RESPONDER_JARS
    ;;
    "!include_s3" ) parseAndProcessInclude_s3 $value
    ;;
    "!download_and_unzip_s3" ) downloadAndUnzip_s3 $value
    ;;
    "!download_and_gunzip_s3" ) downloadAndGunzip_s3 $value
    ;;
    "!download_and_tar_gunzip_s3" ) downloadAndTarGunzip_s3 $value
    ;;
  esac
done < $USER_DATA






