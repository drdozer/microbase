/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 9, 2012, 10:50:45 PM
 */
package uk.org.microbase.gui;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import uk.org.microbase.gui.services.GlobalState;
import uk.org.microbase.gui.services.MessageStateCounts;
import uk.org.microbase.gui.services.MessageStateCounts_prop;
import uk.org.microbase.gui.services.debug.GlobalMessageCountService;
import uk.org.microbase.gui.services.debug.GlobalMessageCountTask;
import uk.org.microbase.notification.data.Message;

/**
 *
 * @author keith
 */
public class MicrobaseGui
    extends Application
{
  private static final ScheduledThreadPoolExecutor exe = new ScheduledThreadPoolExecutor(5);
  
  MsgStateCountLabel finishedSucceededLbl;
  /**
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {

    launch(args);
  }

  @Override
  public void start(Stage stage)
  {
    GlobalState.instance().globalMessageCountState2.setReady(500000);
    GlobalState.instance().globalMessageCountState2.fireChanged();;
    
    
    
//    Group root = new Group();
    BorderPane layout = new BorderPane();
    Scene scene = new Scene(layout, 500, 500, Color.WHITE);
    stage.setTitle("JavaFX Scene Graph Demo");
    stage.setScene(scene);
    stage.show();





//    BorderPane layout = new BorderPane();
    layout.setTop(new Rectangle(200, 50, Color.DARKCYAN));
    layout.setBottom(createButtonPanel());
    layout.setCenter(new Rectangle(100, 100, Color.MEDIUMAQUAMARINE));
    layout.setLeft(createMessageStatesPanel());
    layout.setRight(new Rectangle(50, 100, Color.DARKTURQUOISE));

//        layout.setTop(new Rectangle(1.0, 50, Color.DARKCYAN));
//    layout.setBottom(new Rectangle(1.0, 50, Color.DARKCYAN));
//    layout.setCenter(new Rectangle(0.5, 100, Color.MEDIUMAQUAMARINE));
//    layout.setLeft(new Rectangle(0.25, 100, Color.DARKTURQUOISE));
//    layout.setRight(new Rectangle(0.25, 100, Color.DARKTURQUOISE));

//    root.getChildren().add(layout);

//    root.getChildren().add(new Label("Foo"));


//    primaryStage.setTitle("Hello World!");
//    Button btn = new Button();
//    btn.setText("Say 'Hello World'");
//    btn.setOnAction(new EventHandler<ActionEvent>()
//    {
//
//      @Override
//      public void handle(ActionEvent event)
//      {
//        System.out.println("Hello World!");
//      }
//    });
//
//    StackPane root = new StackPane();
//    root.getChildren().add(btn);
//    primaryStage.setScene(new Scene(root, 300, 250));
//    primaryStage.show();
    
    
//    final GlobalMessageCountService service = new GlobalMessageCountService();
//     service.addEventHandler(EventType.ROOT, new EventHandler() {
//
//      @Override
//      public void handle(Event arg0)
//      {
//        System.out.println(arg0.getEventType() + ", "+arg0.getEventType().getName());
//        if (service.getState() == State.SUCCEEDED)
//        {
//          System.out.println("Worker is done.");
//        }
//      }
//    });
//     
//     service.setExecutor(exe);
//     service.start();
    
    
    exe.scheduleAtFixedRate(new Runnable() {

      @Override
      public void run()
      {
        final GlobalMessageCountTask task1 = new GlobalMessageCountTask();
        task1.run();
//        task1.addEventHandler(EventType.ROOT, new EventHandler() {
//
//          @Override
//          public void handle(Event arg0)
//          {
//            System.out.println(arg0.getEventType());
//            if (task1.isDone())
//            {
//              State state = task1.getState();
//              System.out.println("Worker is done: " + state);
//              GlobalState.instance().globalMessageCountState2.fireChanged();
//            }
//          }
//        });
      }
    }, 1, 1, TimeUnit.SECONDS);
     
    
//    exe.scheduleAtFixedRate(new Runnable() {
//
//      @Override
//      public void run()
//      {
//        System.out.println("Scheduler running service again");
//        System.out.println("is running?");
////        if (service.isRunning())
////        {
////          System.out.println("Cancelling");
////          service.cancel();
////        }
////        System.out.println("Reset / start the service");
////        service.reset();
//        System.out.println("Start");
//        service.start();
//        System.out.println("started");
//      }
//    }, 1000, 5000, TimeUnit.MILLISECONDS);
    
    
    
//    final Task<Integer> worker = new Task<Integer>() {
//
//      @Override
//      protected Integer call()
//          throws Exception
//      {
//        int count = 0;
//        System.out.println("Waiting");
//        Thread.sleep(3000);
//        System.out.println("Running");
//        count = 456789;
////        setCount(456789);
//        System.out.println("Done");
//        
//        return count;
//      }
//    };
      
      
//    worker.addEventHandler(EventType.ROOT, new EventHandler() {
//
//      @Override
//      public void handle(Event arg0)
//      {
//        System.out.println(arg0.getEventType());
//        if (worker.isDone())
//        {
//          
//          int newVal = worker.getValue();
//          State state = worker.getState();
//          System.out.println("Worker is done: "+state+", val: "+newVal);
//          setCount(newVal);
//        }
//      }
//    });
//    
//    new Thread(worker).start();
  }
  
//  private void setCount(int foo)
//  {
//    
//    finishedSucceededLbl.setCount(foo);
//  }

  /**
   * A panel that contains a number of ResponderMsgCountLabel instances - one
   * for each responder detected in the system.
   *
   * @return
   */
  private Node createMessageStatesPanel()
  {
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(5, 5, 5, 5));
    vbox.setSpacing(2);
    Text title = new Text("Message states");
    title.setFont(Font.font("Amble CN", FontWeight.BOLD, 14));
    vbox.getChildren().add(title);
    
    MessageStateCounts_prop globalCounts = GlobalState.instance().globalMessageCountState;
    
    MsgStateCountLabel readyLbl = new MsgStateCountLabel(Message.State.READY, globalCounts.getReady());
    MsgStateCountLabel processingLbl = new MsgStateCountLabel(Message.State.PROCESSING, globalCounts.getProcessing());
    MsgStateCountLabel blockedLbl = new MsgStateCountLabel(Message.State.BLOCKED_AWAITING_LOCKS, globalCounts.getBlocked());
    MsgStateCountLabel errorFailedLbl = new MsgStateCountLabel(Message.State.ERROR_FAILED, globalCounts.getErrorFailed());
    MsgStateCountLabel errorTimeoutLbl = new MsgStateCountLabel(Message.State.ERROR_TIMEOUT, globalCounts.getErrorTimeout());
    MsgStateCountLabel finishedSkippedLbl = new MsgStateCountLabel(Message.State.FINISHED_SKIPPED, globalCounts.getFinishedSkipped());
     finishedSucceededLbl = new MsgStateCountLabel(Message.State.FINISHED_SUCCEEDED, globalCounts.getFinishedSucceeded());
    
        vbox.getChildren().addAll(readyLbl, processingLbl, blockedLbl, 
        errorFailedLbl, errorTimeoutLbl, finishedSkippedLbl, finishedSucceededLbl);
    
//    readyLbl.setCount(GlobalState.);
//    processingLbl.setCount(200);
//    blockedLbl.setCount(20);
//    errorFailedLbl.setCount(350);
//    errorTimeoutLbl.setCount(50);
//    finishedSkippedLbl.setCount(450);
//    finishedSucceededLbl.setCount(25000);
//    
//    vbox.getChildren().addAll(readyLbl, processingLbl, blockedLbl, 
//        errorFailedLbl, errorTimeoutLbl, finishedSkippedLbl, finishedSucceededLbl);
    
    return vbox;
  }

  /**
   * A panel that contains a number of ResponderMsgCountLabel instances - one
   * for each responder detected in the system.
   *
   * @return
   */
  private Node createResponderCountsPanel()
  {
    VBox vbox = new VBox();
    vbox.setPadding(new Insets(10, 10, 10, 10));
    vbox.setSpacing(10);
    Text title = new Text("Responder states");
    title.setFont(Font.font("Amble CN", FontWeight.BOLD, 14));
    vbox.getChildren().add(title);
    
    //Add responders here.
    ResponderMsgCountLabel responderLbl = new ResponderMsgCountLabel();
    vbox.getChildren().add(responderLbl);
    
    responderLbl = new ResponderMsgCountLabel();
    responderLbl.setResponderName("BLAST-1.0");
    responderLbl.setCount(384);
    vbox.getChildren().add(responderLbl);
    
    responderLbl = new ResponderMsgCountLabel();
    responderLbl.setResponderName("SignalP-1.0");
    responderLbl.setCount(12);
    vbox.getChildren().add(responderLbl);

    
    responderLbl = new ResponderMsgCountLabel();
    responderLbl.setResponderName("InterPro Scan 1.0");
    responderLbl.setCount(12345);
    vbox.getChildren().add(responderLbl);
    
    return vbox;
  }
  
  /**
   * A button panel shown at the bottom of the window.
   *
   * @return
   */
  private Node createButtonPanel()
  {
    HBox hbox = new HBox();
    hbox.setPadding(new Insets(15, 12, 15, 12));
    hbox.setSpacing(10);
    hbox.setStyle("-fx-background-color: #336699");

    Button refresh = new Button("Refresh");


    HBox mbLabelBox = new HBox();
    final Label microbaseLabel = new Label("Microbase 2.0");
    mbLabelBox.setAlignment(Pos.CENTER_RIGHT); // Right-justify nodes in mbBox
    mbLabelBox.getChildren().add(microbaseLabel);
    HBox.setHgrow(mbLabelBox, Priority.ALWAYS);  // Give mbBox any extra space

    hbox.getChildren().addAll(refresh, mbLabelBox);



    return hbox;
  }
}
