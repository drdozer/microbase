/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 11, 2012, 5:03:02 PM
 */
package uk.org.microbase.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 *
 * @author Keith Flanagan
 */
public class ResponderMsgCountLabel
    extends HBox
{

//  private String responderName;
//  private int count;
  
  private final Label responderNameLbl;
  
  private final StackPane stack;
  private final Rectangle countIcon;
  private final Label countText;

  public ResponderMsgCountLabel()
  {
    setPadding(new Insets(2, 2, 2, 2));
    setSpacing(10);
//    setStyle("-fx-background-color: #D5F1FE"); //Light-blue-ish
    setStyle("-fx-background-color: #BAD7FD"); //Mac OSX text highlight blue

    //Label for holding responder name
    responderNameLbl = new Label();
    responderNameLbl.setAlignment(Pos.CENTER_LEFT);
    getChildren().add(responderNameLbl);
    
    //Create box for numerical count
    
//    HBox countBox = new HBox();
    
//    countText = new Label();
//    countBox.setAlignment(Pos.CENTER_RIGHT); // Right-justify nodes in countBox
//    countBox.getChildren().add(countText);
//    HBox.setHgrow(countBox, Priority.ALWAYS);  // Give countBox any extra space
    
    
    stack = new StackPane();
    countIcon = new Rectangle(35.0, 25.0);
    countIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
        new Stop[]
        {
          new Stop(0, Color.web("#4977A3")), 
          new Stop(0.5, Color.web("#B0C6DA")), 
          new Stop(1, Color.web("#9CB6CF")),
        }));
    countIcon.setStroke(Color.web("#D0E6FA"));
    countIcon.setArcHeight(3.5);
    countIcon.setArcWidth(3.5);

    countText = new Label();
    countText.setAlignment(Pos.CENTER);
    countText.setFont(Font.font("Amble Cn", FontWeight.BOLD, 18));
//    countText.setFill(Color.WHITE);
//    countText.setStroke(Color.web("#7080A0"));
    stack.getChildren().addAll(countIcon, countText);
    
    stack.setAlignment(Pos.CENTER_RIGHT);   // Right-justify nodes in stack 
    HBox.setHgrow(stack, Priority.ALWAYS);  // Give stack any extra space 
    getChildren().add(stack);
    
    //set intial values
    setResponderName(null);
    setCount(-1);
  }

//  public int getCount()
//  {
//    return count;
//  }

  public void setCount(int count)
  {
//    this.count = count;
    
    if (count < 0)
    {
      countText.setText("N/A");
    }
    else
    {
      countText.setText(String.valueOf(count));
    }
    
    System.out.println(stack.getBoundsInParent().getWidth());
    countIcon.setWidth(stack.getBoundsInParent().getWidth() + 4);
  }

//  public String getResponderName()
//  {
//    return responderName;
//  }

  public void setResponderName(String responderName)
  {
    if (responderName == null)
    {
      responderNameLbl.setText("UNKNOWN");
    }
    else
    {
      responderNameLbl.setText(responderName);
    }
    countIcon.setWidth(stack.getBoundsInParent().getWidth() + 4);
  }
  
  
}
