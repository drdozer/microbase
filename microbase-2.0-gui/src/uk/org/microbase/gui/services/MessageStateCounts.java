/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 14, 2012, 11:36:05 PM
 */
package uk.org.microbase.gui.services;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * A bean that can be storing counts in each Message.State value. For example, 
 * the message state counts for different responders could be stored in different
 * instances of this bean. Alternatively, global message states could be stored.
 * 
 * Listeners can be registered to receive property changes. 
 * 
 * @author Keith Flanagan
 */
public class MessageStateCounts
{

  private int ready;
  private int processing;
  private int blocked;
  private int errorFailed;
  private int errorTimeout;
  private int finishedSkipped;
  private int finishedSucceeded;
  
  public MessageStateCounts()
  {
    ready = -1;
    processing = -1;
    blocked = -1;
    errorFailed = -1;
    errorTimeout = -1;
    finishedSkipped = -1;
    finishedSucceeded = -1;
  }
  

  private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  public void addPropertyChangeListener(
      PropertyChangeListener listener)
  {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }

  /**
   * Remove PropertyChangeListener.
   *
   * @param listener
   */
  public void removePropertyChangeListener(
      PropertyChangeListener listener)
  {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  public void fireChanged()
  {
    propertyChangeSupport.firePropertyChange(null, null, null);
  }

  public int getBlocked()
  {
    return blocked;
  }

  public void setBlocked(int blocked)
  {
    this.blocked = blocked;
  }

  public int getErrorFailed()
  {
    return errorFailed;
  }

  public void setErrorFailed(int errorFailed)
  {
    this.errorFailed = errorFailed;
  }

  public int getErrorTimeout()
  {
    return errorTimeout;
  }

  public void setErrorTimeout(int errorTimeout)
  {
    this.errorTimeout = errorTimeout;
  }

  public int getFinishedSkipped()
  {
    return finishedSkipped;
  }

  public void setFinishedSkipped(int finishedSkipped)
  {
    this.finishedSkipped = finishedSkipped;
  }

  public int getFinishedSucceeded()
  {
    return finishedSucceeded;
  }

  public void setFinishedSucceeded(int finishedSucceeded)
  {
    this.finishedSucceeded = finishedSucceeded;
  }

  public int getProcessing()
  {
    return processing;
  }

  public void setProcessing(int processing)
  {
    this.processing = processing;
  }

  public int getReady()
  {
    return ready;
  }

  public void setReady(int ready)
  {
    this.ready = ready;
  }
  
  
}
