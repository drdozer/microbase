/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 14, 2012, 11:36:05 PM
 */
package uk.org.microbase.gui.services;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * A bean that can be storing counts in each Message.State value. For example, 
 * the message state counts for different responders could be stored in different
 * instances of this bean. Alternatively, global message states could be stored.
 * 
 * Listeners can be registered to receive property changes. 
 * 
 * @author Keith Flanagan
 */
public class MessageStateCounts_prop
{

  private final IntegerProperty ready;
  private final IntegerProperty processing;
  private final IntegerProperty blocked;
  private final IntegerProperty errorFailed;
  private final IntegerProperty errorTimeout;
  private final IntegerProperty finishedSkipped;
  private final IntegerProperty finishedSucceeded;

  public MessageStateCounts_prop()
  {
    ready = new SimpleIntegerProperty(-1);
    processing = new SimpleIntegerProperty(-1);
    blocked = new SimpleIntegerProperty(-1);
    errorFailed = new SimpleIntegerProperty(-1);
    errorTimeout = new SimpleIntegerProperty(-1);
    finishedSkipped = new SimpleIntegerProperty(-1);
    finishedSucceeded = new SimpleIntegerProperty(-1);
  }

  public IntegerProperty getBlocked()
  {
    return blocked;
  }

  public IntegerProperty getErrorFailed()
  {
    return errorFailed;
  }

  public IntegerProperty getErrorTimeout()
  {
    return errorTimeout;
  }

  public IntegerProperty getFinishedSkipped()
  {
    return finishedSkipped;
  }

  public IntegerProperty getFinishedSucceeded()
  {
    return finishedSucceeded;
  }

  public IntegerProperty getProcessing()
  {
    return processing;
  }

  public IntegerProperty getReady()
  {
    return ready;
  }
  
  
}
