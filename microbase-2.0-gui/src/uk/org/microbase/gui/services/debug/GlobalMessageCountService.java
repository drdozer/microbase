/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 14, 2012, 8:59:29 AM
 */

package uk.org.microbase.gui.services.debug;

import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import uk.org.microbase.gui.services.MessageStateCounts;
import uk.org.microbase.gui.services.GlobalState;

/**
 * For each Message.State, retrieves the current message counts across all
 * responders. When the values have been obtained, an event is fired.
 * 
 * @author Keith Flanagan
 * @deprecated this service provides fake data for testing purposes only!
 */
public class GlobalMessageCountService
    extends Service
{
  private int ready;
  private int processing;
  private int blocked;
  private int errorFailed;
  private int errorTimeout;
  private int finishedSkipped;
  private int finishedSucceeded;
  
  
  private static final Logger logger = 
      Logger.getLogger(GlobalMessageCountService.class.getSimpleName());

  public GlobalMessageCountService()
  {
    logger.info("Creating service");
    
//    GlobalMessageCountState state = GlobalState.instance().globalMessageCountState;
//    state.getReady().set(5);
//    state.getProcessing().set(10);
//    state.getBlocked().set(100);
//    state.getErrorFailed().set(1000);
//    state.getErrorTimeout().set(10000);
//    state.getFinishedSkipped().set(100000);
//    state.getFinishedSucceeded().set(1000000);
   
//    GlobalMessageCountState1 state = GlobalState.instance().globalMessageCountState1;
//    state.setReady(1);
//    state.setProcessing(10);
//    state.setBlocked(100);
//    state.setErrorFailed(1000);
//    state.setErrorTimeout(10000);
//    state.setFinishedSkipped(100000);
//    state.setFinishedSucceeded(1000000);
    
  }
  
  

  @Override
  protected Task createTask()
  {
    logger.info("Creating a new task");
    
    Task<Void> task = new Task<Void>() {
      @Override
      protected Void call()
          throws Exception
      {
        MessageStateCounts state = GlobalState.instance().globalMessageCountState2;
        ready++;
        processing++;
        blocked++;
        errorFailed++;
        errorTimeout++;
        finishedSkipped++;
        finishedSucceeded++;
        
        logger.info("Sleeping");
        Thread.sleep(1000);
        
        
        state.setReady(ready);
        state.setProcessing(processing);
        state.setBlocked(blocked);
        state.setErrorFailed(errorFailed);
        state.setErrorTimeout(errorTimeout);
        state.setFinishedSkipped(finishedSkipped);
        state.setFinishedSucceeded(finishedSucceeded);
        
        logger.info("State updated");
        
        return null;
      }
    };
    return task;
  }

}
