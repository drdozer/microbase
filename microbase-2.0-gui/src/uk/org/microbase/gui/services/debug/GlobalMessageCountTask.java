/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Apr 14, 2012, 8:59:29 AM
 */

package uk.org.microbase.gui.services.debug;

import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import uk.org.microbase.gui.services.MessageStateCounts;
import uk.org.microbase.gui.services.GlobalState;
import uk.org.microbase.gui.services.MessageStateCounts_prop;

/**
 * For each Message.State, retrieves the current message counts across all
 * responders. When the values have been obtained, an event is fired.
 * 
 * @author Keith Flanagan
 * @deprecated this service provides fake data for testing purposes only!
 */
public class GlobalMessageCountTask
    extends Task<Void>
{
  
  
  private static final Logger logger = 
      Logger.getLogger(GlobalMessageCountTask.class.getSimpleName());

  public GlobalMessageCountTask()
  {
    logger.info("Creating task");
    
    
  }
  
  

  @Override
  protected Void call()
      throws Exception
  {
        MessageStateCounts_prop state = GlobalState.instance().globalMessageCountState;
        
        
        
        logger.info("Sleeping");
        Thread.sleep(1000);
        
        System.out.println("Old state was: "+state.getReady());
        state.getReady().setValue(state.getReady().getValue() + 1);
        state.getProcessing().setValue(state.getProcessing().getValue() + 1);
//        state.setBlocked(state.getBlocked() + 1);
//        state.setErrorFailed(state.getErrorFailed() + 1);
//        state.setErrorTimeout(state.getErrorTimeout() + 1);
//        state.setFinishedSkipped(state.getFinishedSkipped() + 1);
//        state.setFinishedSucceeded(state.getFinishedSucceeded() + 1);
        
        logger.info("State updated - make sure you notify appropriate listeners");
        return null;
  }

}
