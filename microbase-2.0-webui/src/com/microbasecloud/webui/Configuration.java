/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 10 Jun 2012, 20:40:51
 */
package com.microbasecloud.webui;

import java.io.Serializable;

import uk.org.microbase.runtime.ClientRuntime;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Panel;

/**
 * 
 * @author Keith Flanagan
 */
public class Configuration
		implements Serializable {
	private ClientRuntime runtime;
	
	private final WebuiElements webui;

	public Configuration(WebuiElements webui) {
		this.webui = webui;
	}

	public ClientRuntime getRuntime() {
		return runtime;
	}

	public void setRuntime(ClientRuntime runtime) {
		this.runtime = runtime;
	}

	public WebuiElements getWebui() {
		return webui;
	}


	
	
}
