package com.microbasecloud.webui;

import java.util.Date;

import com.microbasecloud.webui.plugins.debug.DebugToolkitPlugin;
import com.microbasecloud.webui.plugins.notification.NotificationSystemPlugin;
import com.microbasecloud.webui.plugins.summary.SummaryRootPane;
import com.microbasecloud.webui.plugins.summary.SystemSummaryPlugin;
import com.vaadin.Application;
import com.vaadin.ui.*;

public class MicrobaseApplication
    extends Application {

  private static final WebuiPlugin[] PLUGINS = new WebuiPlugin[] {
    new SystemSummaryPlugin(), new NotificationSystemPlugin(),
    new DebugToolkitPlugin()
  };
  
  private Window mainWindow;

  @Override
  public void init() {
    //		setTheme("test");
    setTheme("chameleon");

    mainWindow = new Window("Microbase 2.0");
    setMainWindow(mainWindow);

    /*
     * Layout components
     */
    //		String northPanelBounds = "left: 5px; right: 5px; top: 5px;";
    //		String centerPanelBounds = "left: 0px; right: 0px; top: 60px; bottom: 60px;";
    //		String southPanelBounds = "left: 0px; right:0px; bottom: 0px;";

    //		MainPanel rootLayout = new MainPanel();

    // Set the root layout (VerticalLayout is the default).
    AbsoluteLayout rootLayout = new AbsoluteLayout();
    //		rootLayout.setSizeFull();
    mainWindow.setContent(rootLayout);
    AbsoluteLayout contentPanel = new AbsoluteLayout();
    WebuiElements webui = new WebuiElementsImpl(mainWindow, rootLayout, contentPanel);
    Configuration config = new Configuration(webui);
    contentPanel.setHeight("100%");
    
    
    contentPanel.addComponent(new WelcomePanel(config));
    rootLayout.addComponent(contentPanel,
        "left: 155px; right: 5px; top: 75px; bottom: 150px;");

    NorthPanel north = new NorthPanel(config);
    rootLayout.addComponent(north, "left: 5px; right: 5px; top: 5px;");

    LeftMenu menu = new LeftMenu(config);
    menu.setWidth("150px");
    rootLayout.addComponent(menu, "left: 5px; top: 75px; bottom: 150px;");

    SouthPanel south = new SouthPanel();
    south.setHeight("150px");
    rootLayout.addComponent(south, "left: 0px; right:0px; bottom: 0px;");
    try {
      south.setConfiguration(config);
    }
    catch (WebUiException e) {
      e.printStackTrace();
      config.getWebui().showException("Failed to initialise log panel", e);
    }

    
    for (WebuiPlugin plugin : PLUGINS)
    {
      plugin.setConfiguration(config);
      menu.addMenuSection(plugin.getMenuComponent(), plugin.getMenuComponentTitle()); 
    }
    
    
    //		Button buttRight = new Button( "right: 0px; bottom: 100px;");
    //		rootLayout.addComponent(buttRight, "right: 0px; bottom: 100px;");

    // Add the topmost component.
    //		Label mbLabel = new Label("Microbase 2.0");
    //		rootLayout.addComponent(mbLabel, "left: 0px; top: 0px;");
    //		Label dateLabel = new Label(new Date(System.currentTimeMillis()).toString());
    //		rootLayout.addComponent(dateLabel);

    //		Panel leftMenuPanel = new Panel();
    //		LeftMenu leftMenu = new LeftMenu();
    //		leftMenu.setSizeFull();
    //		leftMenuPanel.addComponent(leftMenu);
    //		leftMenuPanel.setSizeFull();
    //		rootLayout.addComponent(leftMenuPanel,  "left: 0px; right: 50px; "+
    //    "top: 100px; bottom: 50px;");

    // Add a horizontal layout for the bottom part.
    //		HorizontalLayout bottom = new HorizontalLayout();
    //		HorizontalSplitPanel bottom = new HorizontalSplitPanel();
    ////		bottom.setMargin(true);
    //		bottom.setSizeFull();
    //		
    //		rootLayout.addComponent(bottom, centerPanelBounds);
    //		
    //		
    //		LeftMenu leftMenu = new LeftMenu();
    //		bottom.addComponent(leftMenu); 
    //		bottom.addComponent(new SummaryRootPane());
    //		bottom.addComponent(new Label("foo"));

    //		bottom.setExpandRatio(leftMenu, 1.0f); // use all available space

    //		TestComponent testComponent = new TestComponent();
    //		rootLayout.addComponent(testComponent);
    //		

    //		LeftMenu leftMenu =  new LeftMenu();
    //		mainWindow.addComponent(leftMenu);

    //		mainWindow.getContent().setSizeFull();
  }

  //	@Override
  //	public void init() {
  ////		setTheme("test");
  //		
  //		mainWindow = new Window("Microbase 2.0");
  //		setMainWindow(mainWindow);
  //
  //		/*
  //		 * https://vaadin.com/book/-/page/eclipse.html
  //		 * You can use a composite component as you would use any Vaadin 
  //		 * component. Just remember that the component as well as its root 
  //		 * layout, which is an AbsoluteLayout, are 100% wide and high by 
  //		 * default. A component with full size (expand-to-fit container) may 
  //		 * not be inside a layout with undefined size (shrink-to-fit content). 
  //		 * The default root layout for Window is a  VerticalLayout, which by 
  //		 * default has undefined height, so you have to set it explicitly to a 
  //		 * defined size, usually to full (100%) height.
  //		 */
  //		// Needed because composites are full size
  ////		mainWindow.getContent().setSizeFull();
  //
  //		/*
  //		 * Layout components
  //		 */
  //		
  //	  // Set the root layout (VerticalLayout is actually the default).
  //		VerticalLayout rootLayout = new VerticalLayout();
  ////		rootLayout.setSizeFull();
  //		mainWindow.setContent(rootLayout);
  //		 
  //		// Add the topmost component.
  //		Label mbLabel = new Label("Microbase 2.0");
  //		mbLabel.setHeight("15");
  //		rootLayout.addComponent(mbLabel);
  //		Label dateLabel = new Label(new Date(System.currentTimeMillis()).toString());
  //		rootLayout.addComponent(dateLabel);
  //		
  //		
  //		AbsoluteLayout bottomLayout = new AbsoluteLayout();
  //	  // Add a horizontal layout for the bottom part.
  ////		HorizontalLayout bottom = new HorizontalLayout();
  //		HorizontalSplitPanel bottom = new HorizontalSplitPanel();
  ////		bottom.setMargin(true);
  //		bottom.setSizeFull();
  ////		bottom.setHeight("100%");
  //		bottomLayout.addComponent(bottom, "left: 25px; right: 50px; "+
  //        "top: 100px; bottom: 50px;");
  //		
  //		rootLayout.addComponent(bottomLayout);
  //		
  //		
  //		LeftMenu leftMenu = new LeftMenu();
  //		bottom.addComponent(leftMenu); 
  //		bottom.addComponent(new SummaryRootPane());
  //		
  ////		bottom.setExpandRatio(leftMenu, 1.0f); // use all available space
  //		
  //		rootLayout.addComponent(new TestComponent());
  //		
  ////		LeftMenu leftMenu = new LeftMenu();
  ////		mainWindow.addComponent(leftMenu);
  //		
  //		
  ////		mainWindow.getContent().setSizeFull();
  //	}

}
