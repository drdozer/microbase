/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 21 Jun 2012, 22:43:07
 */
package com.microbasecloud.webui;

import java.util.logging.Logger;

import uk.org.microbase.dist.responder.ResponderInfo;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;

/**
 * 
 * @author Keith Flanagan
 */
public class ResponderInfoPrinter
  implements EntryListener<String, ResponderInfo>
{

  private static final Logger logger = 
      Logger.getLogger(ResponderInfoPrinter.class.getName());
 
  private final SouthPanel parent;
  
  public ResponderInfoPrinter(SouthPanel parent)
  {
  	this.parent = parent;
  }
 
  @Override
  public void entryAdded(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder REGISTERED: "+ee.getValue().toString());
    parent.append("Responder REGISTERED: "+ee.getValue().toString());
  }

  @Override
  public void entryRemoved(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder DE-REGISTERED: "+ee.getValue().toString());
    parent.append("Responder DE-REGISTERED: "+ee.getValue().toString());
  }

  @Override
  public void entryUpdated(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder UPDATED: "+ee.getValue().toString());
    parent.append("Responder UPDATED: "+ee.getValue().toString());
  }

  @Override
  public void entryEvicted(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder EVICTED: "+ee.getValue().toString());
    parent.append("Responder EVICTED: "+ee.getValue().toString());
  }

}
