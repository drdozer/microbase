/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.microbasecloud.webui;

import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;

/**
 * 
 * @author Keith Flanagan
 */
public interface WebuiElements {
	public void setMainContentItem(Component panel, String title);
	public void addMenuSection(Panel panel, String title);
	
	public void addPanelToRoot(Panel panel, Integer pxFromTop, Integer pxFromBottom,
			Integer pxFromLeft, Integer pxFromRight);
			
	public void showNotification(String title, String content);
	public void showTrayNotification(String title, String content);
	public void showError(String title, String content);
	public void showException(String title, Throwable t);
}
