package com.microbasecloud.webui;

import java.io.Serializable;

import uk.org.microbase.util.ExceptionUtils;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class WebuiElementsImpl
    implements WebuiElements, Serializable {

  private final Window mainWindow;
  private final AbsoluteLayout rootLayout;
  private final AbsoluteLayout contentPanel;
  private LeftMenu menu;

  public WebuiElementsImpl(Window mainWindow, AbsoluteLayout rootLayout, AbsoluteLayout contentPanel) {
    this.mainWindow = mainWindow;
    this.rootLayout = rootLayout;
    this.contentPanel = contentPanel;
  }

  @Override
  public void setMainContentItem(Component component, String title) {
    contentPanel.removeAllComponents();
    Label titleLabel = new Label("<h2>"+title+"</h2>");
    titleLabel.setContentMode(Label.CONTENT_XHTML);
    titleLabel.setStyleName(".v-label-h1");
    contentPanel.addComponent(titleLabel, "left:0px;right:0px;top:0px;bottom:30px;");
    contentPanel.addComponent(component, "left:0px;right:0px;top:32px;bottom:0px;");
//    contentPanel.setCaption(title);
  }

  @Override
  public void addMenuSection(Panel panel, String title) {
    menu.addMenuSection(panel, title);
  }

  @Override
  public void addPanelToRoot(Panel panel, Integer pxFromTop,
      Integer pxFromBottom, Integer pxFromLeft, Integer pxFromRight) {
    StringBuilder layout = new StringBuilder();
    if (pxFromTop != null) {
      layout.append("top: ").append(pxFromTop).append("px; ");
    }
    if (pxFromBottom != null) {
      layout.append("bottom: ").append(pxFromBottom).append("px; ");
    }
    if (pxFromLeft != null) {
      layout.append("left: ").append(pxFromLeft).append("px; ");
    }
    if (pxFromRight != null) {
      layout.append("right: ").append(pxFromRight).append("px; ");
    }

    rootLayout.addComponent(panel, "left: 5px; right: 5px; top: 5px;");
  }

  @Override
  public void showNotification(String title, String content) {
    mainWindow.showNotification(title, content);
  }
  
  @Override
  public void showTrayNotification(String title, String content) {
    Window.Notification notif = new Window.Notification(
        title, content,
        Window.Notification.TYPE_TRAY_NOTIFICATION);
    notif.setPosition(Window.Notification.POSITION_TOP_RIGHT);
    notif.setDelayMsec(7000);
    mainWindow.showNotification(notif);
  }

  @Override
  public void showError(String title, String content) {
    mainWindow.showNotification(title, content, 
        Window.Notification.TYPE_ERROR_MESSAGE);
  }

  @Override
  public void showException(String title, Throwable t) {
    showError(title,
        "<b>Exception</b><br/>" +
        "<pre>"+ExceptionUtils.throwableToString(t)+"</pre>");
  }

}
