/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 12 Jun 2012, 15:35:13
 */
package com.microbasecloud.webui;

import com.vaadin.ui.Component;

/**
 * 
 * @author Keith Flanagan
 */
public interface WebuiPlugin
    extends Configurable {
  public void setConfiguration(Configuration config);
  public Component getMenuComponent();
  public String getMenuComponentTitle();
  
}
