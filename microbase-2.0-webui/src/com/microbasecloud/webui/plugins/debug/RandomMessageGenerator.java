/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 14 Jun 2012, 12:26:30
 */
package com.microbasecloud.webui.plugins.debug;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uk.org.microbase.notification.data.Message;
import uk.org.microbase.util.UidGenerator;

import com.microbasecloud.webui.Configuration;

/**
 * A utility class for creating and publishing test message tree.
 * 
 * @author Keith Flanagan
 */
public class RandomMessageGenerator {
  private Configuration config;
  private int contentCount = 0;
  private int valueCount = 0;
  
  public RandomMessageGenerator(Configuration config) {
    this.config = config;
  }

  public Set<Message> generate(int msgsPerLevel, int depth) {
    Set<Message> msgs = new HashSet<Message>();
    String workflowExeId = "debug-"+System.currentTimeMillis();
    
    msgs.addAll(generateTreeLevel(null, msgsPerLevel, 0, depth, workflowExeId));

    return msgs;
  }
  
  private Set<Message> generateTreeLevel(String parentMsgId,
      int numMessagesInLevel, int level, int maxDepth, String workflowExeId)
  {
    Set<Message> msgs = new HashSet<Message>();
    if (level > maxDepth)
    {
      return msgs;
    }
    
    
    for (int i = 0; i < numMessagesInLevel; i++) {
      Message root = new Message();
      root.setBinaryContent(null);
      root.setContent(genContent());
      root.setExpiresTimestamp(-1);
      root.setGuid(UidGenerator.generateUid());
      root.setParentMessage(parentMsgId);
      root.setPublisherGuid("debug tool");
      root.setTopicGuid("debug-test-topic-level:"+level);
      root.setUserDescription("Test message");
      root.setWorkflowExeId(workflowExeId);
      root.setWorkflowStepId(workflowExeId+"-level:"+level+"-msg"+i);
      
      msgs.add(root);
      
      
      //Generate sublevel
      msgs.addAll(generateTreeLevel(root.getGuid(), 
          numMessagesInLevel, level+1, maxDepth, workflowExeId));
    }
    
    return msgs;
  }
  
  private Map<String, String> genContent()
  {
    Map<String, String> content = new HashMap<String, String>();
    for (int i=0; i<10; i++)
    {
      content.put("item "+contentCount, "value: "+valueCount);
      contentCount++;
      valueCount++;
    }
    return content;
  }
}
