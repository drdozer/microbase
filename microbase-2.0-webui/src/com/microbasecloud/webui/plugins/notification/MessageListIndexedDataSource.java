/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 10 Jun 2012, 22:10:26
 */
package com.microbasecloud.webui.plugins.notification;

import java.util.Date;
import java.util.List;

import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebUiException;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;

/**
 * 
 * @author Keith Flanagan
 */
public class MessageListIndexedDataSource
    extends IndexedContainer {

  static final String COL_BINARY_CONTENT = "Binary content";
  static final String COL_CONTENT = "Content";
  static final String COL_EXPIRES_DATE = "Expires";
  static final String COL_GUID = "GUID";  
  static final String COL_PARENT = "Parent GUID";
  static final String COL_PUBLISHED_DATE = "Published";
  static final String COL_PUBLISHER = "Publisher";
  static final String COL_TOPIC = "Topic";
  static final String COL_USER_DESCR = "Description";
  static final String COL_WORKFLOW_EXE = "Workflow exe ID";
  static final String COL_WORKFLOW_STEP = "Workflow step ID";
  
  private final Configuration config;
  
  public MessageListIndexedDataSource(Configuration config)
      throws WebUiException {
    this.config = config;
    addContainerProperty(COL_PUBLISHED_DATE, Date.class, null);
    addContainerProperty(COL_TOPIC, String.class, null);
    addContainerProperty(COL_GUID, String.class, null);
    addContainerProperty(COL_PARENT, String.class, null);
    addContainerProperty(COL_PUBLISHER, String.class, null);
    addContainerProperty(COL_WORKFLOW_EXE, String.class, null);
    addContainerProperty(COL_WORKFLOW_STEP, String.class, null);
    addContainerProperty(COL_USER_DESCR, String.class, null);

    refresh();
  }

  public void refresh() 
      throws WebUiException {
    ClientRuntime runtime = config.getRuntime();
    List<Message> messages = null;
    try
    {
      messages = runtime.getNotification().
          listMessagesInTimestampOrder(0, Integer.MAX_VALUE);
    }
    catch(Exception e)
    {
      throw new WebUiException("Failed to obtain a list of responders", e);
    }
    
    System.out.println("Found "+messages.size()+" messages");
    
    int id = 0;
    for (Message msg : messages)
    {
      Item item = addItem(id++);
      item.getItemProperty(COL_PUBLISHED_DATE).setValue(new Date(msg.getPublishedTimestamp()));
      item.getItemProperty(COL_TOPIC).setValue(msg.getTopicGuid());
      item.getItemProperty(COL_GUID).setValue(msg.getGuid());
      item.getItemProperty(COL_PARENT).setValue(msg.getParentMessage());
      item.getItemProperty(COL_PUBLISHER).setValue(msg.getPublisherGuid());
      item.getItemProperty(COL_WORKFLOW_EXE).setValue(msg.getWorkflowExeId());
      item.getItemProperty(COL_WORKFLOW_STEP).setValue(msg.getWorkflowStepId());      
    }
    
    sort(new Object[] { COL_PUBLISHED_DATE }, new boolean[] { true });
  }
  

}
