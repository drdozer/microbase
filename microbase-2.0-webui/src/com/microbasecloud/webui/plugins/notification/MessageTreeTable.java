/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 14 Jun 2012, 12:00:28
 */
package com.microbasecloud.webui.plugins.notification;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.runtime.ClientRuntime;

import com.microbasecloud.webui.Configurable;
import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebUiException;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
//import com.vaadin.event.MouseEvents.ClickEvent;
import com.vaadin.data.Container.ItemSetChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.Tree.ExpandListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Tree.ExpandEvent;
import com.vaadin.ui.Window;

/**
 * 
 * @author Keith Flanagan
 */
public class MessageTreeTable
  extends TreeTable
  implements Configurable {
  /**
   * Load message content from the database as well as metadata
   */
  private static final boolean LOAD_CONTENT = false;
  /**
   * Expand tree nodes by default when they are added
   */
  private static final boolean ON_ADD_EXPAND = false;

  private Configuration config;
  private MicrobaseNotification notification;

  private final Map<String, Object> msgIdToTableId;

  public MessageTreeTable()
  {
	  this.msgIdToTableId = new HashMap<String, Object>();
	  
//	  addContainerProperty(NAME_PROPERTY, String.class, "");
//	  addContainerProperty(HOURS_PROPERTY, Integer.class, 0);
//	  addContainerProperty(MODIFIED_PROPERTY, Date.class, cal.getTime());
	  
//	  set
  
    addContainerProperty(MessageListIndexedDataSource.COL_PUBLISHED_DATE, Date.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_TOPIC, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_GUID, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_PARENT, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_PUBLISHER, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_WORKFLOW_EXE, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_WORKFLOW_STEP, String.class,  null);
    addContainerProperty(MessageListIndexedDataSource.COL_USER_DESCR, String.class,  null);
      
    setSelectable(true);
    setMultiSelect(true);
    setColumnCollapsingAllowed(true);
    setColumnReorderingAllowed(true);
    
    /*
     * When nodes are expanded, load their children
     */
    addListener(new ExpandListener() {
      @Override
      public void nodeExpand(ExpandEvent event) {
        try
        {
          //The item ID here is the message GUID
          System.out.println("tree node expanded: "+event.getItemId());
          loadChildMessages((String) event.getItemId());
        }
        catch(Exception e)
        {
          e.printStackTrace();
          config.getWebui().showException("Failed to expand tree node", e);
        }
      }
    });
    
    addListener(new ItemClickListener() {
      @Override
      public void itemClick(ItemClickEvent event)
      {
        try
        {
          //The item ID here is the message GUID
          
          if (event.isDoubleClick())
          {
          	System.out.println("Item clicked: "+event.getItemId());
            displayMessage((String) event.getItemId());  
          }
          
        }
        catch(Exception e)
        {
          e.printStackTrace();
          config.getWebui().showException("Failed to expand tree node", e);
        }
      }
    });
  }

  public void setConfiguration(Configuration config) throws WebUiException {
    this.config = config;
    this.notification = config.getRuntime().getNotification();
    /*
     * Note - we could use the MessageListIndexedDataSource implementation here,
     * but no hierarchy information would be set
     */
    // setContainerDataSource(new MessageListIndexedDataSource(config));

    /*
     * Instead, initially load the root elements - let the 'collapse/expand'
     * listener handle loading the children
     */
    loadRootMessages();
  }

  /**
   * Called to load the initial tree roots (Messages whose parent ID == null)
   * 
   * @throws WebUiException
   */
  private void loadRootMessages() throws WebUiException {
    ClientRuntime runtime = config.getRuntime();
    List<Message> messages = null;
    try {
      messages = runtime.getNotification().listRootMessages(LOAD_CONTENT, 0,
          Integer.MAX_VALUE);
      addMessages(messages);
    } catch (Exception e) {
      throw new WebUiException("Failed to obtain a root message(s)", e);
    }
  }

  /**
   * Given a message ID, loads its children.
   * 
   * @param parentMsgId
   * @throws WebUiException
   */
  private void loadChildMessages(String parentMsgId) throws WebUiException {
    List<Message> messages = null;
    try {
      messages = notification.listChildMessages(
          parentMsgId, LOAD_CONTENT, 0, Integer.MAX_VALUE);
      addMessages(messages);
    } catch (Exception e) {
      throw new WebUiException("Failed to obtain a child message(s) for: ", e);
    }
  }

  /**
   * Adds a collection of Messages to the table and sets their place in the
   * heirarchy
   * 
   * @param msgs
   *          the message(s) to add to the table
   * @param parentId
   *          the partent ID of the messages being added.
   */
  private void addMessages(Collection<Message> msgs) {
    for (Message msg : msgs) {
      Object tableItemId = addItem(
          new Object[] { new Date(msg.getPublishedTimestamp()),
              msg.getTopicGuid(), msg.getGuid(), msg.getParentMessage(),
              msg.getPublisherGuid(), msg.getWorkflowExeId(),
              msg.getWorkflowStepId(), msg.getUserDescription() },
          // Use Message GUID as the TreeTable Item ID
          msg.getGuid());

      // System.out.println("Setting item expanded: "+ON_ADD_EXPAND);
      setCollapsed(tableItemId, !ON_ADD_EXPAND);

      msgIdToTableId.put(msg.getGuid(), tableItemId);
      // System.out.println("MessageID: "+msg.getGuid()+", itemID: "+tableItemId);

      if (msg.getParentMessage() != null) {
        setParent(tableItemId, msg.getParentMessage());
      }
    }
  }
  
  /**
   * Given a Message GUID, displays its content in a pop-up window.
   * @param msgId
   * @throws WebUiException
   */
  private void displayMessage(String msgId) 
      throws WebUiException {
    try {
      Message msg = notification.getMessage(msgId);
      
      final Window detailsWindow = new Window(
      		"Message details: "+msg.getTopicGuid()+": "+msg.getGuid());
      // Configure the windws layout; by default a VerticalLayout
      VerticalLayout layout = (VerticalLayout) detailsWindow.getContent();
      layout.setMargin(true);
      layout.setSpacing(true);

      // Add some content; a form, a big TextArea for message content and a close-button
      
      /*
       * The Form and content text area are added to a separate HorizontalLayout
       * which is then added to the master VerticalLayout
       */
      
      HorizontalLayout formAndContentHzLayout = new HorizontalLayout();
      formAndContentHzLayout.setMargin(true);
      formAndContentHzLayout.setSpacing(true);
      Form msgForm = createMsgForm(msg);
      formAndContentHzLayout.addComponent(msgForm);
      
      TextArea contentArea = new TextArea();
      StringBuilder sb = new StringBuilder();
      for (String key: msg.getContent().keySet())
      {
      	sb.append(key).append(" = ").append(msg.getContent().get(key)).append("\n");
      }
      contentArea.setColumns(50);
      contentArea.setValue(sb.toString());
      contentArea.setSizeFull();
      contentArea.setWidth("100%");
      

      formAndContentHzLayout.addComponent(contentArea);
      
//      detailsWindow.addComponent(msgForm);
      detailsWindow.addComponent(formAndContentHzLayout);


      Button close = new Button("Close", new Button.ClickListener() {
          // inline click-listener
          public void buttonClick(ClickEvent event) {
              // close the window by removing it from the parent window
              (detailsWindow.getParent()).removeWindow(detailsWindow);
          }
      });
      // The components added to the window are actually added to the window's
      // layout; you can use either. Alignments are set using the layout
      layout.addComponent(close);
      layout.setComponentAlignment(close, Alignment.TOP_RIGHT);

      
      getWindow().addWindow(detailsWindow);

      
    } catch (Exception e) {
      throw new WebUiException("Failed to obtain a content for message: "+msgId, e);
    }
  }
  
  
  private Form createMsgForm(final Message msg) {
    // Create a form and use FormLayout as its layout. 
    final Form form = new Form();

    // Set form caption and description texts 
    form.setCaption("Message details");
    form.setDescription("Displays all information stored about the selected message");

    // Create the custom bean. 
//    final FormBean bean = new FormBean();

    // Create a bean item that is bound to the bean. 
    BeanItem<Message> item = new BeanItem<Message>(msg);

    // Bind the bean item as the data source for the form. 
    form.setItemDataSource(item);

    // Set the footer layout. 
    form.setFooter(new VerticalLayout());

//    form.getFooter().addComponent(
//        new Label("Click OK to publish the required number of messages. "));

    // Have a button bar in the footer.
    HorizontalLayout okbar = new HorizontalLayout();
    okbar.setHeight("25px");
    form.getFooter().addComponent(okbar);

    // Add an Ok (commit), Reset (discard), and Cancel buttons
    // for the form. 
//    Button okbutton = new Button("OK", form, "commit");
//    okbar.addComponent(okbutton);
//    okbar.setComponentAlignment(okbutton, Alignment.TOP_RIGHT);
    
//    okbutton.addListener(new ClickListener() {
//      @Override
//      public void buttonClick(ClickEvent event) {
//        int msgsPerLevel = bean.getNumMsgsPerLevel();
//        int depth = bean.getDepth();
//        generateAndPublish(msgsPerLevel, depth);
//      }
//    });

    return form;
  }

  // private void manuallyAddMessage(Message msg) {
  // Object tableItemId = addItem(new Object[] {
  // new Date(msg.getPublishedTimestamp()),
  // msg.getTopicGuid(),
  // msg.getGuid(),
  // msg.getParentMessage(),
  // msg.getPublisherGuid(),
  // msg.getWorkflowExeId(),
  // msg.getWorkflowStepId(),
  // msg.getUserDescription() });
  //
  // }

  // private void __example()
  // {
  /*
   * // Set hierarchy treetable.setParent(year2010, allProjects);
   * treetable.setParent(customerProject1, year2010);
   * treetable.setParent(customerProject1Implementation, customerProject1);
   * treetable.setParent(customerProject1Planning, customerProject1);
   * treetable.setParent(customerProject1Prototype, customerProject1);
   * treetable.setParent(customerProject2, year2010);
   * treetable.setParent(customerProject2Planning, customerProject2);
   * 
   * // Disallow children from leaves
   * treetable.setChildrenAllowed(customerProject1Implementation, false);
   * treetable.setChildrenAllowed(customerProject1Planning, false);
   * treetable.setChildrenAllowed(customerProject1Prototype, false);
   * treetable.setChildrenAllowed(customerProject2Planning, false);
   * 
   * // Expand all treetable.setCollapsed(allProjects, false);
   * treetable.setCollapsed(year2010, false);
   * treetable.setCollapsed(customerProject1, false);
   * treetable.setCollapsed(customerProject2, false);
   */
  // }
}
