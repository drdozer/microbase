/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 10 Jun 2012, 22:10:26
 */
package com.microbasecloud.webui.plugins.summary;

import java.util.Collection;
import java.util.Date;

import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.runtime.ClientRuntime;

import com.microbasecloud.webui.Configuration;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;

/**
 * 
 * @author Keith Flanagan
 */
public class ProcessListIndexedDataSource
    extends IndexedContainer {

  static final String COL_PROC_GUID = "PID";
  static final String COL_HOSTNAME = "Hostname";
  static final String COL_RESPONDER_ID = "Responder";
  static final String COL_TOPIC_ID = "Topic";
  static final String COL_MESSAGE_ID = "Message ID";
  static final String COL_LOCAL_CORES = "Assigned cores (local)";
  static final String COL_PERCENT_COMPLETE = "% complete";
  static final String COL_PROC_STARTED_AT = "Proc started";
  static final String COL_WORK_STARTED_AT = "Work started";
  
  private final Configuration config;
  
  public ProcessListIndexedDataSource(Configuration config) {
    this.config = config;
    addContainerProperty(COL_PROC_GUID, String.class, null);
    addContainerProperty(COL_HOSTNAME, String.class, null);
    addContainerProperty(COL_RESPONDER_ID, String.class, null);
    addContainerProperty(COL_TOPIC_ID, String.class, null);
    addContainerProperty(COL_MESSAGE_ID, String.class, null);
    addContainerProperty(COL_LOCAL_CORES, Integer.class, null);
    addContainerProperty(COL_PERCENT_COMPLETE, Float.class, null);
    addContainerProperty(COL_PROC_STARTED_AT, Date.class, null);
    addContainerProperty(COL_WORK_STARTED_AT, Date.class, null);

    refresh();
  }


  public void refresh() {
    ClientRuntime runtime = config.getRuntime();
    Collection<ProcessInfo> procs = runtime.getProcessList().getAllProcesses();
    
    System.out.println("Found "+procs.size()+" running processes");
    
    int id = 0;
    for (ProcessInfo info : procs)
    {
      Item item = addItem(id++);
      item.getItemProperty(COL_PROC_GUID).setValue(info.getProcessGuid());
      item.getItemProperty(COL_HOSTNAME).setValue(info.getHostname());
      item.getItemProperty(COL_RESPONDER_ID).setValue(info.getResponderId());
      item.getItemProperty(COL_TOPIC_ID).setValue(info.getTopicId());
      item.getItemProperty(COL_MESSAGE_ID).setValue(info.getMessageId());
      item.getItemProperty(COL_LOCAL_CORES).setValue(info.getAssignedLocalCores());
      item.getItemProperty(COL_PERCENT_COMPLETE).setValue(info.getPercentComplete());
      item.getItemProperty(COL_PROC_STARTED_AT).setValue(new Date(info.getProcessStartedAtMs()));
      item.getItemProperty(COL_WORK_STARTED_AT).setValue(new Date(info.getWorkStartedAtMs()));
    }
    
    sort(new Object[] { COL_HOSTNAME }, new boolean[] { true });
  }
  
}
