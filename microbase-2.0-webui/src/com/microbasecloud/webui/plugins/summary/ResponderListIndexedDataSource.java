/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 10 Jun 2012, 22:10:26
 */
package com.microbasecloud.webui.plugins.summary;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.runtime.ClientRuntime;

import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebUiException;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;

/**
 * 
 * @author Keith Flanagan
 */
public class ResponderListIndexedDataSource
    extends IndexedContainer {

  static final String COL_ID = "Responder ID";
  static final String COL_TOPIC_ID = "Topic";
  static final String COL_PREF_CPUS = "Preferred local cores";
  static final String COL_MAX_CORES_LOCAL = "Max cores (local)";
  static final String COL_MAX_CORES_DIST = "Max cores (dist)";
  static final String COL_RUN_EVERY = "Populator run every (ms)";
  static final String COL_LAST_RUN = "Last run";
  
  private final Configuration config;
  
  public ResponderListIndexedDataSource(Configuration config) 
      throws WebUiException {
    this.config = config;
    addContainerProperty(COL_ID, String.class, null);
    addContainerProperty(COL_TOPIC_ID, String.class, null);
    addContainerProperty(COL_PREF_CPUS, Integer.class, null);
    addContainerProperty(COL_MAX_CORES_LOCAL, Integer.class, null);
    addContainerProperty(COL_MAX_CORES_DIST, Integer.class, null);
    addContainerProperty(COL_RUN_EVERY, Long.class, null);
    addContainerProperty(COL_LAST_RUN, Date.class, null);

    refresh();
  }


  public void refresh() 
      throws WebUiException {
    ClientRuntime runtime = config.getRuntime();
    List<ResponderInfo> responders = null;
    try
    {
      responders = runtime.getRegistration().getRegisteredResponders();
    }
    catch(Exception e)
    {
      throw new WebUiException("Failed to obtain a list of responders", e);
    }
    
    System.out.println("Found "+responders.size()+" registered responders");
    
    int id = 0;
    for (ResponderInfo info : responders)
    {
      Item item = addItem(id++);
      item.getItemProperty(COL_ID).setValue(info.getResponderId());
      item.getItemProperty(COL_TOPIC_ID).setValue(info.getTopicOfInterest());
      item.getItemProperty(COL_PREF_CPUS).setValue(info.getPreferredCpus());
      item.getItemProperty(COL_MAX_CORES_LOCAL).setValue(info.getMaxLocalCores());
      item.getItemProperty(COL_MAX_CORES_DIST).setValue(info.getMaxDistributedCores());
      item.getItemProperty(COL_RUN_EVERY).setValue(info.getQueuePopulatorRunEveryMs());
      item.getItemProperty(COL_LAST_RUN).setValue(new Date(info.getQueuePopulatorLastRunTimestamp()));

      
    }
    
    sort(new Object[] { COL_ID }, new boolean[] { true });
  }
  
}
