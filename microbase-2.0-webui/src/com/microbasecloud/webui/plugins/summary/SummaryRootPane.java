/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Created: 9 Jun 2012, 15:28:39
 */
package com.microbasecloud.webui.plugins.summary;

import com.microbasecloud.webui.Configuration;
import com.microbasecloud.webui.WebUiException;
import com.vaadin.annotations.AutoGenerated;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

/**
 * 
 * @author Keith Flanagan
 */
public class SummaryRootPane
    extends CustomComponent {
  @AutoGenerated
  private VerticalLayout mainLayout;

  @AutoGenerated
  private NodeListPanel nodeListPanel_1;

  @AutoGenerated
  private ResponderListPanel responderListPanel_1;

  @AutoGenerated
  private ProcessListPanel processListPanel_3;

  private final Configuration config;

  /**
   * The constructor should first build the main layout, set the
   * composition root and then do any custom initialization.
   *
   * The constructor will not be automatically regenerated by the
   * visual editor.
   * @throws WebUiException 
   */
  public SummaryRootPane(final Configuration config)
      throws WebUiException {
    this.config = config;

    buildMainLayout();
    setCompositionRoot(mainLayout);

    // TODO add user code here
    nodeListPanel_1.setConfiguration(config);
    processListPanel_3.setConfiguration(config);
    responderListPanel_1.setConfiguration(config);
  }

  @AutoGenerated
  private VerticalLayout buildMainLayout() {
    // common part: create layout
    mainLayout = new VerticalLayout();
    mainLayout.setImmediate(false);
    mainLayout.setWidth("100%");
    mainLayout.setHeight("100%");
    mainLayout.setMargin(false);

    // top-level component properties
    setWidth("100.0%");
    setHeight("100.0%");

    // processListPanel_3
    processListPanel_3 = new ProcessListPanel();
    processListPanel_3.setImmediate(false);
    processListPanel_3.setWidth("-1px");
    processListPanel_3.setHeight("-1px");
    mainLayout.addComponent(processListPanel_3);

    // responderListPanel_1
    responderListPanel_1 = new ResponderListPanel();
    responderListPanel_1.setImmediate(false);
    responderListPanel_1.setWidth("-1px");
    responderListPanel_1.setHeight("-1px");
    mainLayout.addComponent(responderListPanel_1);

    // nodeListPanel_1
    nodeListPanel_1 = new NodeListPanel();
    nodeListPanel_1.setImmediate(false);
    nodeListPanel_1.setWidth("-1px");
    nodeListPanel_1.setHeight("-1px");
    mainLayout.addComponent(nodeListPanel_1);

    return mainLayout;
  }

}
