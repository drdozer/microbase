/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.example.blast;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import uk.org.microbase.filesystem.spi.FileInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.ws.MessageProcessingException;
import uk.org.microbase.responder.ws.MessageProcessorThread;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author Keith Flanagan
 */
public class BlastProcessor
    extends MessageProcessorThread
{
  private final Logger logger;
  private Handler responderLogHandler;
      

  public BlastProcessor()
  {
    logger = Logger.getLogger(BlastProcessor.class.getName());
  }

  @Override
  protected int getPreferredCpus()
  {
    return 5;
  }

  @Override
  protected Set<String> getIncomingMessageTopics()
  {
    Set<String> topicsOfInterest = new HashSet<String>();
    topicsOfInterest.add("NEW_FASTA_FILE");
    return topicsOfInterest;
  }

  @Override
  public void preRun(Message message)
      throws MessageProcessingException
  {
    try
    {
      //Store a log file in the form: RESPONDERID - Datestamp - message ID
      final File logFile = new File(
          getRuntime().getContainerAppDir(),
          getResponderGuid()+"-" + System.currentTimeMillis()
          + "-"+message.getGuid());
      responderLogHandler = new FileHandler(logFile.getAbsolutePath());
      logger.addHandler(responderLogHandler);
    }
    catch(IOException e)
    {
      throw new MessageProcessingException(
          "Failed to initialise logger for responder "+getResponderGuid()
          +", message: "+message.getGuid(), e);
    }
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws MessageProcessingException
  {
    System.out.println("Cleaning up results from a previous execution -"
        + ", topic: " + message.getTopicGuid()
        + " msg ID: " + message.getGuid());
  }

  @Override
  public Set<Message> processMessage(Message message)
      throws MessageProcessingException
  {
    try
    {
      System.out.println("Processing message -"
          + ", topic: " + message.getTopicGuid()
          + " msg ID: " + message.getGuid());
      File workingDirectory =
          new File(getRuntime().getContainerTmpDir(), message.getGuid());
      workingDirectory.mkdirs();

      //Extract details of the FASTA file to process from the message
      String subjectBucket = message.getContent().get("subject_bucket");
      String subjectPath = message.getContent().get("subject_path");
      String subjectName = message.getContent().get("subject_name");

      String queryBucket = message.getContent().get("query_bucket");
      String queryPath = message.getContent().get("query_path");
      String queryName = message.getContent().get("query_name");

      //Download a local copy of the files
      File subjectFastaFile = new File(workingDirectory, subjectName);
      getRuntime().getMicrobaseFS().download(
          subjectBucket, subjectPath, subjectName, subjectFastaFile);

      File queryFastaFile = new File(workingDirectory, queryName);
      getRuntime().getMicrobaseFS().download(
          queryBucket, queryPath, queryName, queryFastaFile);

      //Define where the output file will be placed
      File alignmentOutput = new File(
          workingDirectory, queryName+"-"+subjectName+".blast");

      //Run 'formatdb'
      StringBuilder stdOut = new StringBuilder();
      StringBuilder stdErr = new StringBuilder();

      String[] formatDbCmd = new String[] {
        "formatdb",
        "-p", "F",
        "-i",
        subjectFastaFile.getAbsolutePath()
      };
      NativeCommandExecutor.executeNativeCommand(
          workingDirectory, formatDbCmd, stdOut, stdErr);

      logger.info("STDOUT from FORMATDB: "+stdOut.toString());
      logger.info("STDERR from FORMATDB: "+stdErr.toString());


      //Run BLAST
      stdOut = new StringBuilder();
      stdErr = new StringBuilder();
      String[] blastCmd = new String[] {
        "blastall",
        "-p", "blastn",
        //"-e", String.valueOf(eValue),
        "-m", "8",
        "-i", queryFastaFile.getAbsolutePath(),
        "-d", subjectFastaFile.getAbsolutePath(), //Database
        //"-j", subjectSeqFasta.getAbsolutePath(),
        "-o", alignmentOutput.getAbsolutePath()
      };
      NativeCommandExecutor.executeNativeCommand(
          workingDirectory, blastCmd, stdOut, stdErr);
      logger.info("STDOUT from BLAST: "+stdOut.toString());
      logger.info("STDERR from BLAST: "+stdErr.toString());

      //Upload the output alignment to the Microbase filesystem
      logger.info("Uploading blast result file: "
          + alignmentOutput.getAbsolutePath());
      getRuntime().getMicrobaseFS().upload(alignmentOutput,
          "blast-results", ".", alignmentOutput.getName(), null);

      Set<Message> outputMessages = new HashSet<Message>();
      return outputMessages;
    }
    catch(Exception e)
    {
      throw new MessageProcessingException(
          "Failed to process message: "+message.getGuid(), e);
    }
    finally
    {
      //FIXME - this should probably go inside a 'post-run' method
      responderLogHandler.flush();
      responderLogHandler.close();
    }
  }


}
