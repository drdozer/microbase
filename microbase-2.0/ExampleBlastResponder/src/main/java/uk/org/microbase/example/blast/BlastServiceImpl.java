/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.org.microbase.example.blast;

import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.responder.ws.AbstractResponderWS;

/**
 * Implements the minimal Web service required by a responder.
 *
 * @author Keith Flanagan
 */
@WebService(
            endpointInterface = "uk.org.microbase.client.ws.ResponderEntryPointWS",
            targetNamespace="http://ws.client.microbase.org.uk/",
            serviceName="MicrobaseResponder")
public class BlastServiceImpl
    extends AbstractResponderWS
    //implements BlastService
{
  private static final Logger logger =
      Logger.getLogger(BlastServiceImpl.class.getName());

  private static final String RESPONDER_ID = "BlastTest";

  @Resource
  private WebServiceContext context;

  public BlastServiceImpl()
      throws ConfigurationException
  {
    super(RESPONDER_ID);
    logger.info("Starting responder: "+RESPONDER_ID);
  }


//  /**
//   * Implements the additional HelloWorld-specific method.
//   *
//   * @param helloString
//   * @return
//   */
//  @Override
//  public String helloTest(String helloString)
//  {
//    return "You sent: "+helloString;
//  }
}
