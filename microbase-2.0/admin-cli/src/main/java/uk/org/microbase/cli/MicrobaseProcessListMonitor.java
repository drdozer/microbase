/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 11, 2012, 11:45:07 AM
 */

package uk.org.microbase.cli;

import com.torrenttamer.concurrent.AbstractPausableThread;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;
import uk.org.microbase.cli.listeners.DistLogPrinter;
import uk.org.microbase.cli.listeners.ResponderInfoPrinter;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.dist.processes.DistributedProcessList;
import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.runtime.ConfigurationException;

/**
 * This program connects to the Microbase distributed data structures and
 * allows the logging of various events.
 * 
 * @author Keith Flanagan
 */
public class MicrobaseProcessListMonitor
    extends AbstractPausableThread
    implements EntryListener<String, ProcessInfo>
{
  private static final Logger l = 
      Logger.getLogger(MicrobaseProcessListMonitor.class.getName());
  private static final long DEFAULT_THREAD_WAKE_MS = 1000 * 10;
  
  private final ClientRuntime runtime;
  private final DistributedProcessList list;
  
  
  public MicrobaseProcessListMonitor(ClientRuntime runtime)
  {
    this.runtime = runtime;
    list = runtime.getProcessList();
    
    
    setWakeAtLeastEveryMs(DEFAULT_THREAD_WAKE_MS);
    setPaused(false);
    list.addProcessListListener(this);
  }
 
  
  private static ClientRuntime loadConfiguration() throws ConfigurationException   
  {
    l.info("Creating runtime based on default global configuration");

    ///Configure
    ClientRuntime runtime = GlobalConfiguration.getDefaultRuntime();

    l.info("Client configuration loaded: "+runtime.toString());
    return runtime;
  }
  
  public static void main(String[] args) 
      throws ConfigurationException
  {
    l.info("Microbase monitor starting. Attempting to acquire a runtime");
    ClientRuntime runtime = loadConfiguration();
    
    MicrobaseProcessListMonitor monitor = new MicrobaseProcessListMonitor(runtime);
    monitor.start();
    for(;;)
    {
      try
      {
        monitor.join();
      }
      catch(InterruptedException e)
      {
        e.printStackTrace();
      }
    }
  }

  @Override
  protected void _doWork(long currentRunTimeMs, long previousRunTimeMs)
  {
    try
    {
      StringBuilder sb = new StringBuilder();
      Collection<ProcessInfo> procs = list.getAllProcesses();
      sb.append("Total processes: ").append(procs.size()).append("\n");
      sb.append("Total processes2: ").append(list.countTotalProcesses()).append("\n");
      for (ProcessInfo info : procs)
      {
        sb.append(info.getProcessGuid()).append(", ");
        sb.append(info.getResponderId()).append(", ");
        sb.append(info.getMessageId()).append(", ");
        sb.append(info.getTopicId()).append(", ");
        sb.append(new Date(info.getProcessStartedAtMs()).toString());
        sb.append("\n");
      }
      sb.append("\n\n");
      System.out.println(sb.toString());
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
  }

  @Override
  public void entryAdded(EntryEvent<String, ProcessInfo> ee)
  {
    System.out.println("Process added: "+ee.getKey());
    synchronized(this)
    {
      notify();
    }
  }

  @Override
  public void entryRemoved(EntryEvent<String, ProcessInfo> ee)
  {
    System.out.println("Process removed: "+ee.getKey());
    synchronized(this)
    {
      notify();
    }
  }

  @Override
  public void entryUpdated(EntryEvent<String, ProcessInfo> ee)
  {
    System.out.println("Process updated: "+ee.getKey());
    synchronized(this)
    {
      notify();
    }
  }

  @Override
  public void entryEvicted(EntryEvent<String, ProcessInfo> ee)
  {
    System.out.println("Process evicted: "+ee.getKey());
    synchronized(this)
    {
      notify();
    }
  }
}
