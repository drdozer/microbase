/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 11, 2012, 11:45:07 AM
 */

package uk.org.microbase.cli;

import com.torrenttamer.concurrent.AbstractPausableThread;
import java.util.Date;
import java.util.logging.Logger;
import uk.org.microbase.cli.listeners.DistLogPrinter;
import uk.org.microbase.cli.listeners.ResponderInfoPrinter;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.runtime.ConfigurationException;

/**
 * This program connects to the Microbase distributed data structures and
 * allows the logging of various events.
 * 
 * @author Keith Flanagan
 */
public class MicrobaseStatusMonitor
    extends AbstractPausableThread
{
  private static final Logger l = 
      Logger.getLogger(MicrobaseStatusMonitor.class.getName());
  private static final long DEFAULT_THREAD_WAKE_MS = 1000 * 60;
  
  private final ClientRuntime runtime;
  private final DistLogPrinter logListener;
  private final ResponderInfoPrinter responderInfoListener;
  
  public MicrobaseStatusMonitor(ClientRuntime runtime)
  {
    this.runtime = runtime;
    this.logListener = new DistLogPrinter();
    this.responderInfoListener = new ResponderInfoPrinter();
    
    setWakeAtLeastEveryMs(DEFAULT_THREAD_WAKE_MS);
    registerListeners();
    setPaused(false);
  }
  
  private void registerListeners()
  {
    l.info("Registering listeners");
    runtime.getDistLog().addLogListener(logListener);
    runtime.getRegistration().addResponderInfoListener(responderInfoListener);
  }
  
  

  
  private static ClientRuntime loadConfiguration() throws ConfigurationException   
  {
    l.info("Creating runtime based on default global configuration");

    ///Configure
    ClientRuntime runtime = GlobalConfiguration.getDefaultRuntime();

    l.info("Client configuration loaded: "+runtime.toString());
    return runtime;
  }
  
  public static void main(String[] args) 
      throws ConfigurationException
  {
    l.info("Microbase monitor starting. Attempting to acquire a runtime");
    ClientRuntime runtime = loadConfiguration();
    
    MicrobaseStatusMonitor monitor = new MicrobaseStatusMonitor(runtime);
    monitor.start();
    for(;;)
    {
      try
      {
        monitor.join();
      }
      catch(InterruptedException e)
      {
        e.printStackTrace();
      }
    }
  }

  @Override
  protected void _doWork(long currentRunTimeMs, long previousRunTimeMs)
  {
    System.out.println("Alive. "+new Date(currentRunTimeMs));
  }
}
