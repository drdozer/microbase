/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 03-Nov-2010, 12:43:20
 */

package uk.org.microbase.cli;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author Keith Flanagan
 */
public class NotificationClient
{
  private static final Logger logger =
      Logger.getLogger(NotificationClient.class.getName());

  private static ClientRuntime runtime;

  private static void printHelpExit(Options options)
  {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "notification.sh";
    String header = "";
    String footer = "";
    int width = 80;
    //formatter.printHelp( "notification.sh", options );
    formatter.printHelp(width, cmdSyntax, header, options, footer);
    System.exit(0);
  }

  public static void main(String[] args) throws Exception
  {
    CommandLineParser parser = new PosixParser();

    Options options = new Options();
    options.addOption("t", "topic", true,
        "Specifies a topic for the current operation");

    options.addOption("p", "publisher", true,
        "Specifies a publisher for the current operation");

    options.addOption("r", "result-set", true,
        "Specifies a result-set for the current operation");

    options.addOption("m", "message", true,
        "Specifies a message for the current operation");
    
    options.addOption("C", "copies", true,
        "When publising a message, specifies how many copies of the same "
        + "message should be published. Useful for testing.");

    options.addOption(OptionBuilder
        .withLongOpt("content")
        .withArgName( "property=value" )
        .hasArgs(2)
        .withValueSeparator()
        .withDescription(
        "used to set content strings of messages when publishing.")
        .create( "c" ));


    options.addOption(OptionBuilder
        .withLongOpt("get-message")
        .hasArgs(1)
        .withDescription(
          "Retrieves a message from the database. "
          + "You must also specify a message ID.")
        .create("M") );
    
    options.addOption(OptionBuilder
        .withLongOpt("publish")
//        .withArgName("T, P, R")
//        .hasArgs(3)
//        .withValueSeparator()
        .withDescription( 
          "Publish a simple message. Requires a topic ID, publisher ID, and "
          + "result set ID. In addition you might want to specify a message "
          + "content using the appropriate option.")
        .create("P") );

//    options.addOption(OptionBuilder
//        .withLongOpt( "publish-from-file" )
////        .withArgName("topic> <publisher> <result set> <content")
//        .hasArgs(4)
//        .withValueSeparator()
//        .withDescription(
//          "Publish a message where content is loaded from a properties file.")
//        .create("F") );

    options.addOption(OptionBuilder
        .withLongOpt("list-messages")
        .withDescription(
          "Lists all messages in the database")
        .create(  ) );

    options.addOption(OptionBuilder
        .withLongOpt("list-messages-by-topic")
        .withDescription(
          "Lists all messages with the specified topic")
        .create(  ) );

    options.addOption(OptionBuilder
        .withLongOpt("count-messages")
        .withDescription(
          "Counts all messages in the database")
        .create(  ) );

    options.addOption(OptionBuilder
        .withLongOpt("count-messages-by-topic")
        .withDescription(
          "Counts all messages with the specified topic")
        .create(  ) );

    if (args.length == 0)
    {
      printHelpExit(options);
    }

    //Load Microbase client global configuration from classpath
    try
    {
    // Configure
      System.out.println("Connecting to Microbase");
      ///Configure
      runtime = GlobalConfiguration.getDefaultRuntime();
      System.out.println("Connected: "+runtime.toString());
    }
    catch(Exception e)
    {
      logger.info("Failed to configure a Microbase runtime based on "
          + "settings in the configuration files.");
      e.printStackTrace();
      System.exit(1);
    }


    //Parse command line
    System.out.println("\n\n\n");
    try
    {
      CommandLine line = parser.parse(options, args);

      // validate that block-size has been set
      if(line.hasOption("publish"))
      {
        publish(line);
      }
      else if (line.hasOption("get-message"))
      {
        getMessage(line);
      }
      else if (line.hasOption("list-messages"))
      {
        listMessages(line);
      }
      else if (line.hasOption("list-messages-by-topic"))
      {
        listMessagesByTopic(line);
      }
      else if (line.hasOption("count-messages"))
      {
        countMessages(line);
      }
      else if (line.hasOption("count-messages-by-topic"))
      {
        countMessagesByTopic(line);
      }
      else
      {
        logger.info("No valid command specified.");
        printHelpExit(options);
      }
    }
    catch(ParseException e)
    {
      e.printStackTrace();
      printHelpExit(options);
    }
    finally
    {
      System.out.println("\n\n\nDisconnecting from Microbase...");
      runtime.shutdown();
    }
  }

  private static void publish(CommandLine line)
  {
    logger.info("Publishing new message");

    System.out.println("Content:");
    line.getOptionProperties("content").list(System.out);
    
    int copies = Integer.parseInt(line.getOptionValue("copies", "1"));

    String topic = line.getOptionValue("topic", "unspecified-topic");
    String publisher = line.getOptionValue("publisher", "unspecified-publisher");
    String resultSet = line.getOptionValue("result-set", "unspecified-resultset");

    Properties props = line.getOptionProperties("content");

    Map<String, String> msgContent = new HashMap<String, String>();
    if (props != null)
    {
      for (Object key : props.keySet())
      {
        String propName = (String) key;
        msgContent.put(propName, props.getProperty(propName));
      }
    }

    try
    {
      for (int i=0; i<copies; i++)
      {
        System.out.println("Publishing "+i+" of "+copies);
        runtime.getNotification().publish(publisher, topic, msgContent, null);
      }
    }
    catch(Exception e)
    {
      logger.info("Failed to publish message:");
      e.printStackTrace();
    }
  }


  private static void getMessage(CommandLine line)
  {
    String messageId = line.getOptionValue("get-message");
    try
    {
      logger.info("Searching for message: "+messageId);
      Message msg = runtime.getNotification().getMessage(messageId);
      logger.info("Got object: "+msg);
    }
    catch (Exception ex)
    {
      Logger.getLogger(NotificationClient.class.getName()).log(Level.SEVERE, null, ex);
    }
    
  }

  private static void listMessages(CommandLine line)
  {
    try
    {
      logger.info("Listing all messages in timestamp order");
      int totalMsgs = 0;
      int blockSize = 10;
      for (int offset = 0; ; offset += blockSize)
      {
        System.out.println("******* Getting block: "+offset+" --> "+(offset+blockSize));

        List<Message> msgBlock = runtime.getNotification().
            listMessagesInTimestampOrder(offset, offset+blockSize);
        System.out.println("******* Got: "+msgBlock.size());

        if (msgBlock.isEmpty())
        {
          break;
        }
        totalMsgs += msgBlock.size();

        for (Message msg : msgBlock)
        {
          System.out.println(msg);
        }

      }
      logger.info("Total messages retrieved: "+totalMsgs);
    }
    catch(Exception e)
    {
      logger.info("failed to list messages");
      e.printStackTrace();
    }
  }

  private static void listMessagesByTopic(CommandLine line)
  {
    try
    {
      String topicId = line.getOptionValue("topic");
      if (topicId == null)
      {
        throw new Exception("No topic ID was specified");
      }

      logger.info("Listing all messages for topic ID: "+topicId);
      int totalMsgs = 0;
      int blockSize = 10;
      for (int offset = 0; ; offset += blockSize)
      {
        System.out.println("******* Getting block: "+offset+" --> "+(offset+blockSize));
        List<Message> msgBlock = runtime.getNotification().
            listMessagesByTopic(topicId, offset, offset+blockSize);
        System.out.println("******* Got: "+msgBlock.size());
        if (msgBlock.isEmpty())
        {
          break;
        }
        totalMsgs += msgBlock.size();


        for (Message msg : msgBlock)
        {
          System.out.println(msg);
        }

      }
      logger.info("Total messages retrieved: "+totalMsgs);
    }
    catch(Exception e)
    {
      logger.info("Failed to list messages by topic");
      e.printStackTrace();
    }
  }

  private static void countMessages(CommandLine line)
  {
    try
    {
      long totalMsgs = runtime.getNotification().count();
      logger.info("Total messages in database: "+totalMsgs);
    }
    catch(Exception e)
    {
      logger.info("Failed to count messages");
      e.printStackTrace();
    }
  }

private static void countMessagesByTopic(CommandLine line)
  {
    try
    {
      String topicId = line.getOptionValue("topic");
      if (topicId == null)
      {
        throw new Exception("No topic ID was specified");
      }

      long totalMsgs = runtime.getNotification().
          countMessagesWithTopic(topicId);
      logger.info("Total messages with topic: "+topicId+": "+totalMsgs);
    }
    catch(Exception e)
    {
      logger.info("Failed to count messages by topic");
      e.printStackTrace();
    }
  }
}
