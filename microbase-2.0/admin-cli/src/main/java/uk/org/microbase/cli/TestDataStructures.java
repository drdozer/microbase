/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 11, 2012, 11:45:07 AM
 */

package uk.org.microbase.cli;

import com.torrenttamer.concurrent.AbstractPausableThread;
import java.util.Date;
import java.util.logging.Logger;
import uk.org.microbase.cli.listeners.DistLogPrinter;
import uk.org.microbase.cli.listeners.ResponderInfoPrinter;
import uk.org.microbase.configuration.GlobalConfiguration;
import uk.org.microbase.dist.responder.MicrobaseRegistration;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.util.UidGenerator;

/**
 * This program fires various data items at several of the Microbase distributed
 * data structures for testing purposes.
 * 
 * @author Keith Flanagan
 */
public class TestDataStructures
    extends AbstractPausableThread
{
  private static final Logger l = 
      Logger.getLogger(TestDataStructures.class.getName());
  private static final long DEFAULT_THREAD_WAKE_MS = 1000 * 60;
  
  private final ClientRuntime runtime;
  
  public TestDataStructures(ClientRuntime runtime)
  {
    this.runtime = runtime;
    
    setWakeAtLeastEveryMs(DEFAULT_THREAD_WAKE_MS);
    setPaused(false);
  }
  
  


  
  private static ClientRuntime loadConfiguration() throws ConfigurationException   
  {
    l.info("Creating runtime based on default global configuration");

    ///Configure
    ClientRuntime runtime = GlobalConfiguration.getDefaultRuntime();

    l.info("Client configuration loaded: "+runtime.toString());
    return runtime;
  }
  
  public static void main(String[] args) 
      throws ConfigurationException
  {
    l.info("Microbase monitor starting. Attempting to acquire a runtime");
    ClientRuntime runtime = loadConfiguration();
    
    TestDataStructures monitor = new TestDataStructures(runtime);
    monitor.start();
    for(;;)
    {
      try
      {
        monitor.join();
      }
      catch(InterruptedException e)
      {
        e.printStackTrace();
      }
    }
  }

  @Override
  protected void _doWork(long currentRunTimeMs, long previousRunTimeMs)
  {
    System.out.println("Alive. "+new Date(currentRunTimeMs));
    testLogging();
    testResponderRegistration();
    
    System.out.println("Done testing for now.");
  }
  
  private void testLogging()
  {
    runtime.getDistLog().log("A test log message");
    runtime.getDistLog().log("A test log message", "Multiple", "strings");
  }
  
  private void testResponderRegistration()
  {
    try
    {
      final MicrobaseRegistration reg = runtime.getRegistration();
      
      String id = UidGenerator.generateUid();
      int version = 1;
      ResponderInfo responder = new ResponderInfo();
      responder.setEnabled(true);
      responder.setMaxDistributedCores(18);
      responder.setMaxLocalCores(4);
      responder.setQueuePopulatorRunEveryMs(60000);
      responder.setResponderId(id);
      responder.setResponderName("Test responder: "+id);
      responder.setResponderVersion(String.valueOf(version));
      
      
      //1) Returned should be same as sent
      System.out.println("\n\nAttempting to register responder: "+responder);
      ResponderInfo returned = reg.registerResponder(responder);
      System.out.println("Actually got: "+returned);
      
      //2) Returned should be as (1) (because ID is the same)
      version++;
      responder.setResponderVersion(String.valueOf(version));
      System.out.println("\n\nAttempting to re-register responder: "+responder);
      returned = reg.registerResponder(responder);
      System.out.println("Actually got: "+returned);
      
      //3) Returned should be as (1) but with an updated timestamp
      System.out.println("\n\nFinally, attempting to update queue population time");
      reg.updateQueueLastPopulatedTime(id, System.currentTimeMillis());
      returned = reg.getResponderInfo(id);
      System.out.println("Time updated?: "+returned);
      
    }
    catch(Exception e)
    {
      System.out.println("Registration test failed: "+e.getMessage());
      e.printStackTrace();
    }
  }
  
  
}
