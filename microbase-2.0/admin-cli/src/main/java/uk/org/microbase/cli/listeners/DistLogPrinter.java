/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 5, 2011, 10:31:24 PM
 */

package uk.org.microbase.cli.listeners;

import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Keith Flanagan
 */
public class DistLogPrinter
  implements ItemListener<String>
{

  private static final Logger logger = 
      Logger.getLogger(DistLogPrinter.class.getName());
  
  
  
  public DistLogPrinter()
  {
  }

  

//  @Override
//  public void itemAdded(String e)
//  {
//    synchronized(this)
//    {
//      logger.log(Level.INFO, e);
//    }
//  }
//
//  @Override
//  public void itemRemoved(String e)
//  {
//    synchronized(this)
//    {
//      //TODO we don't need this in release - just for testing purposes.
//      logger.log(Level.INFO, "Item removed from distributed log");
//    }
//  }

  @Override
  public void itemAdded(ItemEvent<String> ie)
  {
    synchronized(this)
    {
      logger.log(Level.INFO, ie.getItem());
    }
  }

  @Override
  public void itemRemoved(ItemEvent<String> ie)
  {
    synchronized(this)
    {
      //TODO we don't need this in release - just for testing purposes.
      logger.log(Level.INFO, "Item removed from distributed log");
    }
  }

}
