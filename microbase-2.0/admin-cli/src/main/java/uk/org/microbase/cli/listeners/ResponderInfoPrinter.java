/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 5, 2011, 10:31:24 PM
 */

package uk.org.microbase.cli.listeners;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import com.hazelcast.core.ItemListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.ResponderInfo;

/**
 *
 * @author Keith Flanagan
 */
public class ResponderInfoPrinter
  implements EntryListener<String, ResponderInfo>
{

  private static final Logger logger = 
      Logger.getLogger(ResponderInfoPrinter.class.getName());
  
 
  @Override
  public void entryAdded(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder REGISTERED: "+ee.getValue().toString());
  }

  @Override
  public void entryRemoved(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder DE-REGISTERED: "+ee.getValue().toString());
  }

  @Override
  public void entryUpdated(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder UPDATED: "+ee.getValue().toString());
  }

  @Override
  public void entryEvicted(EntryEvent<String, ResponderInfo> ee)
  {
    logger.info("Responder EVICTED: "+ee.getValue().toString());
  }

}
