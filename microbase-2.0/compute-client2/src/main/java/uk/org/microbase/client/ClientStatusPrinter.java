/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 5, 2011, 10:31:24 PM
 */

package uk.org.microbase.client;

import com.hazelcast.core.ItemEvent;
import com.torrenttamer.concurrent.AbstractPausableThread;
import com.hazelcast.core.ItemListener;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.processes.ExecutionResult;

/**
 *
 * @author Keith Flanagan
 */
public class ClientStatusPrinter
  extends AbstractPausableThread
  implements ItemListener<String>
{
  private static final long DEFAULT_WAKE_EVERY_MS = 10 * 1000;

  private static final Logger logger = 
      Logger.getLogger(ClientStatusPrinter.class.getName());
  
  private final List<MicrobaseProcessFuture> futures;
  
  public ClientStatusPrinter(List<MicrobaseProcessFuture> futures)
  {
    setName(getClass().getSimpleName()+"-"+getId());
    setWakeAtLeastEveryMs(DEFAULT_WAKE_EVERY_MS);
    this.futures = futures;
  }

  @Override
  protected void _doWork(long currentRunTimeMs, long previousRunTimeMs)
  {
    StringBuilder sb = new StringBuilder("Previously run / scheduled process futures:\n");
    synchronized(futures)
    {
      for (MicrobaseProcessFuture future : futures)
      {
        sb.append("  * ");
        sb.append(future.getResponderInfo().getResponderId());


        if (!future.isDone())
        {
          Date started = new Date(future.getStartedAtMs());
          sb.append(", started: ").append(started);
          sb.append(", done? ").append(future.isDone());
          sb.append(", cancelled? ").append(future.isCancelled());
        }
        else
        {
          try
          {
            ExecutionResult result = future.get();

            Date procStarted = new Date(result.getProcessStartedAtMs());
            Date workStarted = new Date(result.getWorkStartedAtMs());
            Date workCompleted = new Date(result.getWorkCompletedAtMs());
            sb.append(", proc started: ").append(procStarted);
            sb.append(", work started: ").append(workStarted);
            sb.append(", work completed: ").append(workCompleted);
            sb.append(", work? ").append(result.isWorkWasAvailable());
            sb.append(", msg processed? ").append(result.isUserJobAttemptedRun());
            sb.append(", msg processed? ").append(result.isUserJobSuccessful());
            sb.append(", topic: ").append(result.getTopicId());

            if (result.isWorkWasAvailable())
            {
              sb.append(", msg: ").append(result.getMessageId());
            }

            sb.append("\n    COMMENT: ").append(result.getComment());
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
        }

        sb.append("\n");
      }
    }
    sb.append("-------------------\n");
    
    synchronized(this)
    {
      System.out.println(sb);
    }
  }
  

//  @Override
//  public void itemAdded(String e)
//  {
//    synchronized(this)
//    {
//      logger.log(Level.INFO, e);
//    }
//  }
//
//  @Override
//  public void itemRemoved(String e)
//  {
//    synchronized(this)
//    {
//      //TODO we don't need this in release - just for testing purposes.
//      logger.log(Level.INFO, "Item removed from distributed log");
//    }
//  }

  @Override
  public void itemAdded(ItemEvent<String> ie)
  {
    synchronized(this)
    {
      logger.log(Level.INFO, ie.getItem());
    }
  }

  @Override
  public void itemRemoved(ItemEvent<String> ie)
  {
    synchronized(this)
    {
      //TODO we don't need this in release - just for testing purposes.
      logger.log(Level.INFO, "Item removed from distributed log");
    }
  }

}
