/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 16, 2012, 3:05:15 PM
 */

package uk.org.microbase.client;

import com.torrenttamer.concurrent.AbstractPausableThread;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.processes.ExecutionResult;
import uk.org.microbase.runtime.ClientRuntime;

/**
 * Processes MicrobaseFutures and if necessary reports ExecutionResults to
 * other nodes.
 * 
 * @author Keith Flanagan
 */
public class FutureProcessor
    extends AbstractPausableThread
{
  private static final Logger logger = 
      Logger.getLogger(FutureProcessor.class.getSimpleName());
  private static final long DEFAULT_FUTURE_QUEUE_WAIT_MS = 1000 * 5;
  
  private final ClientRuntime runtime;
  private final List<MicrobaseProcessFuture> futures;
  
  
  public FutureProcessor(ClientRuntime runtime,
      List<MicrobaseProcessFuture> futures)
  {
    setName(getClass().getSimpleName()+"-"+getId());
    this.runtime = runtime;
    this.futures = futures;
  }
  
  @Override
  protected void _doWork(long currentRunTimeMs, long previousRunTimeMs)
  {
    synchronized(futures)
    {
      Iterator<MicrobaseProcessFuture> itr = futures.iterator();
//      logger.info("Futures queue size: "+futures.size());
      while(itr.hasNext())
      {
        MicrobaseProcessFuture future = itr.next();
        if (!future.isDone())
        {
          continue;
        }
        try
        {
          ExecutionResult result = future.get();
          logger.info("Processing result from responder: "
              + result.getResponderId() + ", topic: "+result.getTopicId()
              + ", started: "+result.getProcessStartedAtMs() 
              + ", msg avail: "+result.isWorkWasAvailable()
              + ", processed: "+result.isUserJobAttemptedRun()
              + ", success: "+result.isUserJobSuccessful());
  //            + ", work started: "+result.getWorkStartedAtMs()
  //            + ", work completed: " + result.getWorkCompletedAtMs())

          if (result.isUserJobAttemptedRun())
          {
            logger.info("Reporting execution result for responder: "
                + result.getResponderId());
            runtime.getProcessList().reportExecutionResult(result);
            
//            if (!result.isUserJobSuccessful())
//            {
//              logger.info("An Exception was reported while running the use job: \n"
//                  +result.getExceptionStackTrace()+"\n-----------------------");
//            }
          }
          else
          {
//            logger.info("Not reporting execution result - no message was processed");
          }
          itr.remove();
        }
        catch(Exception e)
        {
          e.printStackTrace();
          logger.info("Failed to process / report ExecutionResult. Trying again later.");
        }
      }
    }
    
    try
    {
//      logger.info("Futures queue is empty of completed futures. Waiting.");
      synchronized(futures)
      {
        futures.wait(DEFAULT_FUTURE_QUEUE_WAIT_MS);
      }
    }
    catch (InterruptedException ex)
    {
      Logger.getLogger(FutureProcessor.class.getName()).log(Level.SEVERE, null, ex);
    }
  }


}
