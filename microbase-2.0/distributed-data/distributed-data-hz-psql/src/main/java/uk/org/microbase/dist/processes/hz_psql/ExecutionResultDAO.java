/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 13, 2012, 3:17:30 PM
 */

package uk.org.microbase.dist.processes.hz_psql;

import com.torrenttamer.jdbcutils.JdbcUtils;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.dist.processes.ExecutionResult;
import uk.org.microbase.util.db.DBException;

/**
 *
 * @author Keith Flanagan
 */
public class ExecutionResultDAO
{
  private static final Logger logger =
      Logger.getLogger(ExecutionResultDAO.class.getName());
  private final ObjectMapper jsonMapper;

  public ExecutionResultDAO()
  {
    jsonMapper = new ObjectMapper();
  }

  public void save(Connection txn, ExecutionResult bean)
      throws DBException
  {
    List<ExecutionResult> beans = new LinkedList<ExecutionResult>();
    beans.add(bean);
    save(txn, beans);
  }

  public void save(Connection txn, List<ExecutionResult> beans)
      throws DBException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "INSERT INTO execution_results "
          + "(process_guid, hostname, "
          + "process_started_timestamp, "
          + "work_started_timestamp, work_completed_timestamp, "
          + "responder_id, topic_id, message_id, "
          + "assigned_local_cores, num_global_cores_in_use_at_start, "
          + "exception_stack_trace, comment, "
          + "user_job_attempted_run, user_job_successful"

          + ") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      stmt = txn.prepareStatement(sql);

      for (ExecutionResult result : beans)
      {
        int col = 1;
        stmt.setString(col++, result.getProcessGuid());
        stmt.setString(col++, result.getHostname());
        
        stmt.setTimestamp(col++, new Timestamp(result.getProcessStartedAtMs()));
        stmt.setTimestamp(col++, new Timestamp(result.getWorkStartedAtMs()));
        stmt.setTimestamp(col++, new Timestamp(result.getWorkCompletedAtMs()));
        
        stmt.setString(col++, result.getResponderId());
        stmt.setString(col++, result.getTopicId());
        stmt.setString(col++, result.getMessageId());
        
        stmt.setInt(col++, result.getAssignedLocalCores());
        stmt.setInt(col++, result.getNumGlobalCoresInUseAtStart());
        
        stmt.setString(col++, result.getExceptionStackTrace());
        stmt.setString(col++, result.getComment());
        
        stmt.setBoolean(col++, result.isUserJobAttemptedRun());
        stmt.setBoolean(col++, result.isUserJobSuccessful());

        stmt.addBatch();
//        stmt.executeUpdate();
      }
      stmt.executeBatch();

    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new DBException("Failed to insert "+beans.size()+" execution results", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  public ExecutionResult getResultByProcessId(Connection txn, String processGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM execution_results WHERE process_guid = ?");
      stmt.setString(1, processGuid);
      ResultSet rs = stmt.executeQuery();
      List<ExecutionResult> results = resultSetToList(rs);
      if (results.size() == 1)
      {
        return results.get(0);
      }
      else if (results.isEmpty())
      {
        return null;
      }
      else
      {
        throw new SQLException(results.size()+" ExecutionResult instances had "
            + "the same GUID: "+processGuid + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getResultByProcessId() for: "+processGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public int count(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM execution_results");

      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute count()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  /*
   * Queries over process start times
   */
  public long getFirstProcessStartTimestamp(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT min(process_started_timestamp) as min FROM execution_results");

      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        long timestamp = rs.getTimestamp("min").getTime();
  //      System.out.println("Timestamp: "+timestamp);
        return timestamp;
      }
      else
      {
        return 0;
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getFirstProcessStartTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public long getLastProcessStartTimestamp(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT max(process_started_timestamp) as max FROM execution_results");

      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        return rs.getTimestamp("max").getTime();
      }
      else
      {
        return 0;
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getLastProcessStartTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public int countExecutionResultsBetweenProcStartTimestamps(
      Connection txn, long startTimestamp, long endTimestamp)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM execution_results "
          + "WHERE process_started_timestamp >= ? "
          + "AND process_started_timestamp < ?");
      
      int col = 1;
      stmt.setTimestamp(col++, new Timestamp(startTimestamp));
      stmt.setTimestamp(col++, new Timestamp(endTimestamp));

      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute countExecutionResultsBetweenProcStartTimestamps()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  
  /*
   * Queries over work completed timestamps
   */
  public long getFirstWorkCompletedTimestamp(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT min(work_completed_timestamp) as min FROM execution_results");

      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        long timestamp = rs.getTimestamp("min").getTime();
  //      System.out.println("Timestamp: "+timestamp);
        return timestamp;
      }
      else
      {
        return 0;
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getFirstWorkCompletedTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public long getLastWorkCompletedTimestamp(Connection txn)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT max(work_completed_timestamp) as max FROM execution_results");

      ResultSet rs = stmt.executeQuery();
      if (rs.next())
      {
        return rs.getTimestamp("max").getTime();
      }
      else
      {
        return 0;
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getLastWorkCompletedTimestamp()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public int countExecutionResultsBetweenWorkCompletedTimestamps(
      Connection txn, long startTimestamp, long endTimestamp)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM execution_results "
          + "WHERE work_completed_timestamp >= ? "
          + "AND work_completed_timestamp < ?");
      
      int col = 1;
      stmt.setTimestamp(col++, new Timestamp(startTimestamp));
      stmt.setTimestamp(col++, new Timestamp(endTimestamp));

      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute countExecutionResultsBetweenWorkCompletedTimestamps()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  
  
  
  

  public int countResultsForNode(Connection txn, String hostname)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM execution_results WHERE hostname = ?");
      stmt.setString(1, hostname);
      
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute count() for hostname: "+hostname, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public List<ExecutionResult> listByHostname(
      Connection txn,
      String hostname, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM execution_results WHERE hostname = ?"
          + "ORDER BY process_started_timestamp LIMIT ? OFFSET ? ");
      int col = 1;
      stmt.setString(col++, hostname);
      stmt.setInt(col++, limit);
      stmt.setInt(col++, offset);

      ResultSet rs = stmt.executeQuery();
      List<ExecutionResult> results = resultSetToList(rs);
      return results;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listByHostname() for topic: "+hostname, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  

  public List<ExecutionResult> list(
      Connection txn, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(*) as count FROM execution_results "
          + "ORDER BY process_started_timestamp LIMIT ? OFFSET ? ");
      int col = 1;
      stmt.setInt(col++, limit);
      stmt.setInt(col++, offset);

      ResultSet rs = stmt.executeQuery();
      List<ExecutionResult> results = resultSetToList(rs);
      return results;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute list()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  private List<ExecutionResult> resultSetToList(ResultSet rs)
      throws SQLException, IOException
  {
    List<ExecutionResult> list = new ArrayList<ExecutionResult>();
    while(rs.next())
    {
      ExecutionResult result = new ExecutionResult();
      
      
      result.setProcessGuid(rs.getString("process_guid"));
      result.setHostname(rs.getString("hostname"));
      result.setProcessStartedAtMs(rs.getTimestamp("process_started_timestamp").getTime());
      result.setWorkStartedAtMs(rs.getTimestamp("work_started_timestamp").getTime());
      result.setWorkCompletedAtMs(rs.getTimestamp("work_completed_timestamp").getTime());
      result.setResponderId(rs.getString("responder_id"));
      result.setTopicId(rs.getString("topic_id"));
      result.setMessageId(rs.getString("message_id"));
      result.setAssignedLocalCores(rs.getInt("assigned_local_cores"));
      result.setNumGlobalCoresInUseAtStart(rs.getInt("num_global_cores_in_use_at_start"));
      result.setExceptionStackTrace(rs.getString("exception_stack_trace"));
      result.setComment(rs.getString("comment"));
      
      result.setUserJobAttemptedRun(rs.getBoolean("user_job_attempted_run"));
      result.setUserJobSuccessful(rs.getBoolean("user_job_successful"));
      
      list.add(result);
    }
    return list;
  }

}
