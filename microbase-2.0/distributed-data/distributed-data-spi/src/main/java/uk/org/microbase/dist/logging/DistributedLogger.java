/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 6, 2011, 10:49:04 PM
 */

package uk.org.microbase.dist.logging;

import com.hazelcast.core.ItemListener;
import java.util.List;

/**
 *
 * @author Keith Flanagan
 */
public interface DistributedLogger
{
  public void log(String message);
  public void log(String... strings);
  public List<String> getAllLogItems();
 
  /**
   * Allow other processes to listen to log events.
   * 
   * Note that this is a quick solution - eventually we should have a generic
   * listener interface that doesn't come from Hazelcast. However, when we
   * do so, we need to make sure that not EVERY instance of DistributedLogger
   * registers a listener unnecessarily since that would cause much network
   * traffic that may not be needed. For efficiency reasons, keeping the 
   * Hazelcast listener API exposed for now.
   * 
   * @param listener 
   */
  public void addLogListener(ItemListener<String> listener);
}
