/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 13, 2012, 2:59:31 PM
 */

package uk.org.microbase.dist.processes;

import com.hazelcast.core.EntryListener;
import com.hazelcast.core.ItemListener;
import java.util.Collection;

/**
 * A distributed process list that keeps track of all currently executing
 * jobs on all members in the cluster.
 * 
 * In addition to the transient process list, a permanent list of ExecutionResults
 * is maintained. ExecutionResult stores almost everything that a ProcessInfo
 * object stores, plus finish times. This list is intended to be a record of
 * every job execution that the cluster has ever performed.
 * 
 * @author Keith Flanagan
 */
public interface DistributedProcessList
{
  
  /*
   * These methods deal with currently executing processes
   */
  public void createProcessEntry(ProcessInfo processInfo)
      throws ProcessListException;
  
  public void removeProcessEntry(String processGuid)
      throws ProcessListException;
  
  public void updateProcessEntry(ProcessInfo processInfo)
      throws ProcessListException;
  
  public int countTotalProcesses()
      throws ProcessListException;
  
  public int countDistributedCoresForResponder(String responderId)
      throws ProcessListException;
  
  public Collection<ProcessInfo> getAllProcesses();
  
  public void addProcessListListener(EntryListener<String, ProcessInfo> listener);
  public void removeProcessListListener(EntryListener<String, ProcessInfo> listener);
  
  
  /*
   * These methods deal with logging results of message executions
   */
  
  
  /**
   * Called by a compute client to report the completion of processing a 
   * particular message.
   * @param result
   * @throws ProcessListException 
   */
  public void reportExecutionResult(ExecutionResult result)
      throws ProcessListException;

  public void addExecutionResultListener(ItemListener<ExecutionResult> listener);
  public void removeExecutionResultListener(ItemListener<ExecutionResult> listener);
}
