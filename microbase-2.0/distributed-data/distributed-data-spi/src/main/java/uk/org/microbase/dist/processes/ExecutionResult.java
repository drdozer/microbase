/*
 * Copyright 2011 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Dec 5, 2011, 11:30:14 PM
 */

package uk.org.microbase.dist.processes;

import java.io.Serializable;

/**
 *
 * @author Keith Flanagan
 */
public class ExecutionResult
      implements Serializable
{
  private String processGuid;
  private String hostname;
  
  
  private long processStartedAtMs;
  private long workStartedAtMs;
  private long workCompletedAtMs;
  
  private String responderId;
  private String responderName;
  private String responderVersion;
  private String topicId;
  private String messageId;
  
  private int assignedLocalCores;
  private int numGlobalCoresInUseAtStart;
  
  /**
   * Set true if an unprocessed message was found during the course of this
   * execution.
   */
  private transient boolean workWasAvailable;
  
  /**
   * Set true if this execution completed successfully (i.e., no exception was
   * thrown). Also includes non-domain specific work - eg, if an exception was
   * thrown while attempting to obtain a message, then this would be set false.
   */
//  private boolean success;
  
  /**
   * If an exception was thrown, its stack trace is attached here.
   */
//  private transient Throwable error;
  private String exceptionStackTrace;
  
  /**
   * A human-readable comment for debugging / logging purposes.
   */
  private String comment;
  

  private boolean userJobAttemptedRun;
  private boolean userJobSuccessful;

  public ExecutionResult()
  {
    responderId = null;
    topicId = null;
    messageId = null;
    workWasAvailable = false;
    comment = null;
  }
 

  public String getComment()
  {
    return comment;
  }

  public void setComment(String comment)
  {
    this.comment = comment;
  }

//  public Throwable getError()
//  {
//    return error;
//  }
//
//  public void setError(Throwable error)
//  {
//    this.error = error;
//  }

  public String getMessageId()
  {
    return messageId;
  }

  public void setMessageId(String messageId)
  {
    this.messageId = messageId;
  }

  public String getResponderId()
  {
    return responderId;
  }

  public void setResponderId(String responderId)
  {
    this.responderId = responderId;
  }

  public String getTopicId()
  {
    return topicId;
  }

  public void setTopicId(String topicId)
  {
    this.topicId = topicId;
  }

  public long getProcessStartedAtMs()
  {
    return processStartedAtMs;
  }

  public void setProcessStartedAtMs(long processStartedAtMs)
  {
    this.processStartedAtMs = processStartedAtMs;
  }

  public long getWorkCompletedAtMs()
  {
    return workCompletedAtMs;
  }

  public void setWorkCompletedAtMs(long workCompletedAtMs)
  {
    this.workCompletedAtMs = workCompletedAtMs;
  }

  public long getWorkStartedAtMs()
  {
    return workStartedAtMs;
  }

  public void setWorkStartedAtMs(long workStartedAtMs)
  {
    this.workStartedAtMs = workStartedAtMs;
  }

  public boolean isWorkWasAvailable()
  {
    return workWasAvailable;
  }

  public void setWorkWasAvailable(boolean workWasAvailable)
  {
    this.workWasAvailable = workWasAvailable;
  }

  public String getHostname()
  {
    return hostname;
  }

  public void setHostname(String hostname)
  {
    this.hostname = hostname;
  }

  public String getProcessGuid()
  {
    return processGuid;
  }

  public void setProcessGuid(String processGuid)
  {
    this.processGuid = processGuid;
  }

  public int getAssignedLocalCores()
  {
    return assignedLocalCores;
  }

  public void setAssignedLocalCores(int assignedLocalCores)
  {
    this.assignedLocalCores = assignedLocalCores;
  }

  public int getNumGlobalCoresInUseAtStart()
  {
    return numGlobalCoresInUseAtStart;
  }

  public void setNumGlobalCoresInUseAtStart(int numGlobalCoresInUseAtStart)
  {
    this.numGlobalCoresInUseAtStart = numGlobalCoresInUseAtStart;
  }

  public String getResponderName()
  {
    return responderName;
  }

  public void setResponderName(String responderName)
  {
    this.responderName = responderName;
  }

  public String getResponderVersion()
  {
    return responderVersion;
  }

  public void setResponderVersion(String responderVersion)
  {
    this.responderVersion = responderVersion;
  }

  public String getExceptionStackTrace()
  {
    return exceptionStackTrace;
  }

  public void setExceptionStackTrace(String exceptionStackTrace)
  {
    this.exceptionStackTrace = exceptionStackTrace;
  }

  public boolean isUserJobAttemptedRun()
  {
    return userJobAttemptedRun;
  }

  public void setUserJobAttemptedRun(boolean userJobAttemptedRun)
  {
    this.userJobAttemptedRun = userJobAttemptedRun;
  }

  public boolean isUserJobSuccessful()
  {
    return userJobSuccessful;
  }

  public void setUserJobSuccessful(boolean userJobSuccessful)
  {
    this.userJobSuccessful = userJobSuccessful;
  }
}
