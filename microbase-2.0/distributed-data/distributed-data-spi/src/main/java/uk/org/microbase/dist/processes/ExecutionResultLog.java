/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 13, 2012, 2:59:31 PM
 */

package uk.org.microbase.dist.processes;

import java.util.List;

/**
 * Maintains a persistent log of all message processing attempts
 * 
 * @author Keith Flanagan
 */
public interface ExecutionResultLog
{
  /**
   * Called by a compute client to report the completion of processing a 
   * particular message.
   * @param result
   * @throws ProcessListException 
   */
  public void reportExecutionResult(ExecutionResult result)
      throws ProcessListException;

  /**
   * Returns the total number of execution result instances stored in the database
   */
  public int countExecutionResults()
      throws ProcessListException;
  
  /*
   * Process start timestamp queries
   */
  
  public long getFirstProcessStartTimestamp()
      throws ProcessListException;
  
  public long getLastProcessStartTimestamp()
      throws ProcessListException;
  
  public int countResultsBetweenProcStartTimestamps(
      long startTimestamp, long endTimestamp)
      throws ProcessListException;
  
  /*
   * Work completed queries
   */
  public long getFirstWorkCompletedTimestamp()
      throws ProcessListException;
  
  public long getLastWorkCompletedTimestamp()
      throws ProcessListException;
  
  public int countResultsBetweenWorkCompletedTimestamps(
      long startTimestamp, long endTimestamp)
      throws ProcessListException;
  
  
  /**
   * Returns a paged list of execution results from all responders in ascending
   * timestamp order.
   * 
   * @param offset
   * @param limit
   * @return
   * @throws ProcessListException 
   */
  public List<ExecutionResult> getExecutionResults(int offset, int limit)
      throws ProcessListException;
  
}
