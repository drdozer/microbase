/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.dist.queues;

import java.util.Collection;
import java.util.Set;
import uk.org.microbase.notification.data.Message;

/**
 *
 * @author Keith Flanagan
 */
public interface ResponderMessageQueues
{
//  /**
//   * Allows 
//   * @param responderGuid
//   * @throws DPLException
//   */
//  public void lockQueue(String responderGuid)
//      throws DPLException;
//  public void unlockQueue(String responderGuid)
//      throws DPLException;

//  /**
//   * Allows a responder to add more work to its distributed job queue.
//   * The messages specified in <code>messages</code> are added to the
//   * appropriate queue as long as they don't currently exist.
//   *
//   * @param responderGuid
//   * @param messageGuids
//   * @throws DPLException
//   */
//  public void addJobsToQueue(String responderGuid, Collection<Message> messages)
//      throws DPLException;

  /**
   * Returns a map of responder GUID -> current job queue size. This information
   * is used by the Microbase compute client as part of its decision making
   * process as to which responder it should run next.
   *
   * @return
   * @throws DPLException
   */
//  public Map<String, Integer> getResponderJobQueueSizes()
//      throws DPLException;

  /**
   * Given a responder GUID, returns the current in-memory queue size of
   * Messages waiting to be processed.
   * 
   * @param responderGuid
   * @return
   * @throws DPLException
   */
  public int getResponderJobQueueSize(String responderGuid)
      throws MessageQueueException;

  /**
   * Used by a responder to obtain the next message from its queue.
   * Taking an item removes it from the queue, so only the calling responder
   * processes the item. Returned messages are marked as 'PROCESSING' before
   * being returned to the caller.
   *
   * If the specified queue is currently empty, an attempt is made to
   * re-populate it from new messages available in the notification system
   * (messages maked as 'READY' for <code>responderGuid</code>).
   * This is performed in a thread/process-safe fashion, such that multiple
   * simultaneous calls to this method from different machines will not result
   * in duplicate messages being added to the queue, or returned for processing.
   *
   * @param responderGuid the responder whose message IDs are to be returned
   * @return the message ID of the next item in the queue, or NULL if the
   * queue is empty and could not be re-populated.
   *
   * @throws DPLException
   */
  public Message takeNextQueueItem(String responderGuid, String topicGuid)
      throws MessageQueueException;
}
