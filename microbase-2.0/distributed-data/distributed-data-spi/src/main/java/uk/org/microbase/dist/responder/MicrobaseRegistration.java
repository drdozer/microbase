/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.dist.responder;

import com.hazelcast.core.EntryListener;
import java.util.List;

/**
 * Provides a specification for management of responder registration
 * information.
 * 
 * @author Keith Flanagan
 */
public interface MicrobaseRegistration
{
  /**
   * Registers a responder with Microbase. If a information about a responder 
   * with the same ID already exists, then the provided information, 
   * <code>responderInfo</code> will be ignored.
   * 
   * @param responderInfo the information (responder name, topic of interest, 
   * global configuration parameters) to register.
   * @returns the actual configuration information for this responder. If
   * this is a fresh registration, then the returned object will be the same as
   * <code>responderInfor</code>. However, if the responder had previously 
   * been registered before this call, then the existing ResponderInfo object
   * will be returned instead. Your implementation should configure itself
   * based on the values in the returned object.
   * 
   * @throws RegistrationException if there was an error processing the request.
   */
  public ResponderInfo registerResponder(ResponderInfo responderInfo)
      throws RegistrationException;

  /**
   * Removes any existing information about the specified responder from
   * the storage system.
   * 
   * @param responderGuid
   * @throws RegistrationException 
   */
  public void deregisterResponder(String responderGuid)
      throws RegistrationException;

  public List<ResponderInfo> getRegisteredResponders()
      throws RegistrationException;

  public List<ResponderInfo> getEnabledResponders()
      throws RegistrationException;

  /**
   * Allows a responder to be (de)activated dynamically. This setting does not
   * effect currently executing jobs. If a responder is disabled, then no new
   * messages for that responder will be started after this method call.
   * 
   * @param responderGuid the responder to activate or deactivate.
   * @param enabled if set true, then responders will be active, otherwise false.
   * @throws RegistrationException 
   */
  public void setResponderEnabled(String responderGuid, boolean enabled)
      throws RegistrationException;

  public boolean isResponderEnabled(String responderGuid)
      throws RegistrationException;

  public ResponderInfo getResponderInfo(String responderGuid)
      throws RegistrationException;
  
  
  /**
   * For a specified responder, updates the time at which a message queue
   * re-population operation last occurred. 
   * This method is usually called after a compute node requests re-population
   * of an empty responder message queue.
   * 
   * @param responderGuid 
   * @param timestamp 
   * @throws RegistrationException 
   */
  public void updateQueueLastPopulatedTime(String responderGuid, long timestamp)
      throws RegistrationException;
  
  /**
   * Adds a Hazelcast object listener that gets informed when one of the 
   * ResponderInfo objects gets added/modified/removed.
   * 
   * This currently exposes some of the MicrobaseResponder Hazelcast
   * implementation details, and is therefore subjet to change in future.
   * However, currently the only things using this functionality are system
   * monitors for logging / debugging.
   * 
   * @param listener 
   */
  public void addResponderInfoListener(
      EntryListener<String, ResponderInfo> listener);
}
