/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 05-Nov-2010, 15:23:23
 */

package uk.org.microbase.dist.responder;

import java.io.Serializable;
import java.util.Date;

/**
 * Represents a responder task that has been installed and registered with
 * the Microbase scheduler.
 *
 * @author Keith Flanagan
 */
public class ResponderInfo
    implements Serializable
{
  private static final long DEFAULT_QUEUE_POP_RUN_EVERY_MS = 1000 * 60;
  private static final int DEFAULT_MAX_LOCAL_CORES = 1;
  private static final int DEFAULT_MAX_DIST_CORES = Integer.MAX_VALUE;
  
  private String responderId;
  private String responderName;
  private String responderVersion;

  private String topicOfInterest;
  private String topicOut;
  private String topicErr;
  
  private int preferredCpus;

  /*
   * Parallelism limits
   */

//  private int maxInstances;
  private int maxLocalCores;
  private int maxDistributedCores;

  private boolean enabled;
  
  private long queuePopulatorRunEveryMs;
  private long queuePopulatorLastRunTimestamp;
  
  private String dataDestinationBucketOverride;
  private String logDestinationBucketOverride;

  public ResponderInfo()
  {
    maxLocalCores = DEFAULT_MAX_LOCAL_CORES;
    maxDistributedCores = DEFAULT_MAX_DIST_CORES;
    queuePopulatorRunEveryMs = DEFAULT_QUEUE_POP_RUN_EVERY_MS;
  }

  public String getResponderId()
  {
    return responderId;
  }

  public void setResponderId(String responderId)
  {
    this.responderId = responderId;
  }

  public String getResponderName()
  {
    return responderName;
  }

  public void setResponderName(String responderName)
  {
    this.responderName = responderName;
  }

  public String getResponderVersion()
  {
    return responderVersion;
  }

  public void setResponderVersion(String responderVersion)
  {
    this.responderVersion = responderVersion;
  }
  
  public boolean isEnabled()
  {
    return enabled;
  }

  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  public int getMaxDistributedCores()
  {
    return maxDistributedCores;
  }

  public void setMaxDistributedCores(int maxDistributedCores)
  {
    this.maxDistributedCores = maxDistributedCores;
  }

  public int getMaxLocalCores()
  {
    return maxLocalCores;
  }

  public void setMaxLocalCores(int maxLocalCores)
  {
    this.maxLocalCores = maxLocalCores;
  }

  public long getQueuePopulatorLastRunTimestamp()
  {
    return queuePopulatorLastRunTimestamp;
  }

  public void setQueuePopulatorLastRunTimestamp(long queuePopulatorLastRunTimestamp)
  {
    this.queuePopulatorLastRunTimestamp = queuePopulatorLastRunTimestamp;
  }

  public long getQueuePopulatorRunEveryMs()
  {
    return queuePopulatorRunEveryMs;
  }

  public void setQueuePopulatorRunEveryMs(long queuePopulatorRunEveryMs)
  {
    this.queuePopulatorRunEveryMs = queuePopulatorRunEveryMs;
  }

  public String getTopicOfInterest()
  {
    return topicOfInterest;
  }

  public void setTopicOfInterest(String topicOfInterest)
  {
    this.topicOfInterest = topicOfInterest;
  }

  public int getPreferredCpus()
  {
    return preferredCpus;
  }

  public void setPreferredCpus(int preferredCpus)
  {
    this.preferredCpus = preferredCpus;
  }

  public String getDataDestinationBucketOverride()
  {
    return dataDestinationBucketOverride;
  }

  public void setDataDestinationBucketOverride(String dataDestinationBucketOverride)
  {
    this.dataDestinationBucketOverride = dataDestinationBucketOverride;
  }

  public String getLogDestinationBucketOverride()
  {
    return logDestinationBucketOverride;
  }

  public void setLogDestinationBucketOverride(String logDestinationBucketOverride)
  {
    this.logDestinationBucketOverride = logDestinationBucketOverride;
  }

  public String getTopicErr()
  {
    return topicErr;
  }

  public void setTopicErr(String topicErr)
  {
    this.topicErr = topicErr;
  }

  public String getTopicOut()
  {
    return topicOut;
  }

  public void setTopicOut(String topicOut)
  {
    this.topicOut = topicOut;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder(getClass().getName());
    sb.append("[");
    sb.append("responderId: ").append(responderId);
    sb.append(", responderName: ").append(responderName);
    sb.append(", responderVersion: ").append(responderVersion);
    sb.append(", topic in: ").append(topicOfInterest);
    sb.append(", topic out: ").append(topicOut);
    sb.append(", topic err: ").append(topicErr);
    sb.append(", preferred CPUs: ").append(preferredCpus);
    sb.append(", maxLocalCores: ").append(maxLocalCores);
    sb.append(", maxDistributedCores: ").append(maxDistributedCores);
    sb.append(", enabled: ").append(enabled);
    sb.append(", queuePopulatorRunEveryMs: ").append(queuePopulatorRunEveryMs);
    sb.append(", queuePopulatorLastRunTimestamp: ").append(
        new Date(queuePopulatorLastRunTimestamp).toString());
    sb.append(", dataDestinationBucketOverride: ").append(dataDestinationBucketOverride);
    sb.append(", logDestinationBucketOverride: ").append(logDestinationBucketOverride);
    sb.append("]");
    return sb.toString();
  }

  
}
