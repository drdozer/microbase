/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */
package uk.org.microbase.filesystem.gridfs;

import java.io.File;
import java.io.InputStream;
import java.util.Map;
import java.util.Set;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.filesystem.spi.MicrobaseFS;

/**
 *
 * @author Keith Flanagan
 */
public class MbGridFS
    implements MicrobaseFS
{

  private boolean enabled;

  public MbGridFS()
  {
    enabled = false;
  }


  @Override
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  @Override
  public boolean isEnabled()
  {
    return enabled;
  }

  @Override
  public void upload(InputStream dataStream, String remoteBucket, String remotePath, String remoteName, Map<String, String> tags) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void upload(File dataFile, String remoteBucket, String remotePath, String remoteName, Map<String, String> tags) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public InputStream download(String remoteBucket, String remotePath, String remoteName) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void download(String remoteBucket, String remotePath, String remoteName, File destinationFile) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean exists(String remoteBucket, String remotePath, String remoteName) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Set<String> listFiles(String remoteBucket) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Set<String> listFiles(String remoteBucket, String remotePath) throws FSOperationNotSupportedException, FSException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }


}
