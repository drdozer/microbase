/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 11-Nov-2010, 22:58:28
 */

package uk.org.microbase.fs;

/**
 *
 * @author Keith Flanagan
 */
public class DownloadException
    extends Exception
{

  public DownloadException(Throwable thrwbl)
  {
    super(thrwbl);
  }

  public DownloadException(String string, Throwable thrwbl)
  {
    super(string, thrwbl);
  }

  public DownloadException(String string)
  {
    super(string);
  }

  public DownloadException()
  {
    super();
  }

}
