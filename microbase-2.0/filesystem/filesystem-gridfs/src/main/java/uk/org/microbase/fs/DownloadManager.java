/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 11-Nov-2010, 16:02:43
 */

package uk.org.microbase.fs;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A thread-safe component that may be used to download files from the
 * Microbase file system. Multiple threads can request the same file without
 * several independent copies of the file being downloaded, reducing disk
 * space and bandwidth requirements.
 *
 * @author Keith Flanagan
 */
public class DownloadManager
{
  private static final int MAX_CONCURRENT_DOWNLOADS = 10;

  private final FileDAO fileDao;
  private final File downloadDir;
  private final ScheduledThreadPoolExecutor exe;
  //private Map<String, Download> fileGuidToDownload;
  private final Map<String, ScheduledFuture<File>> fileGuidToFuture;

  public DownloadManager(FileDAO fileDao, File downloadDir)
  {
    this.fileDao = fileDao;
    this.downloadDir = downloadDir;
    exe = new ScheduledThreadPoolExecutor(MAX_CONCURRENT_DOWNLOADS);
    //fileGuidToDownload = new HashMap<String, Download>();
    fileGuidToFuture = new HashMap<String, ScheduledFuture<File>>();
  }

  public File download(String fileGuid) throws DownloadException
  {
    File file = new File(downloadDir, fileGuid);
    if (file.exists())
    {
      return file;
    }
//    Download dl = null;
//    synchronized(fileGuidToDownload)
//    {
//      dl = fileGuidToDownload.get(fileGuid);
//      if (dl == null)
//      {
//        dl = new Download(fileDao, downloadDir, fileGuid);
//        fileGuidToDownload.put(fileGuid, dl);
//        ScheduledFuture<File> future = exe.schedule(dl, 0, TimeUnit.MILLISECONDS);
//      }
//    }
    ScheduledFuture<File> future = null;
    synchronized(fileGuidToFuture)
    {
      future = fileGuidToFuture.get(fileGuid);
      if (future == null)
      {
        Download dl = new Download(fileDao, downloadDir, fileGuid);
        future = exe.schedule(dl, 0, TimeUnit.MILLISECONDS);
        fileGuidToFuture.put(fileGuid, future);
      }
    }

    try
    {
      for(;;)
      {
        try
        {
          file = future.get();
          synchronized(fileGuidToFuture)
          {
            fileGuidToFuture.remove(fileGuid);
          }
          return file;
        }
        catch(InterruptedException e)
        {
          Logger.getLogger(
              DownloadManager.class.getName()).log(Level.SEVERE, null, e);
        }
      }
    }
    catch (ExecutionException ex)
    {
      throw new DownloadException("Failed to download file: "+fileGuid, ex);
    }
  }
}
