/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 08-Nov-2010, 12:59:09
 */

package uk.org.microbase.fs;

import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import me.z80.mongodb.DBException;
import me.z80.mongodb.DBUtils;

/**
 *
 * @author Keith Flanagan
 */
public class FSClient
{
  private static final String USAGE = "Usage:\n" +
      "Config:\n"+
      "  --hostname <machine name> - DB hostname\n" +
      "  --filesystemDB <database name> - Filesystem database to use\n" +


      "\nCommands:\n"+
      "  --list-files - lists all files stored\n"+
      "  --put <path to file> - Stores a file in GridFS\n"+
      "  --put-with-properties <path to file> { set of key/value properties to associate with the file}" +
      "  --put-all <path to directory> - stores all files found in the specified directory\n"+
      "  --get-by-name <filename> - retrieves a copy of the file with the specified name\n"+
      "\n";


  private static DB filesystemDb;

  private static FileDAO fileDao;

  private static File destinationDir = new File(".");

  private static enum Command
  {
    LIST_FILES,
    PUT,
    PUT_WITH_PROPS,
    PUT_ALL,
    GET_BY_NAME;
  }

  public static void main(String[] args) throws DBException
  {
    if (args.length == 0)
    {
      System.out.println(USAGE);
      System.exit(1);
    }

    Command cmd = null;
    Set<String> hosts = new HashSet<String>();
    String filesystemDbName = null;

    int numMessages = 0;
    String path = null;
    String filename = null;
    Map<String, String> properties = new HashMap<String, String>();

    for (int i=0; i<args.length; i++)
    {
      if (args[i].equalsIgnoreCase("--hostname"))
      {
        hosts.add(args[++i]);
      }
      else if (args[i].equals("--filesystemDB"))
      {
        filesystemDbName = args[++i];
      }
      else if (args[i].equals("--list-files"))
      {
        cmd = Command.LIST_FILES;
      }
      else if (args[i].equals("--put"))
      {
        cmd = Command.PUT;
        path = args[++i];
      }
      else if (args[i].equals("--put-with-properties"))
      {
        cmd = Command.PUT_WITH_PROPS;
        path = args[++i];
        for (i++; i < args.length; i+=2 )
        {
          System.out.println(args[i]);
          if (args[i].startsWith("--"))
          {
            break;
          }
          properties.put(args[i], args[i+1]);
        }
      }
      else if (args[i].equals("--put-all"))
      {
        cmd = Command.PUT_ALL;
        path = args[++i];
      }
      else if (args[i].equals("--get-by-name"))
      {
        cmd = Command.GET_BY_NAME;
        filename = args[++i];
      }
    }

    ///Configure
    filesystemDb = DBUtils.connectDB(hosts.iterator().next(), filesystemDbName);
    fileDao = new FileDAO(filesystemDb);

    //Execute command
    if (cmd == null)
    {
      throw new RuntimeException("No command was specified!");
    }
    switch(cmd)
    {
      case LIST_FILES:
        listFiles();
        break;
      case GET_BY_NAME:
        getByName(filename);
        break;
      case PUT:
        put(new File(path));
        break;
      case PUT_ALL:
        putAll(new File(path));
        break;
      case PUT_WITH_PROPS:
        putWithProperties(new File(path), properties);
        break;
    }
  }

  private static void listFiles()
      throws DBException
  {
    for (DBCursor cursor = fileDao.list();
         cursor.hasNext();)
    {
      DBObject gridFile = cursor.next();
      System.out.println(gridFile.toString());
    }
  }


  private static void getByName(String filename)
      throws DBException
  {
    fileDao.downloadFile(destinationDir, filename);
  }

  private static void put(File file)
      throws DBException
  {
    fileDao.upload(file);
  }

  private static void putAll(File directory) throws DBException
  {
    for (File file : directory.listFiles())
    {
      fileDao.upload(file);
    }
  }

  private static void putWithProperties(File file, Map<String, String> props)
      throws DBException
  {
    fileDao.upload(file, props);
  }
}
