/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */
package uk.org.microbase.filesystem.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.org.microbase.filesystem.spi.*;
import uk.org.microbase.util.file.FileUtils;

/**
 * Implements the Microbase Filesystem SPI using a local directory. This
 * implementation is useful for running and debugging jobs on the same machine.
 * Alternatively, it may also be useful if you have a cluster of machines
 * sharing the same network filesystem. It may also find uses if you have a
 * Microbase installation on a single, large multi-CPU machine.
 *
 * The local directory implementation of the MicrobaseFS amalgamates the
 * 'bucket' and 'path' fields into a single directory path.
 *
 * @author Keith Flanagan
 */
public class AmazonFS
    implements MicrobaseFS
{

  private static final Logger logger =
      Logger.getLogger(AmazonFS.class.getName());
  
  private static final String TMP_PREFIX = ".mb_download_";
  private static final String DEFAULT_DELIMETER = "/";
  
  private static final String CONFIG_ENTRY =
      "/" + AmazonFS.class.getSimpleName() + ".properties";
  
  
  private static final String PROP_CACHE_DIR_ROOT = "cache_directory";
  private static final String PROP_ACCESS_KEY = "access_key";
  private static final String PROP_SECRET_KEY = "secret_key";
  
  private final String delimeter;
  private File localCacheDir;
  private boolean enabled;
  
  private RemoteOpMonitor monitor;
  private AmazonS3 s3client;

  
  
  public AmazonFS() throws FSException
  {
    delimeter = DEFAULT_DELIMETER;
    monitor = new RemoteOpMonitor();
    logger.log(Level.INFO, "{0} initialising", AmazonFS.class.getName());
    loadDefaultConfig();
    logger.log(Level.INFO, "{0} ready", AmazonFS.class.getName());
  }
  
  public AmazonFS(Properties config) throws FSException
  {
    delimeter = DEFAULT_DELIMETER;
    monitor = new RemoteOpMonitor();
    logger.log(Level.INFO, "{0} initialising", AmazonFS.class.getName());
    loadConfigFromProperties(config);
    logger.log(Level.INFO, "{0} ready", AmazonFS.class.getName());
  }
  
  

  /**
   * Loads the configuration for this provider from a file on the classpath.
   * This file configures the root directory to use for local file storage
   *
   * @throws FSException
   */
  private void loadDefaultConfig()
      throws FSException
  {
    Properties config = getDefaultConfigResource();
    loadConfigFromProperties(config);
  }
  
  private Properties getDefaultConfigResource()
      throws FSException
  {
    try {
      logger.log(Level.INFO,
          "Loading filesystem provider config from "
          + "CLASSPATH entry {0}", CONFIG_ENTRY);
      InputStream configStream =
          AmazonFS.class.getResourceAsStream(CONFIG_ENTRY);
      if (configStream == null)
      {
        throw new FSException("Configuration file: " + CONFIG_ENTRY
            + " could not be found on the CLASSPATH");
      }
      Properties config = new Properties();
      config.load(configStream);
      return config;
    }
    catch(Exception e) {
      throw new FSException("Failed to open stream for: "+CONFIG_ENTRY, e);
    }
  }
  
  private void loadConfigFromProperties(Properties config)
      throws FSException
  {
    try {
      String cacheDirProperty = config.getProperty(PROP_CACHE_DIR_ROOT);
      String accessKey = config.getProperty(PROP_ACCESS_KEY);
      String secretKey = config.getProperty(PROP_SECRET_KEY);
      _checkConfigPropertyNotNull(PROP_CACHE_DIR_ROOT, cacheDirProperty);
      _checkConfigPropertyNotNull(PROP_ACCESS_KEY, accessKey);
      _checkConfigPropertyNotNull(PROP_SECRET_KEY, secretKey);
      
      localCacheDir = new File(cacheDirProperty);
      logger.log(Level.INFO, "Using local cache directory: {0}",
                              localCacheDir.getAbsolutePath());
      if (!localCacheDir.exists())
      {
        logger.log(Level.INFO,
            "Attempting to create directory: {0}",
            localCacheDir.getAbsolutePath());
        localCacheDir.mkdirs();
      }
      
      logger.info("Connecting to Amazon S3");
      AWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
      s3client = new AmazonS3Client(creds);
    }
    catch (Exception e)
    {
      throw new FSException(
          "Failed to set configuration from properties: "
          + config.stringPropertyNames(), e);
    }

    setEnabled(true);
  }
  
  private void _checkConfigPropertyNotNull(String propName, String propVal)
      throws FSException
  {
    if (propVal == null)
    {
      throw new FSException("Config file "+CONFIG_ENTRY
          + " had no value for property: " + propName);
    }
  }
  
  /**
   * Amazon S3 is picky about leading / trailing delimeters. This method ensures
   * that there is no leading '/', and that there IS a trailing '/'
   * 
   * @param remotePath
   * @return 
   */
  private String _validateRemotePathString(String remotePath)
  {
    if (remotePath == null)
    {
      return null;
    }
    StringBuilder txt = new StringBuilder(remotePath);
    
    //Remove possible leading '/'
    if (remotePath.startsWith(delimeter))
    {
      logger.log(Level.INFO, 
          "WARNING: Removing leading slash from path: {0}", remotePath);
      txt.deleteCharAt(0);
    }
    
    //Add possibly missing trailing '/'
    if (!remotePath.endsWith("/"))
    {
      logger.log(Level.INFO, 
          "WARNING: Adding a trailing slash to path: {0}", remotePath);
      txt.append(delimeter);
    }
   
    return txt.toString();
  }

  @Override
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  @Override
  public boolean isEnabled()
  {
    return enabled;
  }
  
  /**
   * Returns a File that can be used as the source or destination for the 
   * locally-cached copy of a remote file.
   * 
   * If the necessary local directory paths to that file do not exist, an
   * attempt will be made to create them. However, the file itself will not be
   * created.
   * 
   * @param remoteBucket
   * @param remotePath
   * @param remoteName
   * @return
   * @throws FSException 
   */
  private File _getCachePathFor(
      String remoteBucket, String remotePath, String remoteName)
      throws FSException
  {
    //Create a directory in the local filesystem's cache directory
    File cacheLocalDir;
    if (remotePath == null) {
      cacheLocalDir = new File(localCacheDir, remoteBucket);
    } else {
      cacheLocalDir = new File(localCacheDir, 
        remoteBucket + File.separator + remotePath);
    }
    if (!cacheLocalDir.exists())
    {
      //Attempt to create the path in the local cache, if it doesn't exit
      cacheLocalDir.mkdirs();
    }
    //Make sure that the directory is actually a directory and is writable
    if (!cacheLocalDir.exists() || !cacheLocalDir.isDirectory() 
        || !cacheLocalDir.canWrite())
    {
      throw new FSException("Either the directory "
          + cacheLocalDir.getAbsolutePath()
          + " does not exist, "
          + "or it does exist but is not writable, "
          + "or it could not be created (either a permissions "
          + "problem, or a file exists at that location with the same name");
    }
    
    File cacheDestFile = new File(cacheLocalDir, remoteName);
    logger.info("Attempting to download the remote file: " 
        + remoteBucket + " ; "
        + remotePath + " ; "
        + remoteName + " to local cache: "+cacheDestFile.getAbsolutePath());

    return cacheDestFile;
  }
  
  private String _getAmazonKeyFor(String remotePath, String remoteName)
  {
    StringBuilder key = new StringBuilder();
    if (remotePath != null && remotePath.length() > 0)
    {
      key.append(remotePath);
    }
    key.append(remoteName);
    
    return key.toString();
  }
  
  private File _downloadFileToCache(
      String remoteBucket, String remotePath, String remoteName)
      throws FSException
  {
    /*
     * This method downloads the requested file and store in the cache directory
     */
    if (remoteBucket == null)
    {
      throw new FSException("Bucket name must not be NULL");
    }
    
    String amazonFileKey = _getAmazonKeyFor(remotePath, remoteName);
    GetObjectRequest request = new GetObjectRequest(remoteBucket, amazonFileKey);
    
    //The final path/filename of the cache entry
    File cacheDestFile = 
        _getCachePathFor(remoteBucket, remotePath, remoteName);
    
    //A temporary filename used for downloading purposes only
    File cacheDestTmpFile = new File(
        cacheDestFile.getParent(), TMP_PREFIX+cacheDestFile.getName());
    
    try
    {
      ObjectMetadata fileMeta = s3client.getObject(request, cacheDestTmpFile);
      logger.info("Downloaded file version: "+fileMeta.getVersionId());
      if (cacheDestTmpFile.renameTo(cacheDestFile))
      {
        return cacheDestFile;
      }
      else
      {
        throw new FSException("Failed to rename: "
            + cacheDestTmpFile.getAbsolutePath()
            + " to: " + cacheDestFile.getAbsolutePath());
      }
    }
    catch (AmazonServiceException e)
    {
      cacheDestTmpFile.delete();
      throw new FSException("Failed to retreive: "
          + remoteBucket + "; " + remotePath+"; " + remoteName, e);
    }
    catch (AmazonClientException e)
    {
      cacheDestTmpFile.delete();
      throw new FSException("Failed to retreive: "
          + remoteBucket + "; " + remotePath+"; " + remoteName, e);
    }
  }

  @Override
  public InputStream downloadToCache(
      String remoteBucket, String remotePath, String remoteName,
      boolean forceRedownload)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    try
    {
      monitor.notifyOperationStarting(remoteBucket, remotePath, remoteName);
      File cachedFile;
      if (forceRedownload || !_existsCached(remoteBucket, remotePath, remoteName))
      {
        cachedFile = _downloadFileToCache(remoteBucket, remotePath, remoteName);
      }
      else
      {
        cachedFile = _getCachePathFor(remoteBucket, remotePath, remoteName);
      }

      try
      {
        InputStream is = new FileInputStream(cachedFile);
        return is;
      }
      catch(IOException e)
      {
        throw new FSException("Failed to provide an InputStream for cached "
            + "copy of: " + remoteBucket + "; " + remotePath+"; " + remoteName, e);
      }
    }
    finally
    {
      monitor.notifyOperationComplete(remoteBucket, remotePath, remoteName);
    }
  }

  @Override
  public void downloadFileToSpecificLocation(
      String remoteBucket, String remotePath, String remoteName, 
      File destinationFile, boolean useCache)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    
    /*
     * If the caller requested to use the local file cache, and the file is
     * present, then make a copy to the specified destination.
     */
    if (useCache)
    {
      monitor.notifyOperationStarting(remoteBucket, remotePath, remoteName);
      try
      {
        if (!_existsCached(remoteBucket, remotePath, remoteName))
        {
          _downloadFileToCache(remoteBucket, remotePath, remoteName);
        }
        FileUtils.copyFile(
            _getCachePathFor(remoteBucket, remotePath, remoteName), 
            destinationFile);
      }
      catch(FSException e)
      {
        throw new FSException("Failed to download requested file: "
            + remoteBucket + "; " + remotePath+"; " + remoteName, e);
      }
      catch(IOException e)
      {
        throw new FSException("Failed to copy file from cache to specified "
            + "destination directory: "+destinationFile.getAbsolutePath()
            +". Remote file requested: "
            + remoteBucket + "; " + remotePath+"; " + remoteName, e);
      }
      finally
      {
        monitor.notifyOperationComplete(remoteBucket, remotePath, remoteName);
      }
    }
    else
      /*
       * Otherwise, download a copy to the specified destination location
       */
    {
      String amazonFileKey = _getAmazonKeyFor(remotePath, remoteName);
      GetObjectRequest request = new GetObjectRequest(remoteBucket, amazonFileKey);

      try
      {
        ObjectMetadata fileMeta = s3client.getObject(request, destinationFile);
        logger.info("Downloaded file version: "+fileMeta.getVersionId());
      }
      catch (AmazonServiceException e)
      {
        throw new FSException("Failed to retreive: "
            + remoteBucket + "; " + remotePath+"; " + remoteName, e);
      }
      catch (AmazonClientException e)
      {
        throw new FSException("Failed to retreive: "
            + remoteBucket + "; " + remotePath+"; " + remoteName, e);
      }
    }
  }

  @Override
  public boolean exists(String remoteBucket, String remotePath, String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    return true; //Not yet implemented
  }

  @Override
  public boolean existsCached(
      String remoteBucket, String remotePath, String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    try
    {
      monitor.notifyOperationStarting(remoteBucket, remotePath, remoteName);
      return _existsCached(remoteBucket, remotePath, remoteName);
    }
    finally
    {
      monitor.notifyOperationComplete(remoteBucket, remotePath, remoteName);
    }
  }
  
  private boolean _existsCached(
      String remoteBucket, String remotePath, String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    File cacheDestFile = _getCachePathFor(remoteBucket, remotePath, remoteName);
      return cacheDestFile.exists();
  }
  
  @Override
  public Set<String> listRemoteFilenames(String remoteBucket, String remotePath,
      boolean includeFiles, boolean includeDirectories, boolean listFullPathname)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    try
    {
      Set<String> names = new HashSet<String>();
      ListObjectsRequest request = new ListObjectsRequest();
      request.setDelimiter(delimeter);
      request.setBucketName(remoteBucket);
      request.setPrefix(remotePath);
      //request.setMaxKeys(1000);
      ObjectListing list = s3client.listObjects(request);
      do
      {
        if(includeFiles)
        {
          for (S3ObjectSummary objSummary : list.getObjectSummaries())
          {
            String fullKey = objSummary.getKey();
            int lastDelimPos = fullKey.lastIndexOf(delimeter);
            if (!listFullPathname && lastDelimPos >= 0)
            {
              fullKey = fullKey.substring(lastDelimPos+1, fullKey.length());
            }
            names.add(fullKey);
          }
        }
        
        //Directories
        if (includeDirectories)
        {
          for (String commonPrefix : list.getCommonPrefixes())
          {
            int lastDelimPos = commonPrefix.lastIndexOf(delimeter);
            if (!listFullPathname && lastDelimPos >= 0)
            {
              commonPrefix = commonPrefix.substring(lastDelimPos+1, commonPrefix.length());
            }
            names.add(commonPrefix);
          }
        }
        if (list.isTruncated())
        {
          System.out.println("Fetching next batch");
          list = s3client.listNextBatchOfObjects(list);
        }
        else
        {
          list = null;
        }
      } while(list != null);
      
      return names;
    }
    catch (AmazonServiceException e)
    {
      throw new FSException("Failed to retreive listing for: "
          + remoteBucket + "; " + remotePath, e);
    }
    catch (AmazonClientException e)
    {
      throw new FSException("Failed to retreive listing for: "
          + remoteBucket + "; " + remotePath, e);
    }
    
  }

  @Override
  public Set<FileMetaData> listRemoteFileMetaData(String remoteBucket, String remotePath)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    try
    {
//      System.out.println("RemotePath: "+remotePath);
      //Get filenames, excluding directories
      Set<String> names = listRemoteFilenames(remoteBucket, remotePath, true, false, false);
      Set<FileMetaData> metaData = new HashSet<FileMetaData>();
      
      for (String remoteName : names)
      {
//        System.out.println("RemoteName: "+remoteName);
        metaData.add(getRemoteFileMetaData(remoteBucket, remotePath, remoteName));
      }
      
      return metaData;
    }
    catch (AmazonServiceException e)
    {
      throw new FSException("Failed to retrieve file metadata for: "
          + remoteBucket + "; " + remotePath, e);
    }
    catch (AmazonClientException e)
    {
      throw new FSException("Failed to retrieve file metadata for: "
          + remoteBucket + "; " + remotePath, e);
    }
  }

  // note: this is adapted from a copy/paste of listRemoteFileMetaData
  @Override
  public Set<FileInfo> listRemoteDirectoryInfo(String remoteBucket, String remotePath) throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
        try
        {
    //      System.out.println("RemotePath: "+remotePath);
          //Get filenames, excluding directories
          Set<String> names = listRemoteFilenames(remoteBucket, remotePath, false, true, false);
          Set<FileInfo> metaData = new HashSet<FileInfo>();

          for (String remoteName : names)
          {
    //        System.out.println("RemoteName: "+remoteName);
            if(remoteName.endsWith("/"))
            {
              // fixme: we are build a FileMetaData here when we should be building a FileInfo only
              metaData.add(getRemoteFileMetaData(remoteBucket, remotePath, remoteName));
            }
          }

          return metaData;
        }
        catch (AmazonServiceException e)
        {
          throw new FSException("Failed to retrieve file metadata for: "
              + remoteBucket + "; " + remotePath, e);
        }
        catch (AmazonClientException e)
        {
          throw new FSException("Failed to retrieve file metadata for: "
              + remoteBucket + "; " + remotePath, e);
        }
  }

  @Override
  public FileMetaData getRemoteFileMetaData(
      String remoteBucket, String remotePath, String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    try
    {
      String key = _getAmazonKeyFor(remotePath, remoteName);
//      System.out.println("RemotePath: "+remotePath
//          + ", RemoteName: " + remoteName
//          +", Key: "+key);
      ObjectMetadata s3Data = s3client.getObjectMetadata(remoteBucket, key);
      
      FileMetaData fsData = new FileMetaData(remoteBucket, remotePath, remoteName);
      fsData.setLength(s3Data.getContentLength());
      fsData.setLastModified(s3Data.getLastModified());
      
      return fsData;
    }
    catch (AmazonServiceException e)
    {
      throw new FSException("Failed to retreive file metadata for: "
          + "bucket: " + remoteBucket + "; path: " + remotePath + "; name: " + remoteName, e);
    }
    catch (AmazonClientException e)
    {
      throw new FSException("Failed to retreive file metadata for: "
          + " bucket: " + remoteBucket + "; path: " + remotePath + "; name: " + remoteName, e);
    }
  }


  @Override
  public void upload(File dataFile,
      String remoteBucket, String remotePath, String remoteName,
      Map<String, String> tags)
      throws FSOperationNotSupportedException, FSException
  {
    remotePath = _validateRemotePathString(remotePath);
    try
    {
      monitor.notifyOperationStarting(remoteBucket, remotePath, remoteName);
      
      String remoteKey = _getAmazonKeyFor(remotePath, remoteName);
      s3client.putObject(remoteBucket, remoteKey, dataFile);
    }
    catch (AmazonServiceException e)
    {
      throw new FSException("Failed to upload file: "
          + dataFile.getAbsolutePath() 
          + " to: bucket: " + remoteBucket + "; path: " + remotePath 
          + "; name: " + remoteName, e);
    }
    catch (AmazonClientException e)
    {
      throw new FSException("Failed to upload file: "
          + dataFile.getAbsolutePath() 
          + " to: bucket: " + remoteBucket + "; path: " + remotePath 
          + "; name: " + remoteName, e);
    }
    finally
    {
      monitor.notifyOperationComplete(remoteBucket, remotePath, remoteName);
    }
  }

    @Override
    public void deleteRemoteCopy(String remoteBucket, String remotePath, String remoteName)
            throws FSOperationNotSupportedException, FSException 
    {
      remotePath = _validateRemotePathString(remotePath);
      try
      {
        monitor.notifyOperationStarting(remoteBucket, remotePath, remoteName);

        String remoteKey = _getAmazonKeyFor(remotePath, remoteName);
        s3client.deleteObject(remoteBucket, remoteKey);
      }
      catch (AmazonServiceException e)
      {
        throw new FSException("Failed to delete remote file: "
            + " bucket: " + remoteBucket + "; path: " + remotePath 
            + "; name: " + remoteName, e);
      }
      catch (AmazonClientException e)
      {
        throw new FSException("Failed to delete remote file: "
            + " bucket: " + remoteBucket + "; path: " + remotePath 
            + "; name: " + remoteName, e);
      }
      finally
      {
        monitor.notifyOperationComplete(remoteBucket, remotePath, remoteName);
       }
    }

    @Override
    public void deleteLocalCopy(String remoteBucket, String remotePath, String remoteName) 
          throws FSOperationNotSupportedException, FSException 
    {
      remotePath = _validateRemotePathString(remotePath);
      try
      {
        monitor.notifyOperationStarting(remoteBucket, remotePath, remoteName);
        File cacheDestFile = _getCachePathFor(remoteBucket, remotePath, remoteName);
        cacheDestFile.delete();
      }
      finally
      {
        monitor.notifyOperationComplete(remoteBucket, remotePath, remoteName);
      }
    }

}
