/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */
package uk.org.microbase.filesystem.spi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.util.file.FileUtils;

/**
 * A meta-MicrobaseFS provider that iterates over the known concrete filesystem
 * providers in order to give a federated view over all the known file retrieval
 * protocols.
 *
 * For example, if you have a local directory FS provider as well as a Amazon S3
 * storage provider enabled, then the AggregatedFSProvider will search both
 * locally and remotely for files.
 *
 * This provider has the following properties:
 * <ul>
 * <li>File write operations are performed by the first available concrete
 * provider (i.e., one that supports write operations and does not fail).
 * In case of a particlar provider failing to write a file, the next provider
 * is tried, and so on until either the file is written correctly to one of
 * the providers. If all available concrete providers fail to write the file, an
 * exception is thrown. In summary, only ONE provider needs to 'work' in order
 * for a file to be successfully stored.</li>
 * <li>Read operations are performed iteratively over each concrete
 * provider until a provider reports that it has a copy of the requested file.
 * If a particular provider throws an exception, this won't be propagated
 * further unless all available providers fail. Summary: at least one provider
 * storing the file needs to be working correctly in order to read it.
 * </li>
 * <li>Query operations (such as <code>exists</code> and <code>listBucket</code>
 * operations aggregate all results from all available concrete providers. This
 * means that the contents of a particular bucket (multiple files) may be
 * distributed across a number of providers. However, an individual file must
 * always be located on a single provider. Summary: all providers must be
 * available in order to correctly query or list the content of buckets.
 * </li>
 * </ul>
 *
 * @author Keith Flanagan
 */
public class AnyWriteMultiReadAggregatedFSProvider
    implements MicrobaseFS
{

  private static final Logger logger =
      Logger.getLogger(AnyWriteMultiReadAggregatedFSProvider.class.getName());
  private static ServiceLoader<MicrobaseFS> fsLoader =
      ServiceLoader.load(MicrobaseFS.class);

  private boolean enabled;

  public AnyWriteMultiReadAggregatedFSProvider()
  {
    setEnabled(true);
  }

  @Override
  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  @Override
  public boolean isEnabled()
  {
    return enabled;
  }


  private Set<MicrobaseFS> getProviders() throws FSException
  {
    Iterator<MicrobaseFS> impls = fsLoader.iterator();
    Set<MicrobaseFS> enabledImpls = new HashSet<MicrobaseFS>();
    while(impls.hasNext())
    {
      MicrobaseFS impl = impls.next();
      if (impl.isEnabled())
      {
        enabledImpls.add(impl);
      }
    }
    if (enabledImpls.isEmpty())
    {
      throw new FSException(
          "Could not perform file operation - no enabled "
          + MicrobaseFS.class.getName()
          + " SPI providers were available");
    }

    return enabledImpls;
  }

//  @Override
//  public void upload(InputStream dataStream,
//      String remoteBucket, String remotePath, String remoteName,
//      Map<String, String> tags)
//      throws FSOperationNotSupportedException, FSException
//  {
//    Exception lastEx = null;
//    for (MicrobaseFS impl : getProviders())
//    {
//      try
//      {
//        impl.upload(dataStream, remoteBucket, remotePath, remoteName, tags);
//        lastEx = null;
//        break;
//      }
//      catch (Exception e)
//      {
//        //This failure isn't reported unless all concrete providers fail
//        logger.info("Upload operation of "
//            + remoteBucket + ", " + remotePath + ", "+ remoteName
//            + " using provider "+impl.getClass().getName()
//            + " failed. Error was: "+e.getMessage());
//        lastEx = e;
//      }
//    }
//    if (lastEx != null)
//    {
//      throw new FSException("Failed to write data stream to "
//          + remoteBucket + ", " + remotePath + ", "+ remoteName
//          + ". All available providers failed.", lastEx);
//    }
//  }

  @Override
  public void upload(File dataFile, 
      String remoteBucket, String remotePath, String name,
      Map<String, String> tags)
      throws FSOperationNotSupportedException, FSException
  {
    upload(dataFile, remoteBucket, remotePath, name, tags);
//    FileInputStream is = null;
//    try
//    {
//      is = new FileInputStream(dataFile);
//      upload(is, remoteBucket, remotePath, name, tags);
//    }
//    catch(IOException e)
//    {
//      throw new FSException(
//          "Failed to upload file: "+dataFile.getAbsolutePath(), e);
//    }
//    finally
//    {
//      FileUtils.closeInputStreams(is);
//    }
  }

  @Override
  public InputStream downloadToCache(
      String remoteBucket, String remotePath, String remoteName, 
      boolean forceRedownload)
      throws FSOperationNotSupportedException, FSException
  {
    Exception lastEx = null;
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        logger.info("Using provider "+impl.getClass().getName()
            +" to obtain file: " + remoteBucket + ", " + remotePath +", "+remoteName);
        if (!impl.exists(remoteBucket, remotePath, remoteName))
          //If the provider doesn't have the file, move on to the next provider
        {
          logger.info("Provider "+impl.getClass().getName()
              + " did not have the specified file: "
              + remoteBucket + ", " + remotePath +", "+remoteName);
          continue;
        }
        InputStream stream = impl.downloadToCache(
            remoteBucket, remotePath, remoteName, forceRedownload);
        lastEx = null;
        return stream;
      }
      catch (Exception e)
      {
        /*
         * Catch this exception - in the case of a non-existent file, or problem
         * downloading the file, another FS provider might succeed. No exception
         * will be thrown until all providers fail.
         */
        logger.info("Download of: "
            + remoteBucket + ", " + remotePath + ", "+remoteName
            + " using provider "+impl.getClass().getName()
            + " failed. Error was: "+e.getMessage());
        lastEx = e;
      }
    }
    if (lastEx != null)
    {
      //At least one provider threw an exception - pass this on.
      throw new FSException("Failed to read data stream from "
          + remoteBucket + ", " + remotePath + ", "+remoteName
          + ". All available providers failed.", lastEx);
    }
    else
    {
      //No providers threw exceptions - most likely, none had the requested file
      throw new FSException("No MicrobaseFS provider could provide the file: "
          + remoteBucket + ", " + remotePath + ", "+remoteName);
    }
  }

  @Override
  public void downloadFileToSpecificLocation(
      String remoteBucket, String remotePath, String remoteName,
      File destinationFile, boolean useCache)
      throws FSOperationNotSupportedException, FSException
  {
    Exception lastEx = null;
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        logger.info("Using provider "+impl.getClass().getName()
            +" to obtain file: " + remoteBucket + ", " + remotePath +", "+remoteName);
        if (!impl.exists(remoteBucket, remotePath, remoteName))
          //If the provider doesn't have the file, move on to the next provider
        {
          logger.info("Provider "+impl.getClass().getName()
              + " did not have the specified file: "
              + remoteBucket + ", " + remotePath +", "+remoteName);
          continue;
        }
        impl.downloadFileToSpecificLocation(
            remoteBucket, remotePath, remoteName, destinationFile, useCache);
        lastEx = null;
        return;
      }
      catch (Exception e)
      {
        /*
         * Catch this exception - in the case of a non-existent file, or problem
         * downloading the file, another FS provider might succeed. No exception
         * will be thrown until all providers fail.
         */
        logger.info("Download of "
            + remoteBucket + ", " + remotePath + ", "+remoteName
            + " using provider "+impl.getClass().getName()
            + " failed. Error was: "+e.getMessage());
        lastEx = e;
      }
    }
    if (lastEx != null)
    {
      //At least one provider threw an exception - pass this on.
      throw new FSException("Failed to read data stream from "
          + remoteBucket + ", " + remotePath + ", "+remoteName
          + ". All available providers failed.", lastEx);
    }
    else
    {
      //No providers threw exceptions - most likely, none had the requested file
      throw new FSException("No MicrobaseFS provider could provide the file: "
          + remoteBucket + ", " + remotePath + ", "+remoteName);
    }
  }

  @Override
  public boolean exists(String remoteBucket, String remotePath, String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        if (impl.exists(remoteBucket, remotePath, remoteName))
          //If the provider has the file, report that the file is available
        {
          return true;
        }
      }
      catch (Exception e)
      {
        //Any exception results in an execption being thrown.
        logger.info("Querying the existance of "
            + remoteBucket + ", " + remotePath +", "+remoteName
            + " using provider "+impl.getClass().getName()
            + " failed. Error was: "+e.getMessage());
        throw new FSException("Failed to query the existance of: "
            + remoteBucket + ", " + remotePath +", "+remoteName
            + ". At least one available providers failed.", e);
      }
    }
    //No provider had a copy of the file
    return false;
  }
  

  @Override
  public boolean existsCached(String remoteBucket, String remotePath,
      String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        if (impl.existsCached(remoteBucket, remotePath, remoteName))
          //If the provider has the file, report that the file is available
        {
          return true;
        }
      }
      catch (Exception e)
      {
        //Any exception results in an execption being thrown.
        logger.info("Querying the existance of "
            + remoteBucket + ", " + remotePath +", "+remoteName
            + " using provider "+impl.getClass().getName()
            + " failed. Error was: "+e.getMessage());
        throw new FSException("Failed to query the existance of: "
            + remoteBucket + ", " + remotePath +", "+remoteName
            + ". At least one available providers failed.", e);
      }
    }
    //No provider had a copy of the file
    return false;
  }

  @Override
  public Set<String> listRemoteFilenames(String remoteBucket, String remotePath,
      boolean includeFiles, boolean includeDirectories, boolean listFullPathname)
      throws FSOperationNotSupportedException, FSException
  {
    Set<String> filenames = new HashSet<String>();
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        filenames.addAll(impl.listRemoteFilenames(
            remoteBucket, remotePath, includeFiles, includeDirectories, listFullPathname));
      }
      catch (Exception e)
      {
        //Any exception results in an execption being thrown.
        logger.info("Listing the contents of bucket "+remoteBucket
            + " with path: " + remotePath
            + " failed when using provider "+impl.getClass().getName()
            + " Error was: "+e.getMessage());
        throw new FSException("Failed to list the contents of bucket: "
            + remoteBucket + ", " + remotePath
            + ". At least one available providers failed.", e);
      }
    }
    return filenames;
  }


  @Override
  public Set<FileMetaData> listRemoteFileMetaData(
      String remoteBucket, String remotePath)
      throws FSOperationNotSupportedException, FSException
  {
    Set<FileMetaData> metaData = new HashSet<FileMetaData>();
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        metaData.addAll(impl.
            listRemoteFileMetaData(remoteBucket, remotePath));
      }
      catch (Exception e)
      {
        //Any exception results in an execption being thrown.
        logger.info("Listing the metadata of files in bucket "+remoteBucket
            + " with path: " + remotePath
            + " failed when using provider "+impl.getClass().getName()
            + " Error was: "+e.getMessage());
        throw new FSException("Failed to list the contents of bucket: "
            + remoteBucket + ", " + remotePath
            + ". At least one available providers failed.", e);
      }
    }
    return metaData;
  }

  @Override
  public Set<FileInfo> listRemoteDirectoryInfo(String remoteBucket, String remotePath) throws FSOperationNotSupportedException, FSException
  {
    Set<FileInfo> metaData = new HashSet<FileInfo>();
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        // fixme: got this far - it needs to be working from a list including directories
        metaData.addAll(impl.
                listRemoteFileMetaData(remoteBucket, remotePath));
      }
      catch (Exception e)
      {
        //Any exception results in an execption being thrown.
        logger.info("Listing the metadata of files in bucket "+remoteBucket
                + " with path: " + remotePath
                + " failed when using provider "+impl.getClass().getName()
                + " Error was: "+e.getMessage());
        throw new FSException("Failed to list the contents of bucket: "
                + remoteBucket + ", " + remotePath
                + ". At least one available providers failed.", e);
      }
    }
    return metaData;
  }

  @Override
  public FileMetaData getRemoteFileMetaData(String remoteBucket,
      String remotePath, String remoteName)
      throws FSOperationNotSupportedException, FSException
  {
    for (MicrobaseFS impl : getProviders())
    {
      try
      {
        FileMetaData data = impl.
            getRemoteFileMetaData(remoteBucket, remotePath, remoteName);
        if (data != null)
        {
          return data;
        }
      }
      catch (Exception e)
      {
        //Any exception results in an execption being thrown.
        logger.info("Listing the metadata of file: "
            +remoteBucket + ": " + remotePath + "; " +remoteName
            + " failed when using provider "+impl.getClass().getName()
            + " Error was: "+e.getMessage());
        throw new FSException("Failed to list the contents of bucket: "
            + remoteBucket + ", " + remotePath
            + ". At least one available providers failed.", e);
      }
    }
    
    //No metadata could be found in any provider
    return null;
  }

    @Override
    public void deleteRemoteCopy(String remoteBucket, String remotePath, String remoteName) throws FSOperationNotSupportedException, FSException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void deleteLocalCopy(String remoteBucket, String remotePath, String remoteName) throws FSOperationNotSupportedException, FSException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
