/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.org.microbase.filesystem.spi;

import java.io.Serializable;

/**
 *
 * @author Keith Flanagan
 */
public class FileInfo
    implements Serializable
{
  private String bucket;
  private String path;
  private String name;

  public FileInfo()
  {
  }

  public FileInfo(String bucket, String path, String name)
  {
    this.bucket = bucket;
    this.path = path;
    this.name = name;
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder("FileInfo{");
    sb.append("bucket=").append(bucket)
        .append(", path=").append(path)
        .append(", name=").append(name)
        .append("}");
    return sb.toString();
  }

  

  public String getBucket()
  {
    return bucket;
  }

  public void setBucket(String bucket)
  {
    this.bucket = bucket;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }
}
