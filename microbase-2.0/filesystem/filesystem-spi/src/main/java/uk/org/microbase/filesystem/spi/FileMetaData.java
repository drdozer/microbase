/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 27, 2012, 3:39:50 PM
 */

package uk.org.microbase.filesystem.spi;

import java.util.Date;

/**
 *
 * @author Keith Flanagan
 */
public class FileMetaData
    extends FileInfo
{
  private long length;
  private Date lastModified;
  
  public FileMetaData()
  {
  }
  
  public FileMetaData(String bucket, String path, String name)
  {
    super(bucket, path, name);
  }

  public FileMetaData(String bucket, String path, String name, 
      long length, Date lastModified)
  {
    super(bucket, path, name);
    this.length = length;
    this.lastModified = lastModified;
  }

  @Override
  public String toString()
  {
    return "FileMetaData{" + "length=" + length + ", lastModified=" + lastModified + '}';
  }

  public long getLength()
  {
    return length;
  }

  public void setLength(long length)
  {
    this.length = length;
  }

  public Date getLastModified()
  {
    return lastModified;
  }

  public void setLastModified(Date lastModified)
  {
    this.lastModified = lastModified;
  }



}
