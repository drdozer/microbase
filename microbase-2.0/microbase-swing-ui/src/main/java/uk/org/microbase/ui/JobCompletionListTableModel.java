/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.swing.table.DefaultTableModel;
import uk.org.microbase.dist.processes.ExecutionResult;

/**
 *
 * @author Keith Flanagan
 */
public class JobCompletionListTableModel
    extends DefaultTableModel
{
  
  private final Set<ExecutionResult> current;
  
  public JobCompletionListTableModel()
  {
    this.current = new HashSet<ExecutionResult>();
    addColumn("Proc start");
    addColumn("Work start");
    addColumn("Work completed");
    addColumn("Hostname");
    addColumn("Responder ID");
    addColumn("Topic ID");
    addColumn("Message ID");
    addColumn("No. global cores in use at proc start");
    addColumn("Assigned local cores");
    addColumn("Comment");
    
    fireTableStructureChanged();
  }

  public void add(ExecutionResult result)
  {
    Set<ExecutionResult> jobCompletionSet = 
        new HashSet<ExecutionResult>();
    jobCompletionSet.add(result);
    add(jobCompletionSet);
  }
  
  private boolean _add(ExecutionResult entry)
  {
    if (current.contains(entry))
    {
      return false;
    }
    current.add(entry);


    Object[] rowData = new Object[] {
      new Date(entry.getProcessStartedAtMs()).toString(),
      new Date(entry.getWorkStartedAtMs()).toString(),
      new Date(entry.getWorkCompletedAtMs()).toString(),
      entry.getHostname(),
      entry.getResponderId(),
      entry.getTopicId(),
      entry.getMessageId(),
      entry.getNumGlobalCoresInUseAtStart(),
      entry.getAssignedLocalCores(),
      entry.getComment()
    };
    
    this.addRow(rowData);
    return true;
  }
  
  public void add(Collection<ExecutionResult> entries)
  {
    int oldRowCount = getRowCount();
    int numAdded = 0;
    for (ExecutionResult entry : entries)
    {
      if (_add(entry))
      {
        numAdded ++;
      }
    }
    int newRowCount = getRowCount();
    
    if (newRowCount > oldRowCount)
    {
      if (oldRowCount > 0)
      {
        oldRowCount--;
      }
      fireTableRowsInserted(oldRowCount, newRowCount);
    }
    
  }
  
}
