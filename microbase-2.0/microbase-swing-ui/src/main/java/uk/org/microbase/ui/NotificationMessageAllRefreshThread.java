/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.List;
import uk.org.microbase.notification.data.Message;

/**
 * Queries for notification messages of all topics.
 * 
 * @author Keith Flanagan
 */
public class NotificationMessageAllRefreshThread
  implements Runnable
{
  private final SharedState state;
  private final NotificationMessageListTableModel model;
  
  public NotificationMessageAllRefreshThread(
      SharedState state, NotificationMessageListTableModel model)
  {
    this.state = state;
    this.model = model;
  }
  
  
  @Override
  public void run()
  {
    try
    {
//      state.getLog().append("Querying for notification messages");
      try
      {
        //FIXME this will be too slow for many msgs - query for new msgs since last time
        List<Message> msgs = state.getRuntime().getNotification().
            listMessagesInTimestampOrder(0, Integer.MAX_VALUE);
//        state.getLog().append("Found "+msgs.size()+" messages");
        
        model.addMessages(msgs);
      }
      catch(Exception e)
      {
        e.printStackTrace();
        state.getLog().append(
            "Failed to query for notification messages: "+e.getMessage());
      }
    }
    catch(Exception e)
    {
      System.out.println("Unexpected error occurred while processing");
      e.printStackTrace();
    }
  }
  
}
