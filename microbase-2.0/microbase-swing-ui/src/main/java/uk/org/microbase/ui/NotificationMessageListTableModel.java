/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import uk.org.microbase.notification.data.Message;

/**
 *
 * @author Keith Flanagan
 */
public class NotificationMessageListTableModel
    extends DefaultTableModel
{
  
  private final List<String> messageIds;
  
  public NotificationMessageListTableModel()
  {
    this.messageIds = new LinkedList<String>();
    addColumn("Published");
    addColumn("Topic");
    addColumn("Message ID");
    
    fireTableStructureChanged();
  }

  public List<String> getMessageIds()
  {
    return messageIds;
  }
  
  

  public void addMessage(Message msg)
  {
    Set<Message> msgSet = new HashSet<Message>();
    msgSet.add(msg);
    addMessages(msgSet);
//    if (ids.contains(msg.getGuid()))
//    {
//      return;
//    }
//    ids.add(msg.getGuid());
//    
//    msg.getResultSetId();
//    msg.getStepId();
//    msg.getParentMessage();
//    msg.getPublisherGuid();
//    
//    Object[] rowData = new Object[] {
//      new Date(msg.getPublishedTimestamp()).toString(),
//      msg.getTopicGuid(),
//      msg.getGuid()
//    };
//    
//    this.addRow(rowData);
//    
//    int first = 0;
//    int last = getRowCount();
//    
//    if (last > first)
//    {
//      first = last -1;
//    }
//    fireTableRowsInserted(first, last);
  }
  
  private boolean _addMessage(Message msg)
  {
    if (messageIds.contains(msg.getGuid()))
    {
      return false;
    }
    messageIds.add(msg.getGuid());

//    msg.getResultSetId();
//    msg.getStepId();
//    msg.getParentMessage();
//    msg.getPublisherGuid();

    Object[] rowData = new Object[] {
      new Date(msg.getPublishedTimestamp()).toString(),
      msg.getTopicGuid(),
      msg.getGuid()
    };
    
    this.addRow(rowData);
    return true;
  }
  
  public void addMessages(Collection<Message> msgs)
  {
    int oldRowCount = getRowCount();
    int numAdded = 0;
    for (Message msg : msgs)
    {
      if (_addMessage(msg))
      {
        numAdded ++;
      }
    }
//    System.out.println("Added "+numAdded+" messages");
    int newRowCount = getRowCount();
    
    if (newRowCount > oldRowCount)
    {
      if (oldRowCount > 0)
      {
        oldRowCount--;
      }
      fireTableRowsInserted(oldRowCount, newRowCount);
    }
    
  }
  
//  @Override
//  public int getRowCount()
//  {
//    throw new UnsupportedOperationException("Not supported yet.");
//  }
//
//  @Override
//  public int getColumnCount()
//  {
//    throw new UnsupportedOperationException("Not supported yet.");
//  }
//
//  @Override
//  public Object getValueAt(int i, int i1)
//  {
//    throw new UnsupportedOperationException("Not supported yet.");
//  }
  
}
