/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.org.microbase.dist.responder.ResponderInfo;

/**
 * Queries for notification messages of all topics.
 * 
 * @author Keith Flanagan
 */
public class ResponderQueueRefreshThread
  implements Runnable
{
  private final SharedState state;
  private final ResponderQueueTableModel model;
  
  public ResponderQueueRefreshThread(
      SharedState state, ResponderQueueTableModel model)
  {
    this.state = state;
    this.model = model;
  }
  
  
  @Override
  public void run()
  {
    try
    {
      try
      {
        List<ResponderInfo> responders =
          state.getRuntime().getRegistration().getRegisteredResponders();
        Map<String, Integer> responderToQueueSize = new HashMap<String, Integer>();        
        for (ResponderInfo responder : responders)
        {
          int size = state.getRuntime().
              getMessageQueues().
              getResponderJobQueueSize(responder.getResponderId());
          responderToQueueSize.put(responder.getResponderId(), size);

        }

//        System.out.println("Found "+responderToQueueSize.size()+" queues");
        model.update(responderToQueueSize);
      }
      catch(Exception e)
      {
        e.printStackTrace();
        state.getLog().append(
            "Failed to query for responders: "+e.getMessage());
      }
    }
    catch(Exception e)
    {
      System.out.println("Unexpected error occurred while processing");
      e.printStackTrace();
    }
  }
  
}
