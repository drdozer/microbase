/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author Keith Flanagan
 */
public class SharedState
{
  private static SharedState state;
  public static synchronized SharedState getState()
  {
    if (state == null)
    {
      throw new RuntimeException("State has not yet been initialised");
    }
    return state;
  }
  
  public static synchronized SharedState 
      createState(ClientRuntime runtime, Appendable log)
  {
    if (state != null)
    {
      throw new RuntimeException(
          "An attempt was made to create a new state object, but it has "
          + "already been initialised");
    }
    state = new SharedState(runtime, log);
    return state;
  }
  
  private final ClientRuntime runtime;
  
  private final Appendable log;
  
  private final ScheduledThreadPoolExecutor executor;

  private SharedState(ClientRuntime runtime, Appendable log)
  {
    this.runtime = runtime;
    this.log = log;
    this.executor = new ScheduledThreadPoolExecutor(3);
  }

  public ClientRuntime getRuntime()
  {
    return runtime;
  }

  public Appendable getLog()
  {
    return log;
  }

  public ScheduledThreadPoolExecutor getExecutor()
  {
    return executor;
  }
    
}
