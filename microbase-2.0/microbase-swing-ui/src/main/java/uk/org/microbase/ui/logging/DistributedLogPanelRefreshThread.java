/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui.logging;

import java.util.List;
import uk.org.microbase.ui.SharedState;

/**
 * Queries for notification messages of all topics.
 * 
 * @author Keith Flanagan
 */
public class DistributedLogPanelRefreshThread
  implements Runnable
{
  private final SharedState state;
  private final LogAppendable logAppendable;
  
  public DistributedLogPanelRefreshThread(
      SharedState state, LogAppendable logAppendable)
  {
    this.state = state;
    this.logAppendable = logAppendable;
  }
  
  
  @Override
  public void run()
  {
    try
    {
      try
      {
        List<String> distLog = state.getRuntime().getDistLog().getAllLogItems();
        
        StringBuilder sb = new StringBuilder();
        for (String line : distLog)
        {
          sb.append(line).append("\n");
        }
        
        logAppendable.setText(sb.toString());
      }
      catch(Exception e)
      {
        e.printStackTrace();
        state.getLog().append(
            "Failed to query for responders: "+e.getMessage());
      }
    }
    catch(Exception e)
    {
      System.out.println("Unexpected error occurred while processing");
      e.printStackTrace();
    }
  }
  
}
