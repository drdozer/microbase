/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.org.microbase.ui.logging;

import java.io.IOException;
import java.util.Date;
import javax.swing.JTextArea;

/**
 * Provides an implementation of Appendable that can be shared among various
 * GUI elements and can be used as a log for their actions. Typically, in
 * instance of LogAppendable will add a timestamp and then forward raw log text
 * to a GUI component, such as a TextArea being used as the container for the
 * log.
 * 
 * This implementation is thread safe.
 * 
 * @author Keith Flanagan
 */
public class LogAppendable
    implements Appendable
{
  //private final Appendable textContainer;
  private final JTextArea textContainer;
  public LogAppendable(JTextArea textContainer)
  {
    this.textContainer = textContainer;
  }
  
  @Override
  public Appendable append(CharSequence cs) throws IOException
  {
    StringBuilder sb = new StringBuilder();
    sb.append(new Date(System.currentTimeMillis()).toString());
    sb.append(": ").append(cs);
    sb.append("\n");
    
    synchronized(textContainer)
    {
      textContainer.append(sb.toString());
    }
    
    return this;
  }

  @Override
  public Appendable append(CharSequence cs, int i, int i1) throws IOException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Appendable append(char c) throws IOException
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  
  public void clear()
  {
    textContainer.setText("");
  }
  
  public void setText(String text)
  {
    textContainer.setText(text);
  }
  
  public String getText()
  {
    return textContainer.getText();
  }
  
}
