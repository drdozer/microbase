/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 8, 2012, 5:14:34 PM
 */

package uk.org.microbase.ui.messagetree;

import java.awt.Component;
import java.awt.Dialog;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import uk.org.microbase.notification.data.Message;

/**
 *
 * @author Keith Flanagan
 */
public class MessageCellRenderer
    extends DefaultTreeCellRenderer
    //implements TreeCellRenderer
{
  private final Map<String, Icon> topicToIcons;
  
  public MessageCellRenderer()
  {
    topicToIcons = new HashMap<String, Icon>();
    topicToIcons.put("msg.processing.failure", UIManager.getIcon("OptionPane.errorIcon"));
    topicToIcons.put("msg.processing.success", UIManager.getIcon("OptionPane.informationIcon"));
  }

  @Override
  public Component getTreeCellRendererComponent(JTree tree, Object value,
      boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus)
  {
    super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
    
    StringBuilder label = new StringBuilder();
    MessageNode node = (MessageNode) value;
    if (node.getMessage() == null)
    {
      setText("Message list:");
      return this;
    }
    Message msg = node.getMessage();
    label.append("<html>");
    label.append(new Date(msg.getPublishedTimestamp()).toString());
    label.append(": ").append(msg.getTopicGuid());
    label.append(", <b>ID:</b> ").append(msg.getGuid());
    label.append(", <b>From:</b> ").append(msg.getPublisherGuid());
    label.append("</html>");    
      
    setText(label.toString());

    Icon topicSpecificIcon = topicToIcons.get(msg.getTopicGuid());
    
    if (topicSpecificIcon != null)
    {
      setIcon(topicSpecificIcon);
    }
//    
//    if (leaf)
//    {
//      setIcon(leafIcon);
//    }
//    else if (expanded)
//    {
//      setIcon(openIcon);
//    }
//    else
//    {
//      setIcon(closedIcon);
//    }
//    return new JLabel(label.toString());
    
    return this;
  }

}
