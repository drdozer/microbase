/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 8, 2012, 5:20:01 PM
 */

package uk.org.microbase.ui.messagetree;

import java.util.ArrayList;
import java.util.List;
import uk.org.microbase.notification.data.Message;

/**
 *
 * @author Keith Flanagan
 */
public class MessageNode
{
  private static int count = 0;
  
  private String parentMsgId;
  private int directChildCount;
  private List<MessageNode> directChildren;
  //private String messageId;
  private Message message;
  
//  //A quick hack to increase performance of root node. Other nodes don't use this
//  private transient List<Integer> rootNodeChildCounts;
  
  public MessageNode()
  {
    count++;
    System.out.println("Creating new MessageNode: "+count);
    
    directChildren = null;
    directChildCount = -1;
  }

  public int getDirectChildCount()
  {
    return directChildCount;
  }

  public void setDirectChildCount(int directChildCount)
  {
    this.directChildCount = directChildCount;
  }

  public Message getMessage()
  {
    return message;
  }

  public void setMessage(Message message)
  {
    this.message = message;
  }

  public String getParentMsgId()
  {
    return parentMsgId;
  }

  public void setParentMsgId(String parentMsgId)
  {
    this.parentMsgId = parentMsgId;
  }

  public List<MessageNode> getDirectChildren()
  {
    return directChildren;
  }

  public void setDirectChildren(List<MessageNode> directChildren)
  {
    this.directChildren = directChildren;
  }
//
//  @Deprecated
//  public List<Integer> getRootNodeChildCounts()
//  {
//    return rootNodeChildCounts;
//  }
//
//  @Deprecated
//  public void setRootNodeChildCounts(List<Integer> rootNodeChildCounts)
//  {
//    this.rootNodeChildCounts = rootNodeChildCounts;
//  }
 
  
}
