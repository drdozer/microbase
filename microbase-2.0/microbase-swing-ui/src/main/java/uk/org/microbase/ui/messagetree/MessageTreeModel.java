/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 8, 2012, 5:13:48 PM
 */

package uk.org.microbase.ui.messagetree;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.notification.spi.NotificationException;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author Keith Flanagan
 */
public class MessageTreeModel
    implements TreeModel
{
  private static final Logger l = 
      Logger.getLogger(MessageTreeModel.class.getName());
  private final ClientRuntime runtime;
  private final MicrobaseNotification notif;
  private final MessageNode root;
  private final Set<TreeModelListener> listeners;
  
  private final Map<String, Integer> tmpMsgToChildCounts;
  
  public MessageTreeModel(ClientRuntime runtime)
  {
    this.listeners = new HashSet<TreeModelListener>();
    this.runtime = runtime;
    this.notif = runtime.getNotification();
    this.root = new MessageNode();
    this.tmpMsgToChildCounts = new HashMap<String, Integer>();
  }
  
  private void ensureChildrenLoaded(MessageNode node)
  {
    if (node.getDirectChildren() == null)
    {
      try
      {
        List<Message> children;
        if (node == root)
        {
          System.out.println("Getting root messages");
          children = notif.listRootMessages(false, 0, Integer.MAX_VALUE);
          
          System.out.println("Getting root grandchild counts");
          List<String> msgIds = new ArrayList<String>(children.size());
          for (Message child : children)
          {
            msgIds.add(child.getGuid());
          }
          List<Integer> childCounts = notif.getChildMessageCounts(msgIds);
          //Pre-cache grandchild counts
          for (int i=0; i<msgIds.size(); i++)
          {
            tmpMsgToChildCounts.put(msgIds.get(i), childCounts.get(i));
          }
        }
        else
        {
  //        List<String> childIds = notif.
  //            listChildMessageIds(node.getMessage().getGuid(), 0, Integer.MAX_VALUE);
          children = notif.listChildMessages(
              node.getMessage().getGuid(), false, 0, Integer.MAX_VALUE);
        }
        node.setDirectChildren(msgsToNodes(node.getParentMsgId(), children));
        node.setDirectChildCount(children.size());
      }
      catch (NotificationException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * There are several places we can get a child node count:
   * 1) the child nodes have already be retrieved from the database - we can
   *    simply count the collection
   * 2) the child nodes have not yet been populated, but we have previously
   *    received a 'count' from the database
   * 3) Neither - in which case we need to obtain a 'count' from the DB.
   * @param node 
   */
  private int getChildMessageCount(MessageNode node)
  {
    if (node.getDirectChildCount() >= 0)
    {
      return node.getDirectChildCount();
    }
    try
    {
      int count;
      if (node == root)
      {
        count = notif.getRootMessageCount();
      }
      else if (tmpMsgToChildCounts.containsKey(node.getMessage().getGuid()))
      {
        count = tmpMsgToChildCounts.get(node.getMessage().getGuid());
        tmpMsgToChildCounts.remove(node.getMessage().getGuid());
      }
      else
      {
        count = notif.getChildMessageCount(node.getMessage().getGuid());
      }
      node.setDirectChildCount(count);
      return count;
    }
    catch(NotificationException e)
    {
      e.printStackTrace();
      return 0;
    }
  }
  
  private List<MessageNode> msgsToNodes(String parentId, List<Message> children)
  {
    List<MessageNode> nodes = new ArrayList<MessageNode>(children.size());
    for (Message child : children)
    {
      MessageNode msgNode = new MessageNode();
      msgNode.setMessage(child);
      msgNode.setParentMsgId(parentId);
      nodes.add(msgNode);
    }
    return nodes;
  }
  
//  private List<MessageNode> idsToNodes(String parentId, List<String> ids)
//  {
//    List<MessageNode> nodes = new ArrayList<MessageNode>(ids.size());
//    for (String id : ids)
//    {
//      MessageNode msgNode = new MessageNode();
//      msgNode.setMessageId(id);
//      msgNode.setParentMsgId(parentId);
//      nodes.add(msgNode);
//    }
//    return nodes;
//  }

  @Override
  public Object getRoot()
  {
    l.info("Returning root object");
    return root;
  }

  @Override
  public Object getChild(Object o, int i)
  {
    l.log(Level.INFO, "Returning child {0} from {1}", new Object[]{i, o});
    MessageNode node = (MessageNode) o;
    ensureChildrenLoaded(node);

    return node.getDirectChildren().get(i);
  }

  @Override
  public int getChildCount(Object o)
  {
//    l.info("Counting children of: "+o);
    MessageNode node = (MessageNode) o;
    //ensureChildrenLoaded(node);
    //int count = node.getDirectChildren().size();
    
    int count = getChildMessageCount(node);
//    l.info("Counting children of: "+o+": "+count);
    return count;
  }

  @Override
  public boolean isLeaf(Object o)
  {
//    l.info("Determining if: "+o+" is a leaf");
    if (o == root)
    {
      return false;
    }
    MessageNode node = (MessageNode) o;
    int count = getChildMessageCount(node);
    return count == 0;
  }

  @Override
  public void valueForPathChanged(TreePath tp, Object o)
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int getIndexOfChild(Object parent, Object child)
  {
//    l.info("Determining the index of: "+child+" in: "+parent);
    if (parent == null || child == null)
    {
      return -1;
    }
    MessageNode parentMsg = (MessageNode) parent;
    ensureChildrenLoaded(parentMsg);
    return parentMsg.getDirectChildren().indexOf(child);
  }

  @Override
  public void addTreeModelListener(TreeModelListener tl)
  {
    listeners.add(tl);
  }

  @Override
  public void removeTreeModelListener(TreeModelListener tl)
  {
    listeners.remove(tl);
  }

}
