CREATE TABLE responders(
    guid character varying(255) NOT NULL,
    check_demand_every_ms bigint NOT NULL,
    max_instances int NOT NULL,
    enabled boolean NOT NULL,
    
    file_bucket character varying(255) NOT NULL,
    file_path character varying(255) NOT NULL,
    file_name character varying(255) NOT NULL

    PRIMARY KEY (guid)
);

CREATE TABLE messages(
    guid character varying(255) NOT NULL,
    published_timestamp bigint NOT NULL,
    expires_timestamp bigint NOT NULL,
    topic_guid character varying(255) NOT NULL,
    publisher_guid character varying(255) NOT NULL,
    parent_msg_guid character varying(255) NOT NULL,
    
    result_set_id character varying(255) NOT NULL,
    step_id character varying(255) NOT NULL,
    user_desc character varying(255),
    
    content_keys character varying(255)[],
    content_vals text[],
    binary_content bytea,
    
    PRIMARY KEY (guid)
);
CREATE INDEX messages__published_timestamp ON messages USING btree (published_timestamp);
CREATE INDEX messages__topic_guid ON messages USING btree (topic_guid);

CREATE TABLE message_states(
    message_guid character varying(255) NOT NULL,
    responder_guid character varying(255) NOT NULL,
    topic_guid character varying(255) NOT NULL,
    processing_state character varying(255) NOT NULL,
    description character varying(255),
    timestamp bigint NOT NULL,

    PRIMARY KEY (message_guid, responder_guid)
);
CREATE INDEX message_states__processing_state ON message_states USING btree (processing_state);

