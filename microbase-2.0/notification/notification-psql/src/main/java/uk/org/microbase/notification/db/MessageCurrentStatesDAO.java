/*
 * Copyright 2010 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.microbase.notification.db;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.data.Message.State;
import uk.org.microbase.notification.data.MessageStateInfo;
import uk.org.microbase.util.db.DBException;

/**
 * Allows setting and querying message/responder states.
 *
 * @author Keith Flanagan
 */
public class MessageCurrentStatesDAO
{
  private static final Logger logger =
      Logger.getLogger(MessageCurrentStatesDAO.class.getName());

  private DistributedJdbcPool pool;
  private MessageStateLogDAO stateLogDao;

  public MessageCurrentStatesDAO()
  {
    stateLogDao = null;
  }

  /**
   * Creates a message state table DAO with optional state history log.
   * Whenever a message/responder state object is modified, the a new state
   * log entry is also created. 
   * 
   * When specifying a delegate state log object here, state change events will
   * only ever by *added* to the log. Calling various 'remove' operations on
   * <code>this</code> will not remove anything from the log.
   * 
   * @param stateLogDao an optional message state logger
   */
  public MessageCurrentStatesDAO(MessageStateLogDAO stateLogDao)
  {
    this.stateLogDao = stateLogDao;
  }
  
  public void setDbPool(DistributedJdbcPool dbPool)
  {
    this.pool = dbPool;
  }
  public DistributedJdbcPool getDbPool()
  {
    return pool;
  }



  private void createNewState(Connection txn, MessageStateInfo stateInfo)
      throws DBException
  {
    final Timestamp now = new Timestamp(System.currentTimeMillis());
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "INSERT INTO message_current_states "
          + "(message_guid, responder_guid, processing_state, "
          + "hostname, description, timestamp) VALUES (?,?,?,?,?,?)";

      stmt = txn.prepareStatement(sql);

      int col = 1;
      stmt.setString(col++, stateInfo.getMessageGuid());
      stmt.setString(col++, stateInfo.getResponderGuid());
      stmt.setString(col++, stateInfo.getState().name());
      stmt.setString(col++, stateInfo.getHostname());
      stmt.setString(col++, stateInfo.getDescription());
      stmt.setTimestamp(col++, now);

      stmt.executeUpdate();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new DBException("Failed to insert message state for: "
          + " message: "+stateInfo.getMessageGuid()
          + ", responder: "+stateInfo.getResponderGuid()
          + ", state: "+stateInfo.getState()
          + ", hostname: "+stateInfo.getHostname()
          + ", desc: "+stateInfo.getDescription(), e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
    
    if (stateLogDao != null)
    {
      stateLogDao.logStateChange(txn, stateInfo);
    }
  }
  
  public void updateState(Connection txn, MessageStateInfo stateInfo)
      throws DBException
  {
    final Timestamp now = new Timestamp(System.currentTimeMillis());
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "UPDATE message_current_states SET "
          + "processing_state = ?, hostname = ?, description = ?, timestamp = ? "
          + "WHERE message_guid = ? AND responder_guid = ?";

      stmt = txn.prepareStatement(sql);

      int col = 1;
      stmt.setString(col++, stateInfo.getState().name());
      stmt.setString(col++, stateInfo.getHostname());
      stmt.setString(col++, stateInfo.getDescription());
      stmt.setTimestamp(col++, now);
      
      stmt.setString(col++, stateInfo.getMessageGuid());
      stmt.setString(col++, stateInfo.getResponderGuid());

      stmt.executeUpdate();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new DBException("Failed to update message state for: "
          + " message: "+stateInfo.getMessageGuid()
          + ", responder: "+stateInfo.getResponderGuid()
          + ", state: "+stateInfo.getState()
          + ", hostname: "+stateInfo.getHostname()
          + ", desc: "+stateInfo.getDescription(), e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
    
    if (stateLogDao != null)
    {
      stateLogDao.logStateChange(txn, stateInfo);
    }
  }


  public boolean exists(Connection txn,
      String messageGuid, String responderGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) AS count FROM message_current_states "
        + "WHERE message_guid = ? AND responder_guid = ?");
      stmt.setString(1, messageGuid);
      stmt.setString(1, responderGuid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute count()", e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  /**
   * Removes all state information for the specified message, wrt the specified
   * responder.
   * 
   * Note that this method only removes the *current* state entry. If there is
   * an associated message state log delegate, then the history in that log will
   * remain. You will need to manually call <code>removeState</code> on the log 
   * as well if you wish to remove all state item history as well.
   * 
   * @param txn
   * @param messageGuid
   * @param responderGuid
   * @throws DBException
   */
  public void removeState(Connection txn,
      String messageGuid, String responderGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "DELETE FROM message_current_states "
        + "WHERE message_guid = ? AND responder_guid = ?");
      stmt.setString(1, messageGuid);
      stmt.setString(2, responderGuid);
      stmt.executeUpdate();
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute removeState() for "
          + "message: "+messageGuid
          + ", responder: "+responderGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  public Message.State getState(Connection txn,
      String messageGuid, String responderGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT processing_state FROM message_current_states "
        + "WHERE message_guid = ? AND responder_guid = ?");
      stmt.setString(1, messageGuid);
      stmt.setString(2, responderGuid);

      ResultSet rs = stmt.executeQuery();
      if (!rs.next())
      {
        return null;
      }
      return Message.State.valueOf(rs.getString("processing_state"));
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getState() for "
          + "message: "+messageGuid
          + ", responder: "+responderGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public MessageStateInfo getStateInfo(Connection txn,
      String messageGuid, String responderGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM message_current_states "
        + "WHERE message_guid = ? AND responder_guid = ?");
      stmt.setString(1, messageGuid);
      stmt.setString(2, responderGuid);

      ResultSet rs = stmt.executeQuery();
      List<MessageStateInfo> stateInfo = resultSetToMessageStates(rs);
      if (stateInfo.size() == 1)
      {
        return stateInfo.get(0);
      }
      else if (stateInfo.isEmpty())
      {
        return null;
      }
      else
      {
        throw new SQLException(stateInfo.size()+" MessageStateInfo instances had "
            + "the same PK: "+messageGuid+", "+ responderGuid
            + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getStateInfo() for "
          + "message: "+messageGuid
          + ", responder: "+responderGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }

  /**
   * Returns the set of message IDs of messages the are in state
   * <code>state</code>, for the specified responder: <code>responderGuid</code>.

   * @param txn
   * @param responderGuid the responder that the caller is interested in.
   * @param state the message state that the caller is interested in, eg, 'READY'.
   * @param limit the maximum number of messages to return
   * @return the IDs of messages for responder <code>responderGuid</code> whose
   * last known state was <code>state</code>. i.e., the distinct set of
   * message IDs of messages whose current (latest) state entry is
   * <code>state</code>.
   * @throws DBException
   */
  public Set<String> getMessagesInState(Connection txn,
      String responderGuid, Message.State state, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT message_guid FROM message_current_states "
        + "WHERE responder_guid = ? AND processing_state = ? "
          + "LIMIT ?");
      
      stmt.setString(1, responderGuid);
      stmt.setString(2, state.name());
      stmt.setInt(3, limit);

      Set<String> messageGuids = new HashSet<String>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        messageGuids.add(rs.getString("message_guid"));
      }
      return messageGuids;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute getMessagesInState() for "
          + ", responder: "+responderGuid+", state: "+state, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  /**
   * Given a responder GUID and a topic, finds the set of message IDs that do
   * not have any state entries in the message state log table. This effectively
   * finds all new messages that have not yet been seen by the specified
   * responder.
   *
   * @param topicGuid the topic of messages to include in the search
   * @param responderGuid the responder to find new messages for
   * @return the set of message IDs that are 'new' to this responder - i.e.,
   * have no state log entries.
   */
  public Set<String> findMessagesNewToResponder(
      Connection txn, String topicGuid, String responderGuid)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT guid FROM messages m WHERE topic_guid = ? "
        + "AND m.guid NOT IN ("
            + "SELECT message_guid FROM message_current_states "
            + "WHERE responder_guid = ?)");
      stmt.setString(1, topicGuid);
      stmt.setString(2, responderGuid);

      ResultSet rs = stmt.executeQuery();
      Set<String> guids = new HashSet<String>();

      while(rs.next())
      {
        guids.add(rs.getString("guid"));
      }

      return guids;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute findMessagesNewToResponder() for "
          + "topic: "+topicGuid
          + ", responder: "+responderGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }


  /**
   * Given a responder GUID and a topic, finds the set of message IDs that do
   * not have any state entries in the message state log table. This effectively
   * finds all new messages that have not yet been seen by the specified
   * responder. A 'READY' state entry is then logged for each message found.
   *
   * @param topicGuid the topic of messages to include in the search
   * @param responderGuid the responder to find new messages for
   * @param hostname the hostname of the caller - required in order to generate
   * the initial state info object.
   * @return the number of new messages found and set to a READY state for
   * <code>responderGuid</code> and topic: <code>topic</code>.
   */
  public int findMessagesNewToResponderAndSetReady(
      Connection txn, String topicGuid, String responderGuid, String hostname)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT guid FROM messages m WHERE topic_guid = ? "
        + "AND m.guid NOT IN ("
            + "SELECT message_guid FROM message_current_states "
            + "WHERE responder_guid = ?)");
      stmt.setString(1, topicGuid);
      stmt.setString(2, responderGuid);

      ResultSet rs = stmt.executeQuery();
      int count = 0;
      while(rs.next())
      {
        //TODO might want to consider batch inserts here?
        createNewState(txn, new MessageStateInfo(
            rs.getString("guid"), responderGuid, State.READY, hostname));
        count++;
      }

      return count;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute findMessagesNewToResponderAndSetReady() for "
          + "topic: "+topicGuid
          + ", responder: "+responderGuid, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
  
  public List<MessageStateInfo> listAllMessageStatesByTimestamp(
      Connection txn, int offset, int limit)
      throws DBException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM message_current_states "
        + "ORDER BY timestamp ASC "
        + "LIMIT ? OFFSET ?");
      stmt.setInt(1, offset);
      stmt.setInt(2, limit);

      ResultSet rs = stmt.executeQuery();
      List<MessageStateInfo> stateInfo = resultSetToMessageStates(rs);
      return stateInfo;
    }
    catch(Exception e)
    {
      throw new DBException(
          "Failed to execute listAllStateEntriesByTimestamp() for "
          + "offset: " + offset
          + ", limit: " + limit, e);
    }
    finally
    {
      JdbcUtils.silentClose(stmt);
    }
  }
      


  private static List<MessageStateInfo> resultSetToMessageStates(ResultSet rs)
      throws SQLException
  {
    List<MessageStateInfo> list = new ArrayList<MessageStateInfo>();
    while(rs.next())
    {
      MessageStateInfo msg = new MessageStateInfo();

      msg.setDescription(rs.getString("description"));
      msg.setHostname(rs.getString("hostname"));
      msg.setMessageGuid(rs.getString("message_guid"));
      msg.setResponderGuid(rs.getString("responder_guid"));
      msg.setState(State.valueOf(rs.getString("processing_state")));
      msg.setTimestamp(new java.util.Date(rs.getTimestamp("timestamp").getTime()));
      
      list.add(msg);
    }
    return list;
  }



}
