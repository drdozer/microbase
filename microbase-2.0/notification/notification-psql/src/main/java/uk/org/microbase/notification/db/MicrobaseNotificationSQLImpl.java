/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.notification.db;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.data.Message.State;
import uk.org.microbase.notification.data.MessageStateInfo;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.notification.spi.NotificationException;

/**
 *
 * @author Keith Flanagan
 */
public class MicrobaseNotificationSQLImpl
    implements MicrobaseNotification
{
  private static final Logger logger =
      Logger.getLogger(MicrobaseNotificationSQLImpl.class.getName());
  
  private final MessageDAO messageDao;
  private final MessageCurrentStatesDAO messageStateDao;
  private final MessageStateLogDAO messageStateLogDao;

//  private final DataSource dataSource;
  private final String hostname;
  private DistributedJdbcPool pool;

  /**
   * 
   * @param hostname a hostname to use for logging purposes (every message 
   * state change needs to be associated with the host that performed the change)
   * @param pool a database pool to use
   */
  public MicrobaseNotificationSQLImpl(
       String hostname, DistributedJdbcPool pool)
  {
    this.hostname = hostname;
    this.pool = pool;
    this.messageDao = new MessageDAO();
    this.messageStateLogDao = new MessageStateLogDAO();
    this.messageStateDao = new MessageCurrentStatesDAO(messageStateLogDao);
  }

  public void setDbPool(DistributedJdbcPool dbPool)
  {
    this.pool = dbPool;
  }
  public DistributedJdbcPool getDbPool()
  {
    return pool;
  }
  

  @Override
  public void publish(String publisherGuid, String topicGuid,
      Map<String, String> content, byte[] binaryContent)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getWritableConnection();
      messageDao.publish(txn, publisherGuid, topicGuid, content, binaryContent);
      txn.commit();
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public void publish(Message message) throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getWritableConnection();
      messageDao.publish(txn, message);
      txn.commit();
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public void publish(Collection<Message> messages) throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getWritableConnection();
      for (Message message : messages)
      {
        messageDao.publish(txn, message);
      }
      txn.commit();
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public void publishAndSetState(Collection<Message> messages,
      String messageGuid, String responderGuid, State newState)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getWritableConnection();
      for (Message message : messages)
      {
        messageDao.publish(txn, message);
      }

      MessageStateInfo stateInfo = new MessageStateInfo();
      stateInfo.setMessageGuid(messageGuid);
      stateInfo.setResponderGuid(responderGuid);
      stateInfo.setState(newState);
      stateInfo.setHostname(hostname);

      messageStateDao.updateState(txn, stateInfo);
      txn.commit();
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public Set<String> findMessagesInState(
      String responderGuid, Message.State state, int maxMessages)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageStateDao.getMessagesInState(
          txn, responderGuid, state, maxMessages);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public Message getMessageAndSetState(
      String messageGuid, String responderGuid, State state)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();

      Message msg = messageDao.getMessage(txn, messageGuid);
      
      MessageStateInfo stateInfo = new MessageStateInfo();
      stateInfo.setMessageGuid(messageGuid);
      stateInfo.setResponderGuid(responderGuid);
      stateInfo.setState(state);
      stateInfo.setHostname(hostname);

      messageStateDao.updateState(txn, stateInfo);
      return msg;
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public Message getMessage(String messageGuid)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();

      Message msg = messageDao.getMessage(txn, messageGuid);

      return msg;
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public Map<String, Message> getMessages(Set<String> messageGuids)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();

      Map<String, Message> guidToMsgs = new HashMap<String, Message>();
      for (String messageGuid : messageGuids)
      {
        Message msg = messageDao.getMessage(txn, messageGuid);
        guidToMsgs.put(messageGuid, msg);
      }

      return guidToMsgs;
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
    public State getMessageState(
      String messageGuid, String responderGuid)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      State state = messageStateDao.getState(txn, messageGuid, responderGuid);
      return state;
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public void setMessageState(
      String messageGuid, String responderGuid, State newState)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getWritableConnection();
      
      
      MessageStateInfo stateInfo = new MessageStateInfo();
      stateInfo.setMessageGuid(messageGuid);
      stateInfo.setResponderGuid(responderGuid);
      stateInfo.setState(newState);
      stateInfo.setHostname(hostname);
      
      messageStateDao.updateState(txn, stateInfo);
      txn.commit();
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public int prepareNewMessagesForResponder(
      String responderGuid, String topicGuid)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getWritableConnection();

      /*
       * Generate initial 'READY' state entries so that the message can be
       * processed.
       */
      //int count = messageStateLogDao.findMessagesNewToResponderAndSetReady(
      int count = messageStateDao.findMessagesNewToResponderAndSetReady(
          txn, topicGuid, responderGuid, hostname);
      logger.log(Level.INFO, "Found {0} new messages with topic: {1} for "
          + "responder: {2}", new Object[]{count, topicGuid, responderGuid});

      txn.commit();
      return count;
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public List<Message> listMessagesInTimestampOrder(
      int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listMessagesInTimestampOrder(txn, offset, limit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public List<Message> listMessagesInTimestampOrder(
      String topicId, long afterTimestamp, int preferredLimit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listMessagesInTimestampOrder(
          txn, topicId, afterTimestamp, preferredLimit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public List<Message> listMessagesByTopic(
      String topicGuid, int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listMessagesByTopic(txn, topicGuid, offset, limit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public List<Message> listMessagesByPublisher(
      String publisherId, int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listMessagesByPublisher(txn, publisherId, offset, limit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public long count()
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.count(txn);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public long countMessagesWithTopic(String topicGuid)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.countMessagesWithTopic(txn, topicGuid);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public List<MessageStateInfo> listCurrentMessageStates(int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageStateDao.listAllMessageStatesByTimestamp(txn, offset, limit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

  @Override
  public List<MessageStateInfo> listMessageStateLogEntriesInTimestampOrder(
      int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageStateLogDao.listAllStateEntriesByTimestamp(txn, offset, limit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public List<String> listRootMessageIds(int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listRootMessageIds(txn, offset, limit);

    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public List<Message> listRootMessages(boolean includeContent, int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listRootMessages(txn, includeContent, offset, limit);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  
  @Override
  public int getRootMessageCount()
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.getRootMessageCount(txn);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public List<String> listChildMessageIds(String parentId, int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listChildMessageIds(txn, parentId, offset, limit);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public List<Message> listChildMessages(String parentId, 
      boolean includeContent, int offset, int limit)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.listChildMessages(
          txn, parentId, includeContent, offset, limit);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public int getChildMessageCount(String parentId)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.getChildMessageCount(txn, parentId);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
  @Override
  public List<Integer> getChildMessageCounts(List<String> parentIds)
      throws NotificationException
  {
    Connection txn = null;
    try
    {
      txn = pool.getReadOnlyConnection();
      return messageDao.getChildMessageCounts(txn, parentIds);
    }
    catch (Exception ex)
    {
      JdbcUtils.silentRollback(txn);
      throw new NotificationException("Operation failed", ex);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }
  
}
