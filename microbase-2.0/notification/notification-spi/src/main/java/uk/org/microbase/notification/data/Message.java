/*
 * Copyright 2010 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.microbase.notification.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Stores a single message in the notification system database. A message has
 * a number of fields that serve the different needs of different types of
 * consumers. The primary purpose of a message is to store application-specific
 * content for communication among responders. This data is stored in
 * <code>content</code> and <code>binaryContent</code>, respectively.
 *
 * Messages store a number of other fields for a variety of purposes, including:
 * <ul>
 * <li>Data of use to Microbase housekeeping processes - current processing
 * states of various responders (e.g., PROCESSING, SUCCESS, ERROR states.</li>
 * <li>Provenance data, such as which responder published the data, when
 * it was published, what user data set the message is associated with, etc.</li>
 * </ul>
 *
 * Once published, message content is immutable except for the following:
 * <ul>
 * <li>Responder state flags that are updated according to the outcome of
 * message processing by various responders</li>
 * <li>metaTags - user writable tags that may be added, modified or deleted
 * at any time.</li>
 * </ul>
 * @author Keith Flanagan
 */
public class Message
    implements Serializable
{

  public enum State
      implements Serializable
  {
//    NEW,
    READY,
    BLOCKED_AWAITING_LOCKS,
    PROCESSING,
    ERROR_TIMEOUT,
    ERROR_FAILED,
    FINISHED_SUCCEEDED,
    FINISHED_SKIPPED;
  };

  private String guid;
  private long publishedTimestamp;
  private long expiresTimestamp;
  private String topicGuid;
  private String publisherGuid;
  private String parentMessage;

  /**
   * If this message is part of the processing for a particular set of results,
   * then this is the resultset to which it belongs. This String should uniquely
   * identify a specific workflow's execution of a particular dataset.
   */
  private String workflowExeId;

  /**
   * Uniquely identifies the part of the workflow that this message belongs to.
   * Whereas <code>workflowExeId<code> identifies the pipeline execution,
   * <code>workflowStepId</code> identifies a particular point within that
   * pipeline (i.e., a particular data item being processed by a responder).
   */
  private String workflowStepId;

  /**
   * If this message forms a job description for some computational work,
   * then this is the unique job ID.
   */
//  private String jobId;
  /**
   * Optional human readable message for tracking / debugging purposes. This
   * content field is completely ignored for message processing reasons.
   */
  private String userDescription;


  /**
   * A list of responders IDs that have correctly processed this message.
   */
//  private Map<String, State> responderStates;

  /**
   * Key/value metadata tags that may be added to a message at any time, even
   * after it is published. Tags are useful for responders to flag messages in
   * various ways.
   */
//  private Map<String, String> metaTags;

  /**
   * Forms a key -> value based message content.
   */
  private Map<String, String> content;

  /**
   * Binary content can be stored in this field, if required
   */
  private byte[] binaryContent;

  /**
   * Convenience factory method that creates a new message and chains it to
   * a 'parent' message. This involves setting result set IDs as well
   * as setting the new message's parentId field.
   * 
   * Content and metadata fields are still blank.
   * 
   * This method is useful when you want to create a new message (for example
   * a job description) that results from an incoming notification (for example
   * a previous responder's "new data" notification).
   * 
   * @param parent the parent message to copy fields from.
   * @return a new message configured with an existing parent message
   */
  public static Message createMessageFromExisting(Message parent)
  {
    Message msg = new Message();
    msg.setParentMessage(parent.getGuid());
    msg.setWorkflowExeId(parent.getWorkflowExeId());
    return msg;
  }
  public Message()
  {
    publishedTimestamp = System.currentTimeMillis();
//    responderStates = new HashMap<String, State>();
//    metaTags = new HashMap<String, String>();
    content = new HashMap<String, String>();
    binaryContent = new byte[0];
  }

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder("Message{");
    sb.append("guid=").append(guid)
        .append(", publishedTimestamp=").append(publishedTimestamp)
        .append(", expiresTimestamp=").append(expiresTimestamp)
        .append(", topicGuid=").append(topicGuid)
        .append(", publisherGuid=").append(publisherGuid)
        .append(", parentMessage=").append(parentMessage)
        .append(", workflowExeId=").append(workflowExeId)
        .append(", workflowStepId=").append(workflowStepId)
        .append(", userDescription=").append(userDescription)
//        .append(", responderStates=").append(responderStates)
//        .append(", metaTags=").append(metaTags)
        .append(", content=").append(content);
    if (binaryContent != null)
    {
      sb.append(", binaryContent=").append(binaryContent.length);
    }
    sb.append('}');
    return sb.toString();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final Message other = (Message) obj;
    if ((this.guid == null) ? (other.guid != null) : !this.guid.equals(other.guid))
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 3;
    hash = 67 * hash + (this.guid != null ? this.guid.hashCode() : 0);
    return hash;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public byte[] getBinaryContent()
  {
    return binaryContent;
  }

  public void setBinaryContent(byte[] binaryContent)
  {
    this.binaryContent = binaryContent;
  }

  public long getExpiresTimestamp()
  {
    return expiresTimestamp;
  }

  public void setExpiresTimestamp(long expiresTimestamp)
  {
    this.expiresTimestamp = expiresTimestamp;
  }

  public long getPublishedTimestamp()
  {
    return publishedTimestamp;
  }

  public void setPublishedTimestamp(long publishedTimestamp)
  {
    this.publishedTimestamp = publishedTimestamp;
  }

  public String getPublisherGuid()
  {
    return publisherGuid;
  }

  public void setPublisherGuid(String publisherGuid)
  {
    this.publisherGuid = publisherGuid;
  }

  public String getTopicGuid()
  {
    return topicGuid;
  }

  public void setTopicGuid(String topicGuid)
  {
    this.topicGuid = topicGuid;
  }

  public Map<String, String> getContent()
  {
    return content;
  }

  public void setContent(Map<String, String> content)
  {
    this.content = content;
  }

  public String getParentMessage()
  {
    return parentMessage;
  }

  public void setParentMessage(String parentMessage)
  {
    this.parentMessage = parentMessage;
  }

  public String getUserDescription()
  {
    return userDescription;
  }

  public void setUserDescription(String userDescription)
  {
    this.userDescription = userDescription;
  }

  public String getWorkflowExeId()
  {
    return workflowExeId;
  }

  public void setWorkflowExeId(String workflowExeId)
  {
    this.workflowExeId = workflowExeId;
  }

  public String getWorkflowStepId()
  {
    return workflowStepId;
  }

  public void setWorkflowStepId(String workflowStepId)
  {
    this.workflowStepId = workflowStepId;
  }
}
