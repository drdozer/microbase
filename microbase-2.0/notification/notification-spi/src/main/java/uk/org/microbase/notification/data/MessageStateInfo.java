/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.org.microbase.notification.data;

import java.util.Date;
import uk.org.microbase.notification.data.Message.State;


/**
 *
 * @author Keith Flanagan
 */
public class MessageStateInfo
{
  private String messageGuid;
  private String responderGuid;
  private Message.State state;
  private String hostname;
  private String description;
  private Date timestamp;

  public MessageStateInfo()
  {
    timestamp = new Date(System.currentTimeMillis());
  }

  public MessageStateInfo(String messageGuid, String responderGuid, 
      State state, String hostname)
  {
    this();
    this.messageGuid = messageGuid;
    this.responderGuid = responderGuid;
    this.state = state;
    this.hostname = hostname;
  }

  public MessageStateInfo(String messageGuid, String responderGuid,
      State state, String hostname, String description)
  {
    this();
    this.messageGuid = messageGuid;
    this.responderGuid = responderGuid;
    this.state = state;
    this.hostname = hostname;
    this.description = description;
  }

  public MessageStateInfo(String messageGuid, String responderGuid,
      State state, String hostname, String description, Date timestamp)
  {
    this();
    this.messageGuid = messageGuid;
    this.responderGuid = responderGuid;
    this.state = state;
    this.hostname = hostname;
    this.description = description;
    this.timestamp = timestamp;
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final MessageStateInfo other = (MessageStateInfo) obj;
    if ((this.messageGuid == null) ? (other.messageGuid != null) : !this.messageGuid.equals(other.messageGuid))
    {
      return false;
    }
    if ((this.responderGuid == null) ? (other.responderGuid != null) : !this.responderGuid.equals(other.responderGuid))
    {
      return false;
    }
    if (this.state != other.state)
    {
      return false;
    }
    if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description))
    {
      return false;
    }
    if (this.timestamp != other.timestamp && (this.timestamp == null || !this.timestamp.equals(other.timestamp)))
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 5;
    hash = 47 * hash + (this.messageGuid != null ? this.messageGuid.hashCode() : 0);
    hash = 47 * hash + (this.responderGuid != null ? this.responderGuid.hashCode() : 0);
    hash = 47 * hash + (this.state != null ? this.state.hashCode() : 0);
    hash = 47 * hash + (this.description != null ? this.description.hashCode() : 0);
    hash = 47 * hash + (this.timestamp != null ? this.timestamp.hashCode() : 0);
    return hash;
  }



  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getMessageGuid()
  {
    return messageGuid;
  }

  public void setMessageGuid(String messageGuid)
  {
    this.messageGuid = messageGuid;
  }

  public String getResponderGuid()
  {
    return responderGuid;
  }

  public void setResponderGuid(String responderGuid)
  {
    this.responderGuid = responderGuid;
  }

  public State getState()
  {
    return state;
  }

  public void setState(State state)
  {
    this.state = state;
  }

  public Date getTimestamp()
  {
    return timestamp;
  }

  public void setTimestamp(Date timestamp)
  {
    this.timestamp = timestamp;
  }

  public String getHostname()
  {
    return hostname;
  }

  public void setHostname(String hostname)
  {
    this.hostname = hostname;
  }

}
