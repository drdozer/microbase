/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 16, 2012, 4:52:03 PM
 */

package ${package}.helloworld;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;

/**
 * This class implements a simple Microbase responder. It is configured to
 * react to messages with the topic 'HelloTopic'. 
 * 
 * @author Keith Flanagan
 */
public class HelloWorldProcessor
    extends AbstractMessageProcessor
{

  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(300);
    info.setMaxLocalCores(50);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName("HelloWorldResponder");
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest("HelloTopic");
    return info;
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    System.out.println("Doing preRun");
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    System.out.println("Doing cleanup");
  }

  @Override
  public Set<Message> processMessage(Message incomingMessage)
      throws ProcessingException
  {
    System.out.println("Processing message");
    for (int i=0; i<100; i++)
    {
      try
      {
        /*
         * Simulate work by pausing for a while.
         */
        Thread.sleep(1000);
        System.out.print(".");
        
        /*
         * We can optionally notify Microbase of this job's current progress.
         * You should set a value between 0 and 100.
         */
        notifyProgress(i);
      }
      catch(InterruptedException e)
      {
        e.printStackTrace();
      }
    }
    System.out.println("");
    
    /*
     * Here, we simulate a job failure by throwing an exception for 10% of 
     * HelloWorld jobs.
     */
    if (Math.random() > 0.9)
    {
      System.out.println("\n\n*****  SIMULATING JOB FAILURE BY THROWING EXCEPTION  *****\n\n");
      throw new ProcessingException("Simulated job failure!");
    }
    
    /*
     * All 'work' has now been completed, so the final action to perform is
     * to prepare a notification message to send on job completion. You could
     * send multiple messages of different types here.
     */
    Set<Message> toPublish = new HashSet<Message>();
    
    /*
     * The 'createMessage' method is provided for convenience. It takes an 
     * existing message -- in this case 'incomingMessage' -- and creates a
     * child message. The new message 'done' has 'incomingMessage' set as its
     * parent. Various other fields such as the workflow execution ID are also
     * set automatically.
     */
    Message done = createMessage(
        incomingMessage,               //The message to use as a parent
        "HelloWorldDone",              //The topic of the new message
        "HelloWorld-"+Math.random(),   //The workflow step ID
        //An optional human-readable description
        "User description of how the job completed successfully. "
        + "This is optional and can be NULL");
    //Arbitrary message content items can be added like this:
    done.getContent().put("some key", "some value");
    done.getContent().put("completed timestamp", 
        new Date(System.currentTimeMillis()).toString());

    toPublish.add(done);
    return toPublish;
  }
  
}
