/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 16, 2012, 4:52:03 PM
 */

package uk.org.microbase.cli_responder;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;
import uk.org.microbase.util.file.zip.ZipUtils;

/**
 * This class implements a simple fully-functional Microbase responder capable
 * of executing generic command lines. The command lines and their requirements
 * (input files, etc) should be encoded in a notification message. This 
 * responder then obtains the necessary input files from the notification system,
 * executes the command line, and uploads any specified output files.
 * See the full tutorial for more details:
 * http://www.microbasecloud.com/developer-documentation/getting-started
 * 
 * Alternatively, this responder can be used as a superclass for a more 
 * specific responder implementation. If you subclass this responder, be sure to
 * change (at the minimum) the responder ID, and incoming/outgoing topic names.
 * 
 * @author Keith Flanagan
 */
public class CommandLineProcessor
    extends AbstractMessageProcessor
{
  private static final Logger l = Logger.
      getLogger(CommandLineProcessor.class.getSimpleName());
  private static final String TOPIC_IN_PREFIX = "-RUN_COMMAND";
  private static final String TOPIC_OUT_PREFIX = "-COMPLETED";
  
  
  private static final String PREFIX_COMMAND = "command_";
//  private static final String COMMAND_0 = PREFIX_COMMAND + "0";
  private static final String PREFIX_REQUIRES_FILE = "requires_file_";
  private static final String PREFIX_REQUIRES_ZIP = "requires_zip_";
  private static final String PREFIX_OUTFILE = "outfile_";
  private static final String VAR_ASSIGNED_CPUS = "%assigned_cpus";
  private static final String VAR_WORKING_DIR = "%working_dir";
  private static final String VAR_MESSAGE_GUID = "%message_guid";
  
  /*
   * --------------------------------------------------------------------------
   * If you subclass the CLP, make sure you override at least the next 2 methods
   * --------------------------------------------------------------------------
   */
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(300);
    info.setMaxLocalCores(50);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(CommandLineProcessor.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(info.getResponderId()+TOPIC_IN_PREFIX);
    return info;
  }
  
  protected String getOutgoingTopic()
  {
    String outTopic = getResponderInfo().getResponderId()+TOPIC_OUT_PREFIX;
    return outTopic;
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    System.out.println("Doing preRun");
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    System.out.println("Doing cleanup");
  }
  
  private JobFile parseInputFile(String key, String value)
      throws ProcessingException
  {
    String[] remoteLocalSplit = value.split("\\|");
//    System.out.println("RemoteLocalSplit: "+Arrays.asList(remoteLocalSplit));
    if (remoteLocalSplit.length != 2)
    {
      throw new ProcessingException(
          "Input file: "+key+" was specified in the wrong format. Should be "
          + "of the form: remote_bucket remote_path remote_name | local_path");
    }
    
    return _parseRemoteLocalFileParts(remoteLocalSplit[1], remoteLocalSplit[0]);
  }
  
  private JobFile parseOutputFile(String key, String value)
      throws ProcessingException
  {
    String[] localRemoteSplit = value.split("\\|");
    if (localRemoteSplit.length != 2)
    {
      throw new ProcessingException(
          "Output file: "+key+" was specified in the wrong format. Should be "
          + "of the form: local_path | remote_bucket remote_path remote_name");
    }
    
    return _parseRemoteLocalFileParts(localRemoteSplit[0], localRemoteSplit[1]);
  }
  
  private JobFile _parseRemoteLocalFileParts(String localPart, String remotePart)
      throws ProcessingException
  {
    remotePart = remotePart.trim();
    localPart = localPart.trim();
    
    JobFile file = new JobFile();
    
    String[] remote = remotePart.split("\\s");
//    System.out.println("++++++++++++++++++++++++ Remote part: "+Arrays.asList(remote));
    if (remote.length == 2)
    {
      file.setBucket(remote[0]);
      file.setName(remote[1]);
    }
    else if (remote.length == 3)
    {
      file.setBucket(remote[0]);
      file.setPath(remote[1]);
      file.setName(remote[2]);
    }
    else
    {
      throw new ProcessingException("Message content "
          + " had an unexpected number of items");
    }
    
    localPart = localPart.trim();
    if (localPart.startsWith("/"))
    {
      localPart = localPart.substring(1);
    }
    file.setLocalPath(localPart);
    return file;
  }
  
  private void downloadFileDependencies(Message message)
      throws ProcessingException
  {
    Map<String, String> content = message.getContent();
    for (String key : content.keySet())
    {
      if (key.startsWith(PREFIX_REQUIRES_FILE))
      {
        JobFile fileDesc = parseInputFile(
            key, expandVars(content.get(key), message));
        File destination = new File(getWorkingDirectory(), fileDesc.getLocalPath());
        
        try
        {
          getRuntime().getMicrobaseFS().
              downloadFileToSpecificLocation(
              fileDesc.getBucket(), fileDesc.getPath(), fileDesc.getName(),
              destination, true);
        }
        catch(Exception e)
        {
          throw new ProcessingException("Failed to download input file: "
              + key + ": " + content.get(key), e);
        }
      }
    }
  }
  
  private void downloadAndExtractZipDependencies(Message message)
      throws ProcessingException
  {
    Map<String, String> content = message.getContent();
    for (String key : content.keySet())
    {
      if (key.startsWith(PREFIX_REQUIRES_ZIP))
      {
        JobFile fileDesc = parseInputFile(
            key, expandVars(content.get(key), message));
//        File destination = new File(getWorkingDirectory(), fileDesc.getLocalPath());
        
        try
        {
          InputStream zipStream = getRuntime().getMicrobaseFS().
              downloadToCache(
              fileDesc.getBucket(), fileDesc.getPath(), fileDesc.getName(), false);
          ZipUtils.extractZip(zipStream, getWorkingDirectory());
        }
        catch(Exception e)
        {
          throw new ProcessingException("Failed to download input file: "
              + key + ": " + content.get(key), e);
        }
      }
    }
  }
  
  private void uploadOutputFiles(Message message)
      throws ProcessingException
  {
    Map<String, String> content = message.getContent();
    for (String key : content.keySet())
    {
      if (key.startsWith(PREFIX_OUTFILE))
      {
        JobFile fileDesc = parseOutputFile(
            key, expandVars(content.get(key), message));
        File source = new File(getWorkingDirectory(), fileDesc.getLocalPath());
        
        try
        {
          l.log(Level.INFO, "Uploading output file: {0} to: {1}; {2}; {3}", 
              new Object[]{source.getAbsolutePath(), fileDesc.getBucket(), 
                fileDesc.getPath(), fileDesc.getName()});
          getRuntime().getMicrobaseFS().
              upload(source,
              fileDesc.getBucket(), fileDesc.getPath(), fileDesc.getName(), null);
        }
        catch(Exception e)
        {
          throw new ProcessingException("Failed to upload output file: "
              + key + ": " + content.get(key), e);
        }
      }
    }
  }
  
  /**
   * Example parser to show how a command line string could be parsed - a more
   * generic method would be required for multiple command lines in a message.
   */
//  private String[] parseCommandLine_0(Message message) 
//      throws ProcessingException
//  {
//    String commandStr = message.getContent().get(COMMAND_0);
//    if (commandStr == null)
//    {
//      throw new ProcessingException("No command string was specified!");
//    }
//    
//    commandStr = expandVars(commandStr, message);
//    
//    return commandStr.split("\\s");
//  }
  
  /**
   * Given a Message, returns a list of parsed command line strings. The list of
   * commands returned is in numerical order - i.e., COMMAND_0 will be returned
   * before COMMAND_1.
   * @param message
   * @return a list of command lines. Each command line is represented by an
   * array of Strings.
   * @throws ProcessingException 
   */
  private List<String[]> parseCommandLines(Message message) 
      throws ProcessingException
  {
    List<String[]> commands = new ArrayList<String[]>();
    
    for (int i=0; i<Integer.MAX_VALUE; i++)
    {
      String commandKey = PREFIX_COMMAND + i;
      String commandStr = message.getContent().get(commandKey);
      if (commandStr == null)
      {
        //No more command line strings to parse
        break;
      }
      
      commandStr = expandVars(commandStr, message);
      commands.add(commandStr.split("\\s"));
    }
    
    l.info("Parsed: "+commands.size()+" command lines");
    return commands;
  }
  
  private String expandVars(String original, Message message)
  {
    String replaced = original
      .replaceAll(VAR_ASSIGNED_CPUS, 
                  String.valueOf(getProcessInfo().getAssignedLocalCores()))
      .replaceAll(VAR_WORKING_DIR, getWorkingDirectory().getAbsolutePath())
      .replaceAll(VAR_MESSAGE_GUID, message.getGuid());
    return replaced;
  }

  @Override
  public Set<Message> processMessage(Message incomingMessage)
      throws ProcessingException
  {
    l.info("Obtaining input files");
    downloadFileDependencies(incomingMessage);
    downloadAndExtractZipDependencies(incomingMessage);
    
//    String[] command = parseCommandLine_0(incomingMessage);
    List<String[]> commands = parseCommandLines(incomingMessage);
    
    //Summary of output from successful commands.
    List<Map<String, String>> commandSummaries = new ArrayList<Map<String, String>>();
    
    try
    {
      for (int count=0; count<commands.size(); count++)
      {
        String[] command = commands.get(count);
        l.log(Level.INFO, "Running command {0} of {1}: {2}", 
            new Object[]{count, commands.size(), Arrays.asList(command)});
        
        File stdOutFile = new File(getWorkingDirectory(), "stdout-"+count+".txt");
        File stdErrFile = new File(getWorkingDirectory(), "stderr-"+count+".txt");
        FileWriter stdOut = new FileWriter(stdOutFile);
        FileWriter stdErr = new FileWriter(stdErrFile);

        l.log(Level.INFO, "Executing command line: {0}", Arrays.asList(command));

        int exitStatus = NativeCommandExecutor.executeNativeCommand(
            getWorkingDirectory(), command, stdOut, stdErr);

        stdOut.flush();
        stdOut.close();

        stdErr.flush();
        stdErr.close();

        l.info("Finished executing command: "+command[0]
            + "\nExit status was: "+exitStatus
            + "\nSTDOUT was written to: "+stdOutFile.getAbsolutePath()
            + "\nSTDERR was written to: "+stdErrFile.getAbsolutePath());
        
        if (exitStatus != 0)
        {
          throw new ProcessingException("Exit status of the command: "
              + Arrays.asList(command)
              + " \nwas "+exitStatus+". Assuming that non-zero means that execution failed.");
        }
        
        
        
        /*
         * Summarise execution and content produced as a series of strings
         */
        long stdOutLen = 0;
        long stdErrLen = 0;
        if (stdOutFile.exists())
        {
          stdOutLen = stdOutFile.length();
        }
        if (stdErrFile.exists())
        {
          stdErrLen = stdErrFile.length();
        }
        Map<String, String> summaryInfo = new HashMap<String, String>();
        
        summaryInfo.put("exitStatus", String.valueOf(exitStatus));
        summaryInfo.put("completed timestamp", 
            new Date(System.currentTimeMillis()).toString());
        summaryInfo.put("STDOUT_length", String.valueOf(stdOutLen));
        summaryInfo.put("STDERR_length", String.valueOf(stdErrLen));
        summaryInfo.put("Actual command strings", Arrays.asList(command).toString());
        summaryInfo.put("hostname", getRuntime().getNodeInfo().getHostname());
        summaryInfo.put("working_dir", getWorkingDirectory().getAbsolutePath());
        
        commandSummaries.add(summaryInfo);
      }
      
      
      l.info("All command lines have finished executing. Upload result files...");
      uploadOutputFiles(incomingMessage);
      

      

      
    
      /*
      * All 'work' has now been completed, so the final action to perform is
      * to prepare a notification message to send on job completion. You could
      * send multiple messages of different types here.
      */
      Set<Message> toPublish = new HashSet<Message>();

      /*
      * The 'createMessage' method is provided for convenience. It takes an 
      * existing message -- in this case 'incomingMessage' -- and creates a
      * child message. The new message 'done' has 'incomingMessage' set as its
      * parent. Various other fields such as the workflow execution ID are also
      * set automatically.
      */
      
      Message done = createMessage(
          incomingMessage,               //The message to use as a parent
          getOutgoingTopic(),              //The topic of the new message
          incomingMessage.getWorkflowStepId(),   //The workflow step ID
          //An optional human-readable description
          "Command line executed successfully.");
      
      //Arbitrary message content items can be added like this:
      for (int i=0; i<commandSummaries.size(); i++)
      {
        Map<String, String> summaryInfo = commandSummaries.get(i);
        done.getContent().put(PREFIX_COMMAND+i, summaryInfo.toString());
      }

      toPublish.add(done);
      return toPublish;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run one or more commands: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
    
  }
  
}
