/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package uk.org.microbase.workshop;

import java.io.File;
import java.io.FileWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;

/**
 * This class implements a simple fully-functional Microbase responder capable
 * of generating notification messages that describe a pairwise BLAST analysis.
 * 
 * @author Keith Flanagan
 */
public class WorkSplitterResponder
    extends AbstractMessageProcessor
{
  private static final Logger l = Logger.
      getLogger(WorkSplitterResponder.class.getSimpleName());
  private static final String TOPIC_IN = "SCAN-DIRECTORY";
  private static final String TOPIC_OUT = "PAIRWISE-BLAST-JOB";
  
  
  /**
   * This method creates a <code>ResponderInfo</code> instance that contains
   * default configuration information for this type of responder.
   * @return an object containing default configuration parameters for this
   * responder.
   */
  @Override
  protected ResponderInfo createDefaultResponderInfo()
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(true);
    info.setMaxDistributedCores(300);
    info.setMaxLocalCores(16);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(WorkSplitterResponder.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(TOPIC_IN);
    return info;
  }


  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    System.out.println("Doing preRun");
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    System.out.println("Doing cleanup");
  }
  
  @Override
  public Set<Message> processMessage(Message incomingMessage)
      throws ProcessingException
  {
    System.out.println("Performing computational work");
    try
    {
      parseMessage(incomingMessage);
      Set<FileMetaData[]> pairs = createPairsFromDirListing();
      Set<Message> jobMsgs = generateJobMessages(incomingMessage, pairs);
      
      return jobMsgs;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed generate a set of pairwise comparison notifications: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
  }
  
  private String sourceBucketName;
  private String sourcePath;
  private void parseMessage(Message m) 
      throws ProcessingException
  {
    try
    {
      sourceBucketName = m.getContent().get("bucket");
      sourcePath = m.getContent().get("path");
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to decode message", e);
    }
  }
  
  private Set<FileMetaData[]> createPairsFromDirListing()
      throws ProcessingException
  {
    try
    {
      Set<FileMetaData> s3Files = 
          getRuntime().getMicrobaseFS().
          listRemoteFileMetaData(sourceBucketName, sourcePath);
      
      Set<FileMetaData[]> pairs = new HashSet<FileMetaData[]>();
      for (FileMetaData file1 : s3Files)
      {
        if (!file1.getName().endsWith(".fna")) { continue; }
        for (FileMetaData file2 : s3Files)
        {
          if (!file2.getName().endsWith(".fna")) { continue; }
          FileMetaData[] pair = new FileMetaData[2];
          pair[0] = file1;
          pair[1] = file2;
          pairs.add(pair);
        }
      }
      return pairs;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to list remote directory", e);
    }
  }
  
  private Set<Message> generateJobMessages(
      Message parent, Set<FileMetaData[]> pairs)
      throws ProcessingException
  {
    try
    {
      Set<Message> pairMsgs = new HashSet<Message>();
      for (FileMetaData[] pair : pairs)
      {
        Message pairMsg = createMessage(
            parent,               //The message to use as a parent
            TOPIC_OUT,              //The topic of the new message
            parent.getWorkflowStepId(),   //The workflow step ID
            //An optional human-readable description
            "Contains a pairwise BLAST job description");
        pairMsg.getContent().put("subject_bucket", pair[0].getBucket());
        pairMsg.getContent().put("subject_path", pair[0].getPath());
        pairMsg.getContent().put("subject_filename", pair[0].getName());
        
        pairMsg.getContent().put("target_bucket", pair[1].getBucket());
        pairMsg.getContent().put("target_path", pair[1].getPath());
        pairMsg.getContent().put("target_filename", pair[1].getName());
        
        pairMsgs.add(pairMsg);
      }
      return pairMsgs;
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to generate message(s)", e);
    }
  }
  
}
