/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * File created: 28-Nov-2011, 23:12:54
 */
package uk.org.microbase.responder.spi;

import java.util.Set;
import uk.org.microbase.dist.processes.ExecutionResult;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author Keith Flanagan
 */
public interface MessageProcessor
    extends Callable<ExecutionResult>
{
  
  /**
   * Creates or obtains an existing ResponderInfo object for this
   * responder. Depending on your responder implementation, you may wish to
   * load a responder configuration from a local file, network location, or
   * something else.
   * 
   * @return
   * @throws RegistrationException 
   */
  public ResponderInfo acquireResponderInfo()
      throws RegistrationException;
  
  /**
   * Returns the existing ResponderInfo object. Can only be called after
   * <code>acquireResponderInfo</code> has been called at least once to create
   * or obtain a ResponderInfo instance.
   * 
   * @return 
   */
  public ResponderInfo getResponderInfo();
  
  /*
   * Methods called by the client
   */
  
  public void setProcessGuid(String processGuid);
  public void setClientRuntime(ClientRuntime runtime);
  public void setCpuPermits(Semaphore cpuPermits);
  
  /**
   * Called by the compute client once per instance of the responder,
   * after the responder has been assigned a <code>ResponderInfo</code>, 
   * <code>ClientRuntime</code>, and CPU permits semaphore, but before
   * any Messages have been processed.
   * 
   * You can use this method to perform any setup operations needed by your
   * responder implementation that requires a <code>ClientRuntime</code>, but
   * which must be completed before any Messages can be processed.
   * 
   * @throws RegistrationException 
   */
  public void initialise()
      throws RegistrationException;
  
  /**
   * This is executed before the 'cleanup' and 'processMessage' methods and
   * can optionally be used by your responder for setting up loggers, etc.
   *
   * @param message
   * @throws MessageProcessingException
   */
  public void preRun(Message message)
      throws ProcessingException;

  public void cleanupPreviousResults(Message message)
      throws ProcessingException;

  public Set<Message> processMessage(Message message)
      throws ProcessingException;
  
  
  /**
   * Allows a job implementation to notify the system of how far through a
   * particular unit of work it is. The implementation may decide how this
   * information is presented - e.g. in the form of meta data attached to the
   * global process ID, or a notification message. 
   * 
   * Other processes should only rely on progress broadcasts as a HINT,and must
   * NOT rely on progress information for anything critical to their own correct
   * operation. Even if a progress of '100%' is reported, the job may still be
   * killed by the host client before it can be marked 'complete'. Therefore,
   * other processes should always wait for the official 'completed' 
   * notification message before assuming this job is complete.
   * 
   * @param percentComplete a value between 0 and 1, where 0 is 0% complete and
   * 1 is 100% complete.
   * @throws ProcessingException 
   */
  public void notifyProgress(float percentComplete)
      throws ProcessingException;
  
}
