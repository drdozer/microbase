/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 27-Sep-2012, 13:49:34
 */

package uk.org.microbase.responder.spi;

import java.util.Properties;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;

/**
 * A convenience class that can build a ResponderInfo object from a
 * set of configuration options specified in a a Properties-format 
 * stream.
 * 
 * @author Keith Flanagan
 */
public class ResponderInfoPropertiesParser
{
  private static final String PROP_RESPONDER_NAME = "RESPONDER_NAME";
  private static final String PROP_RESPONDER_VERSION = "RESPONDER_VERSION";
  private static final String PROP_RESPONDER_ID = "RESPONDER_ID";
  
  private static final String PROP_TOPIC_IN = "TOPIC_IN";
  private static final String PROP_TOPIC_OUT = "TOPIC_OUT";
  private static final String PROP_TOPIC_ERR = "TOPIC_ERR";
  
  private static final String PROP_ENABLED = "ENABLED";
  
  private static final String PROP_MAX_DISTRIBUTED_CORES = "MAX_DISTRIBUTED_CORES";
  private static final String PROP_MAX_LOCAL_CORES = "MAX_LOCAL_CORES";
  private static final String PROP_PREFERRED_CORES_PER_JOB = "PREFERRED_CORES_PER_JOB";
  
  private static final String PROP_QUEUE_POPULATOR_RUN_EVERY_MS = "QUEUE_POPULATOR_RUN_EVERY_MS";
  
  private static final String PROP_DATA_DESTINATION_BUCKET_OVERRIDE = "DATA_DESTINATION_BUCKET_OVERRIDE";
  
  private static final String PROP_LOG_DESTINATION_BUCKET_OVERRIDE = "LOG_DESTINATION_BUCKET_OVERRIDE";
  
  public static ResponderInfo parseFromProperties(Properties props)
      throws RegistrationException
  {
    try
    {
      ResponderInfo info = new ResponderInfo();
      
      info.setEnabled(Boolean.parseBoolean(props.getProperty(PROP_ENABLED)));
      info.setMaxDistributedCores(Integer.parseInt(props.getProperty(PROP_MAX_DISTRIBUTED_CORES)));
      info.setMaxLocalCores(Integer.parseInt(props.getProperty(PROP_MAX_LOCAL_CORES)));
      info.setPreferredCpus(Integer.parseInt(props.getProperty(PROP_PREFERRED_CORES_PER_JOB)));
      info.setQueuePopulatorLastRunTimestamp(0);
      info.setQueuePopulatorRunEveryMs(Long.parseLong(props.getProperty(PROP_QUEUE_POPULATOR_RUN_EVERY_MS)));
      info.setResponderName(props.getProperty(PROP_RESPONDER_NAME));
      info.setResponderVersion(props.getProperty(PROP_RESPONDER_VERSION));
      info.setResponderId(props.getProperty(PROP_RESPONDER_ID));
      info.setTopicOfInterest(props.getProperty(PROP_TOPIC_IN));
      
      info.setTopicOut(props.getProperty(PROP_TOPIC_OUT));
      info.setTopicErr(props.getProperty(PROP_TOPIC_ERR));
      
      info.setDataDestinationBucketOverride(props.getProperty(PROP_DATA_DESTINATION_BUCKET_OVERRIDE));
      info.setLogDestinationBucketOverride(props.getProperty(PROP_LOG_DESTINATION_BUCKET_OVERRIDE));
      
      return info;
    }
    catch(Exception e)
    {
      throw new RegistrationException(
          "Failed to obtain configuration information from a Properties object. "
          + "Perhaps one of the required properties was missing?", e);
    }
  }
}
