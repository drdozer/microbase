/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package misc.files;

import java.util.HashMap;
import java.util.Map;

/**
 * A description necessary to execute a job. A responder description ID is
 * required, plus input parameters.
 *
 * @author Keith Flanagan
 */
public class JobDescription
{
  private String guid;
  private String responderGuid;
  /**
   * Optional - stores a reference to the notification event that caused this
   * job description to be generated.
   */
  private String messageGuid;

  private Map<String, Object> inputParamemers;

  public JobDescription()
  {
    inputParamemers = new HashMap<String, Object>();
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public void addInputParameter(String name, Object value)
  {
    inputParamemers.put(name, value);
  }

  public Map<String, Object> getInputParamemers()
  {
    return inputParamemers;
  }

  public void setInputParamemers(Map<String, Object> inputParamemers)
  {
    this.inputParamemers = inputParamemers;
  }

  public String getResponderGuid()
  {
    return responderGuid;
  }

  public void setResponderGuid(String responderGuid)
  {
    this.responderGuid = responderGuid;
  }

  public String getMessageGuid()
  {
    return messageGuid;
  }

  public void setMessageGuid(String messageGuid)
  {
    this.messageGuid = messageGuid;
  }

}
