/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package misc.files;

/**
 * An enum containing the possible states a job is permitted to be in. A job may
 * switch between many states, depending on the situation. However, a job should
 * always start in state <code>NEW</code> and end in either state
 * <code>COMPLETED_SUCCESS</code> or <code>COMPLETED_FAILURE</code>.
 * 
 * 
 * @author Keith Flanagan
 */
public enum JobState
{
  /**
   * New job, not doing anything yet. Not yet noticed by the task scheduler.
   * All jobs should enter the system in this state. A job may only be in this
   * state once.
   */
  NEW,
  /**
   * Noticed by the scheduler, but can't be marked RELEASABLE yet - one or
   * more resources required by this job are not yet in secure storage.
   */
  WAITING,
  /**
   * All of this job's required resources are present. Jobs in this state can
   * either be sent for processing, or sub-leased to a child JobServer
   */
  RELEASABLE,
  /**
   * The current state of this job is unknown because its' sub-leased to
   * another jobserver (use appropriate method call to find out which
   * JobServer). The job is guaranteed to be in at least state 'releasable'.
   */
  SUBLEASED,
  /**
   * The job is currently leased to a compute node.
   */
  PROCESSING,
  /**
   * Processing is complete, but resource archival if results is still
   * outstanding
   */
  ARCHIVAL,
  /**
   * Notification has been received of all the job's results being
   * successfully archived. This is a termination state - once this has been
   * assigned, a job should not be assigned another state.
   */
  COMPLETED_SUCCESS,
  /**
   * This job has reached its retry limit. Permanently cancelled. This is a
   * termination state - once this has been assigned, a job should not be
   * assigned another state.
   */
  COMPLETED_FAILURE;
}
