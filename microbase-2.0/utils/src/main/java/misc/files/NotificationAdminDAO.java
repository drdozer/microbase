/*
 * Copyright 2010 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package misc.files;

import java.util.Set;
import misc.files.Publisher;
import misc.files.Subscriber;

/**
 *
 * @author Keith Flanagan
 */
public class NotificationAdminDAO
{

//  public NotificationAdminDAO(ClientPool pool, String keyspace)
//  {
//    this.pool = pool;
//    this.keyspace = keyspace;
//  }

  public void registerSubscriber(Subscriber subscriber)
  {

  }

  public void registerPublisher(Publisher publisher)
  {

  }

//  public void registerTopic(Topic topic)
//  {
//
//  }
//
//  public void putMessage(Message message)
//  {
//
//  }

//  public void getMessage(Message message)
//  {
//
//  }
}
