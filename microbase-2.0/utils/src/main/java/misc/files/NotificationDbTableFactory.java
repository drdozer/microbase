/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 10-Aug-2010, 17:14:33
 */

package misc.files;


/**
 *
 * @author Keith Flanagan
 */
public class NotificationDbTableFactory
{
  private static final String NODE_TABLE_CF = "nodes";
//  public static Table createNodesTable(ClientPool pool, String keyspace)
//  {
//    return new Table(pool, keyspace, NODE_TABLE_CF);
//  }
//
//  private static final String HOST_REGISTRATION_TABLE_CF = "node_host_to_registrations";
//  public static Table createHostnameRegistrationsTable(ClientPool pool, String keyspace)
//  {
//    return new Table(pool, keyspace, HOST_REGISTRATION_TABLE_CF);
//  }
}
