/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package misc.files;

import java.util.Set;

/**
 * One of these is stored in the database when a responder/job is first installed.
 *
 * @author Keith Flanagan
 */
public class ResponderDescription
{
  private String guid;
  private JobType type;

  private String mavenGroupId;
  private String mavenArtifactId;
  private String mavenVersion;
  private String javaClassname;

  private int minInstances;
  private int maxInstances;

  private Set<String> subscriberTopics;
  private Set<String> publisherTopics;

  public ResponderDescription()
  {
  }

  public String getJavaClassname()
  {
    return javaClassname;
  }

  public void setJavaClassname(String javaClassname)
  {
    this.javaClassname = javaClassname;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public String getMavenArtifactId()
  {
    return mavenArtifactId;
  }

  public void setMavenArtifactId(String mavenArtifactId)
  {
    this.mavenArtifactId = mavenArtifactId;
  }

  public String getMavenGroupId()
  {
    return mavenGroupId;
  }

  public void setMavenGroupId(String mavenGroupId)
  {
    this.mavenGroupId = mavenGroupId;
  }

  public String getMavenVersion()
  {
    return mavenVersion;
  }

  public void setMavenVersion(String mavenVersion)
  {
    this.mavenVersion = mavenVersion;
  }

  public int getMaxInstances()
  {
    return maxInstances;
  }

  public void setMaxInstances(int maxInstances)
  {
    this.maxInstances = maxInstances;
  }

  public int getMinInstances()
  {
    return minInstances;
  }

  public void setMinInstances(int minInstances)
  {
    this.minInstances = minInstances;
  }

  public Set<String> getPublisherTopics()
  {
    return publisherTopics;
  }

  public void setPublisherTopics(Set<String> publisherTopics)
  {
    this.publisherTopics = publisherTopics;
  }

  public Set<String> getSubscriberTopics()
  {
    return subscriberTopics;
  }

  public void setSubscriberTopics(Set<String> subscriberTopics)
  {
    this.subscriberTopics = subscriberTopics;
  }

  public JobType getType()
  {
    return type;
  }

  public void setType(JobType type)
  {
    this.type = type;
  }
}
