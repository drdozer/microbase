/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.org.microbase.util.serialization;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Keith Flanagan
 */
public class JsonSerializer
{
  private static final ObjectMapper jsonMapper = new ObjectMapper();

  public static byte[] serialize(Object bean)
      throws MbSerializerException
  {
    try
    {
      String serialized = jsonMapper.writeValueAsString(bean);
      return serialized.getBytes("UTF-8");
    }
    catch(Exception e)
    {
      throw new MbSerializerException("Failed to serialise object: "+
          bean.getClass().getName()+": "+bean, e);
    }
  }

  public static String serializeToString(Object bean)
      throws MbSerializerException
  {
    try
    {
      String serialized = jsonMapper.writeValueAsString(bean);
      return serialized;
    }
    catch(Exception e)
    {
      throw new MbSerializerException("Failed to serialise object: "+
          bean.getClass().getName()+": "+bean, e);
    }
  }

  public static <T> T deserialize(byte[] jsonBytes, Class<T> expectedType)
      throws MbSerializerException
  {
    if (jsonBytes == null)
    {
      return null;
    }
    try
    {
      T deserialised = jsonMapper.readValue(
          new String(jsonBytes, "UTF-8"), expectedType);
      return deserialised;
    }
    catch(IOException e)
    {
      throw new MbSerializerException(
          "Failed to de-serialise JSON byte[] to type: "
          + expectedType.getClass().getName(), e);
    }
  }

  public static <T> T deserializeFromString(String jsonString, Class<T> expectedType)
      throws MbSerializerException
  {
    if (jsonString == null)
    {
      return null;
    }
    try
    {
      T deserialised = jsonMapper.readValue(jsonString, expectedType);
      return deserialised;
    }
    catch(IOException e)
    {
      throw new MbSerializerException(
          "Failed to de-serialise JSON String to type: "
          + expectedType.getClass().getName(), e);
    }
  }
}
